export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyAcbwfcipQUSrlgy8xqPGY30MQCGS7W6NQ',
    authDomain: 'three-steps-to-pay-ops.firebaseapp.com',
    databaseURL: 'https://three-steps-to-pay-ops.firebaseio.com',
    projectId: 'three-steps-to-pay-ops',
    storageBucket: 'three-steps-to-pay-ops.appspot.com',
    messagingSenderId: '848966429933',
    appId: '1:848966429933:web:823f6f95cf45ff3bdd30e4',
  },
  constants: {
    INIT_YEAR: 2020,
    MIN_AMOUNT_VALUE: 1,
    MAX_AMOUNT_VALUE: 100,
  },
  returnUrl: 'https://churchpay.be/store/status',
  pusher: {
    apiKey: '2b0584c4942e76b48d44',
    cluster: 'eu',
  },
  stripe: {
    apiKey:
      'pk_live_51H6dshGn5AzSiPFB6TI2vWKsqiygXT4TZo6vdfff3gMHZD4ViQe0Yj6y7qjACsLsSfFE59mbWl8jYPGLsdYvBms200onPbjIyk',
  },
  paypal: {
    clientId:
      'AdLfVrBdG0uAogBSQxD-tjh5GRtd9-HmecFOj050WZxt_Cs_PD304kmHrOXhorz_2WcxBUl7kw_wJZA5',
  },
  bancontact: {
    returnUrl: 'https://churchpay.be/store/payment/bancontact/status',
  },
  appVersion: require('../../package.json').version,
}
