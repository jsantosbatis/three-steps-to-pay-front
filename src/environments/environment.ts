// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDGsUGkkyeQJGgXxgOadmUJrQTtqX-K8w8',
    authDomain: 'three-steps-to-pay.firebaseapp.com',
    databaseURL: 'https://three-steps-to-pay.firebaseio.com',
    projectId: 'three-steps-to-pay',
    storageBucket: 'three-steps-to-pay.appspot.com',
    messagingSenderId: '778440860310',
    appId: '1:778440860310:web:a420f81d6776d5128318b7',
    measurementId: 'G-0WBGMQH8N7',
  },
  constants: {
    INIT_YEAR: 2020,
    MIN_AMOUNT_VALUE: 1,
    MAX_AMOUNT_VALUE: 100,
  },
  returnUrl: 'https://192.168.1.6:4200/store/status',
  pusher: {
    apiKey: '1c239f0caa3d343fb103',
    cluster: 'eu',
  },
  stripe: {
    apiKey: 'pk_test_JHYA8Il7W95eEnxFrvgnUQ4r00fYxCr1E5',
  },
  paypal: {
    clientId:
      'AdLfVrBdG0uAogBSQxD-tjh5GRtd9-HmecFOj050WZxt_Cs_PD304kmHrOXhorz_2WcxBUl7kw_wJZA5',
  },
  bancontact: {
    returnUrl:
      'https://churchpay-dev-front.jo-consultancy.be/store/payment/bancontact/status',
  },
  appVersion: require('../../package.json').version + '-dev',
}
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
