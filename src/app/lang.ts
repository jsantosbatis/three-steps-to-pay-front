export const SUCCESSFULLY_UPDATED = 'Modifications bien enregistrées';
export const SUCCESSFULLY_CREATED = "Ajout d'un nouvel enregistrement";
export const SUCCESSFULLY_DELETED = "Suppression de l'enregistrement";
export const SUCCESSFULLY_CANCELLED = 'Modificiations annulées';

export const UNSUCCESSFULLY_DELETED =
  "Impossible de supprimer l'enregistrement";

export const EMAIL_SENT = 'Email bien envoyé';
