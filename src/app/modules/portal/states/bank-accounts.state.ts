import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap, catchError, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import * as moment from 'moment';
import { MayHaveId } from '@portal/interfaces/MayHaveId';
import { Navigate } from '@ngxs/router-plugin';
import { BankAccount } from '@portal/interfaces/BankAccount';
import { BankAccountService } from '@portal/services/bank-account.service';
import {
  GetAll,
  AddBankAccount,
  EditBankAccount,
  DeleteBankAccount,
  SetCurrentBankAccount,
  ResetDeleteError,
} from '../actions/bank-accounts.action';
import { HttpErrorResponse } from '@angular/common/http';

export interface BankAccountsStateModel {
  items: BankAccount[];
  isLoading: boolean;
  lastUpdate: moment.Moment;
  current: BankAccount;
  error: string;
  deleted: boolean;
}
@State<BankAccountsStateModel>({
  name: 'portal_bank_accounts',
  defaults: {
    items: [],
    isLoading: false,
    lastUpdate: null,
    current: null,
    error: '',
    deleted: null,
  },
})
@Injectable()
export class BankAccountsState {
  constructor(private bankAccountService: BankAccountService) {}

  @Selector()
  static items(state: BankAccountsStateModel) {
    return state.items;
  }

  @Selector()
  static isLoading(state: BankAccountsStateModel) {
    return state.isLoading;
  }

  @Selector()
  static current(state: BankAccountsStateModel) {
    return state.current;
  }

  @Selector()
  static error(state: BankAccountsStateModel) {
    return state.error;
  }

  @Selector()
  static deleted(state: BankAccountsStateModel) {
    return state.deleted;
  }

  @Action(GetAll)
  getAll(ctx: StateContext<BankAccountsStateModel>) {
    ctx.patchState({ isLoading: true });
    if (
      !ctx.getState().lastUpdate ||
      this.haveToRefresh(ctx.getState().lastUpdate)
    ) {
      return this.bankAccountService.getAll().pipe(
        tap((data) => {
          ctx.patchState({
            items: data,
            isLoading: false,
            lastUpdate: moment(),
          });
        }),
        catchError(() => {
          ctx.patchState({ isLoading: false, lastUpdate: null });
          return of<BankAccount[]>([]);
        })
      );
    } else {
      ctx.patchState({ isLoading: false });
    }
  }

  @Action(AddBankAccount)
  add(ctx: StateContext<BankAccountsStateModel>, action: AddBankAccount) {
    ctx.patchState({ isLoading: true });
    return this.bankAccountService.insert(action.data).pipe(
      mergeMap((data: MayHaveId) => {
        return of<string>(data.id);
      }),
      mergeMap((id) => {
        return this.bankAccountService.getById(id);
      }),
      tap((data) => {
        const state = ctx.getState();
        ctx.patchState({
          items: [...state.items, data],
          current: data,
          isLoading: false,
        });
        ctx.dispatch(new Navigate(['portal/bank-accounts']));
      }),
      catchError(() => {
        return of<BankAccount[]>([]);
      })
    );
  }

  @Action(EditBankAccount)
  edit(ctx: StateContext<BankAccountsStateModel>, action: EditBankAccount) {
    ctx.patchState({ isLoading: true, deleted: null });
    return this.bankAccountService.update(action.data).pipe(
      tap((data) => {
        const state = ctx.getState();
        ctx.patchState({
          items: state.items.map((item) => (item.id === data.id ? data : item)),
          current: data,
          isLoading: false,
        });
      }),
      catchError(() => {
        return of<BankAccount[]>([]);
      })
    );
  }

  @Action(DeleteBankAccount)
  delete(ctx: StateContext<BankAccountsStateModel>, action: DeleteBankAccount) {
    ctx.patchState({ isLoading: true });
    return this.bankAccountService.delete(action.id).pipe(
      tap(() => {
        const state = ctx.getState();
        ctx.patchState({
          items: state.items.filter((item) => item.id !== action.id),
          isLoading: false,
          deleted: true,
        });
      }),
      catchError((error: HttpErrorResponse) => {
        ctx.patchState({
          error: error.error.message,
          isLoading: false,
          deleted: false,
        });
        return of<BankAccount[]>([]);
      })
    );
  }

  @Action(ResetDeleteError)
  resetDeleteError(ctx: StateContext<BankAccountsStateModel>) {
    ctx.patchState({ deleted: null, error: '' });
  }

  @Action(SetCurrentBankAccount)
  setCurrentBankAccount(
    ctx: StateContext<BankAccountsStateModel>,
    action: SetCurrentBankAccount
  ) {
    ctx.patchState({ isLoading: true, deleted: null, error: '' });
    if (action.id) {
      const items = ctx.getState().items;
      const search = items.filter((item) => item.id == action.id);
      if (search.length == 1) {
        ctx.patchState({ current: search[0], isLoading: false });
      } else {
        return this.bankAccountService.getById(action.id).pipe(
          tap((data) => {
            ctx.patchState({ current: data, isLoading: false });
          }),
          catchError(() => {
            ctx.patchState({ current: null, isLoading: false });
            return of<BankAccount[]>([]);
          })
        );
      }
    } else {
      ctx.patchState({ current: null, isLoading: false });
    }
  }

  haveToRefresh(lastUpdate) {
    return moment.duration(moment().diff(lastUpdate)).asMinutes() > 15;
  }
}
