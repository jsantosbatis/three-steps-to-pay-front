import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { PointOfSaleService } from '../services/point-of-sale.service';
import { tap, catchError, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { PointOfSale } from '@portal/interfaces/PointOfSale';
import {
  GetAll,
  AddPointOfSale,
  SetCurrentPointOfSale,
  EditPointOfSale,
  DeletePointOfSale,
} from '../actions/point-of-sales.action';
import * as moment from 'moment';
import { MayHaveId } from '@portal/interfaces/MayHaveId';
import { Navigate } from '@ngxs/router-plugin';

export interface PointOfSalesStateModel {
  items: PointOfSale[];
  isLoading: boolean;
  lastUpdate: moment.Moment;
  current: PointOfSale;
}
@State<PointOfSalesStateModel>({
  name: 'portal_pointOfSales',
  defaults: {
    items: [],
    isLoading: false,
    lastUpdate: null,
    current: null,
  },
})
@Injectable()
export class PointOfSalesState {
  constructor(private pointOfSaleService: PointOfSaleService) {}

  @Selector()
  static items(state: PointOfSalesStateModel) {
    return state.items;
  }

  @Selector()
  static isLoading(state: PointOfSalesStateModel) {
    return state.isLoading;
  }

  @Selector()
  static current(state: PointOfSalesStateModel) {
    return state.current;
  }

  @Action(GetAll)
  getAll(ctx: StateContext<PointOfSalesStateModel>) {
    ctx.patchState({ isLoading: true });
    if (
      !ctx.getState().lastUpdate ||
      this.haveToRefresh(ctx.getState().lastUpdate)
    ) {
      return this.pointOfSaleService.getAll().pipe(
        tap((data) => {
          ctx.patchState({
            items: data,
            isLoading: false,
            lastUpdate: moment(),
          });
        }),
        catchError(() => {
          ctx.patchState({ isLoading: false, lastUpdate: null });
          return of<PointOfSale[]>([]);
        })
      );
    } else {
      ctx.patchState({ isLoading: false });
    }
  }

  @Action(AddPointOfSale)
  add(ctx: StateContext<PointOfSalesStateModel>, action: AddPointOfSale) {
    ctx.patchState({ isLoading: true });
    return this.pointOfSaleService.insert(action.data).pipe(
      mergeMap((data: MayHaveId) => {
        return of<string>(data.id);
      }),
      mergeMap((id) => {
        return this.pointOfSaleService.getById(id);
      }),
      tap((data) => {
        const state = ctx.getState();
        ctx.patchState({
          items: [...state.items, data],
          isLoading: false,
        });
        ctx.dispatch(new Navigate(['portal/point-of-sale/', data.id]));
      }),
      catchError(() => {
        return of<PointOfSale[]>([]);
      })
    );
  }

  @Action(EditPointOfSale)
  edit(ctx: StateContext<PointOfSalesStateModel>, action: AddPointOfSale) {
    ctx.patchState({ isLoading: true });
    return this.pointOfSaleService.update(action.data).pipe(
      tap((data) => {
        const state = ctx.getState();
        ctx.patchState({
          items: state.items.map((item) => (item.id === data.id ? data : item)),
          isLoading: false,
          current: state.current.id == data.id ? data : state.current,
        });
      }),
      catchError(() => {
        return of<PointOfSale[]>([]);
      })
    );
  }

  @Action(DeletePointOfSale)
  delete(ctx: StateContext<PointOfSalesStateModel>, action: DeletePointOfSale) {
    ctx.patchState({ isLoading: true });
    return this.pointOfSaleService.delete(action.id).pipe(
      tap(() => {
        const state = ctx.getState();
        ctx.patchState({
          items: state.items.filter((item) => item.id !== action.id),
          isLoading: false,
        });
      }),
      catchError(() => {
        return of<PointOfSale[]>([]);
      })
    );
  }

  @Action(SetCurrentPointOfSale)
  setCurrentPointOfSale(
    ctx: StateContext<PointOfSalesStateModel>,
    action: SetCurrentPointOfSale
  ) {
    ctx.patchState({ isLoading: true });
    if (action.id) {
      const items = ctx.getState().items;
      const search = items.filter((item) => item.id == action.id);
      if (search.length == 1) {
        ctx.patchState({ current: search[0], isLoading: false });
      } else {
        return this.pointOfSaleService.getById(action.id).pipe(
          tap((data) => {
            ctx.patchState({ current: data, isLoading: false });
          }),
          catchError(() => {
            ctx.patchState({ current: null, isLoading: false });
            return of<PointOfSale[]>([]);
          })
        );
      }
    } else {
      ctx.patchState({ current: null, isLoading: false });
    }
  }

  haveToRefresh(lastUpdate) {
    return moment.duration(moment().diff(lastUpdate)).asMinutes() > 15;
  }
}
