import { Injectable } from '@angular/core'
import { Navigate } from '@ngxs/router-plugin'
import { Action, Selector, State, StateContext } from '@ngxs/store'
import { MayHaveId } from '@portal/interfaces/MayHaveId'
import { Product } from '@portal/interfaces/Product'
import { ProductCategory } from '@portal/interfaces/ProductCategory'
import { ProductCategoryService } from '@portal/services/product-category.service'
import { ProductService } from '@portal/services/product.service'
import * as moment from 'moment'
import { of } from 'rxjs'
import { catchError, mergeMap, tap } from 'rxjs/operators'
import {
  AddProduct,
  DeleteProduct,
  EditProduct,
  GetProductCategories,
  GetProducts,
  SetCurrentProduct,
} from '../actions/products.action'
import { PointOfSaleService } from '../services/point-of-sale.service'

export interface ProductsStateModel {
  items: Product[]
  categories: ProductCategory[]
  isLoading: boolean
  lastUpdate: moment.Moment
  current: Product
  idLoaded: string[]
}
@State<ProductsStateModel>({
  name: 'portal_products',
  defaults: {
    items: [],
    categories: [],
    isLoading: false,
    lastUpdate: null,
    current: null,
    idLoaded: [],
  },
})
@Injectable()
export class ProductsState {
  constructor(
    private pointOfSaleService: PointOfSaleService,
    private productService: ProductService,
    private productCategoryService: ProductCategoryService
  ) {}

  @Selector()
  static items(state: ProductsStateModel) {
    return (id: string) => {
      return state.items.filter((item) => item.point_of_sale_id == id)
    }
  }

  @Selector()
  static isLoading(state: ProductsStateModel) {
    return state.isLoading
  }

  @Selector()
  static current(state: ProductsStateModel) {
    return state.current
  }

  @Selector()
  static productCategories(state: ProductsStateModel) {
    return state.categories
  }

  @Action(GetProducts)
  getProducts(ctx: StateContext<ProductsStateModel>, action: GetProducts) {
    ctx.patchState({ isLoading: true })
    if (
      !ctx.getState().lastUpdate ||
      this.haveToRefresh(ctx.getState().lastUpdate)
    ) {
      ctx.patchState({ items: [], idLoaded: [] })
    }

    const state = ctx.getState()
    if (!state.idLoaded.includes(action.id)) {
      return this.pointOfSaleService.getProducts(action.id).pipe(
        tap((data) => {
          ctx.patchState({
            items: [...ctx.getState().items, ...data],
            isLoading: false,
            idLoaded: [...ctx.getState().idLoaded, action.id],
            lastUpdate: moment(),
          })
        }),
        catchError(() => {
          ctx.patchState({ isLoading: false, lastUpdate: null })
          return of<Product[]>([])
        })
      )
    } else {
      ctx.patchState({ isLoading: false })
    }
  }

  @Action(AddProduct)
  add(ctx: StateContext<ProductsStateModel>, action: AddProduct) {
    ctx.patchState({ isLoading: true })
    return this.productService.insert(action.data).pipe(
      mergeMap((data: MayHaveId) => {
        return of<string>(data.id)
      }),
      mergeMap((id) => {
        return this.productService.getById(id)
      }),
      tap((data) => {
        const state = ctx.getState()
        ctx.patchState({
          items: [...state.items, data],
          isLoading: false,
        })
        ctx.dispatch(
          new Navigate([
            'portal/point-of-sale',
            data.point_of_sale_id,
            'catalog',
          ])
        )
      }),
      catchError(() => {
        return of<Product[]>([])
      })
    )
  }

  @Action(EditProduct)
  edit(ctx: StateContext<ProductsStateModel>, action: EditProduct) {
    ctx.patchState({ isLoading: true })
    return this.productService.update(action.data).pipe(
      tap((data) => {
        const state = ctx.getState()
        ctx.patchState({
          items: state.items.map((item) => (item.id === data.id ? data : item)),
          isLoading: false,
        })
      }),
      catchError(() => {
        return of<Product[]>([])
      })
    )
  }

  @Action(DeleteProduct)
  delete(ctx: StateContext<ProductsStateModel>, action: DeleteProduct) {
    ctx.patchState({ isLoading: true })
    return this.productService.delete(action.id).pipe(
      tap(() => {
        const state = ctx.getState()
        ctx.patchState({
          items: state.items.filter((item) => item.id !== action.id),
          isLoading: false,
        })
      }),
      catchError(() => {
        return of<Product[]>([])
      })
    )
  }

  @Action(SetCurrentProduct)
  setCurrentPointOfSale(
    ctx: StateContext<ProductsStateModel>,
    action: SetCurrentProduct
  ) {
    ctx.patchState({ isLoading: true })
    if (action.id) {
      const items = ctx.getState().items
      const search = items.filter((item) => item.id == action.id)
      if (search.length == 1) {
        ctx.patchState({ current: search[0], isLoading: false })
      } else {
        return this.productService.getById(action.id).pipe(
          tap((data) => {
            ctx.patchState({ current: data, isLoading: false })
          }),
          catchError(() => {
            ctx.patchState({ current: null, isLoading: false })
            return of<Product[]>([])
          })
        )
      }
    } else {
      ctx.patchState({ current: null, isLoading: false })
    }
  }

  @Action(GetProductCategories)
  getProductCategories(ctx: StateContext<ProductsStateModel>) {
    return this.productCategoryService.fromLang('fr').pipe(
      tap((data) => {
        ctx.patchState({ categories: data })
      }),
      catchError(() => {
        ctx.patchState({ categories: [] })
        return of<ProductCategory[]>([])
      })
    )
  }

  haveToRefresh(lastUpdate) {
    console.log(moment.duration(moment().diff(lastUpdate)).asMinutes())
    return moment.duration(moment().diff(lastUpdate)).asMinutes() > 1
  }
}
