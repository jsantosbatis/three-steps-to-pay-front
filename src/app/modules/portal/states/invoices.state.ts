import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import * as moment from 'moment';
import { Invoice } from '@portal/interfaces/Invoice';
import { InvoiceService } from '@portal/services/invoice.service';
import { GetByPeriod } from '../actions/invoices.action';

export interface InvoicesStateModel {
  items: Invoice[];
  isLoading: boolean;
  lastUpdate: moment.Moment;
}
@State<InvoicesStateModel>({
  name: 'portal_invoices',
  defaults: {
    items: [],
    isLoading: false,
    lastUpdate: null,
  },
})
@Injectable()
export class InvoicesState {
  constructor(private invoiceService: InvoiceService) {}

  @Selector()
  static items(state: InvoicesStateModel) {
    return state.items;
  }

  @Selector()
  static isLoading(state: InvoicesStateModel) {
    return state.isLoading;
  }

  @Action(GetByPeriod)
  getByPeriod(ctx: StateContext<InvoicesStateModel>, action: GetByPeriod) {
    ctx.patchState({ isLoading: true });
    if (
      !ctx.getState().lastUpdate ||
      this.haveToRefresh(ctx.getState().lastUpdate)
    ) {
      return this.invoiceService
        .getByPeriod(action.startDate, action.endDate)
        .pipe(
          tap((data) => {
            ctx.patchState({
              items: data,
              isLoading: false,
              lastUpdate: moment(),
            });
          }),
          catchError(() => {
            ctx.patchState({ isLoading: false, lastUpdate: null });
            return of<Invoice[]>([]);
          })
        );
    } else {
      ctx.patchState({ isLoading: false });
    }
  }

  haveToRefresh(lastUpdate) {
    return moment.duration(moment().diff(lastUpdate)).asMinutes() > 15;
  }
}
