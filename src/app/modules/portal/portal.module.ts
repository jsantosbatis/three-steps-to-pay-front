import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin'
import { NgxsRouterPluginModule } from '@ngxs/router-plugin'
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin'
import { NgxsModule } from '@ngxs/store'
import { CurrencyMaskInputMode, NgxCurrencyModule } from 'ngx-currency'
import { environment } from 'src/environments/environment'
import { SharedModule } from '../shared/shared.module'
import { AppRoutingModule } from './app-routing.module'
import { BankAccountTileComponent } from './components/bank-account-tile/bank-account-tile.component'
import { CategoryDonutChartComponent } from './components/category-donut-chart/category-donut-chart.component'
import { CategoryLineChartComponent } from './components/category-line-chart/category-line-chart.component'
import { EmptyDataComponent } from './components/empty-data/empty-data.component'
import { HeaderComponent } from './components/header/header.component'
import { NavBarComponent } from './components/nav-bar/nav-bar.component'
import { PaymentMethodTypeDonutChartComponent } from './components/payment-method-type-donut-chart/payment-method-type-donut-chart.component'
import { PointOfSaleTileComponent } from './components/point-of-sale-tile/point-of-sale-tile.component'
import { AppPortalContainerComponent } from './components/portal-container/portal-container.component'
import { CanEditUserAccount } from './guards/can-edit-user-account.guard'
import { IsNotSignedInGuard } from './guards/is-not-signed-in.guard'
import { IsSignedInGuard } from './guards/is-signed-in.guard'
import { BankAccountDetailComponent } from './pages/bank-account-detail/bank-account-detail.component'
import { BankAccountsComponent } from './pages/bank-accounts/bank-accounts.component'
import { DashboardComponent } from './pages/dashboard/dashboard.component'
import { InvoiceTransfersComponent } from './pages/invoice-transfers/invoice-transfers.component'
import { InvoicesComponent } from './pages/invoices/invoices.component'
import { LoginComponent } from './pages/login/login.component'
import { PointOfSaleCatalogComponent } from './pages/point-of-sale-catalog/point-of-sale-catalog.component'
import { PointOfSaleDetailComponent } from './pages/point-of-sale-detail/point-of-sale-detail.component'
import { PointOfSaleTransactionComponent } from './pages/point-of-sale-transaction/point-of-sale-transaction.component'
import { PointOfSaleComponent } from './pages/point-of-sale/point-of-sale.component'
import { PointOfSalesComponent } from './pages/point-of-sales/point-of-sales.component'
import { ProductDetailComponent } from './pages/product-detail/product-detail.component'
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component'
import { UserDetailComponent } from './pages/user-detail/user-detail.component'
import { UsersComponent } from './pages/users/users.component'
import { BankAccountsState } from './states/bank-accounts.state'
import { InvoicesState } from './states/invoices.state'
import { PointOfSalesState } from './states/point-of-sales.state'
import { ProductsState } from './states/products.state'

export const customCurrencyMaskConfig = {
  align: 'left',
  allowNegative: false,
  allowZero: true,
  decimal: '.',
  precision: 2,
  prefix: '',
  suffix: ' €',
  thousands: '',
  nullable: true,
  min: null,
  max: null,
  inputMode: CurrencyMaskInputMode.FINANCIAL,
}

@NgModule({
  declarations: [
    AppPortalContainerComponent,
    BankAccountsComponent,
    BankAccountDetailComponent,
    BankAccountTileComponent,
    DashboardComponent,
    InvoicesComponent,
    LoginComponent,
    NavBarComponent,
    PointOfSalesComponent,
    PointOfSaleComponent,
    PointOfSaleTileComponent,
    PointOfSaleCatalogComponent,
    PointOfSaleDetailComponent,
    PointOfSaleTransactionComponent,
    ProductDetailComponent,
    UsersComponent,
    UserDetailComponent,
    ResetPasswordComponent,
    InvoiceTransfersComponent,
    EmptyDataComponent,
    HeaderComponent,
    CategoryDonutChartComponent,
    CategoryLineChartComponent,
    PaymentMethodTypeDonutChartComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    NgxsModule.forRoot(
      [PointOfSalesState, ProductsState, BankAccountsState, InvoicesState],
      {
        developmentMode: !environment.production,
      }
    ),
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
    NgxsStoragePluginModule.forRoot({
      key: [
        'portal_pointOfSales',
        'portal_products',
        'portal_bank_accounts',
        'portal_invoices',
      ],
    }),
    NgxsLoggerPluginModule.forRoot(),
    NgxsRouterPluginModule.forRoot(),
  ],
  providers: [IsSignedInGuard, IsNotSignedInGuard, CanEditUserAccount],
})
export class PortalModule {}
