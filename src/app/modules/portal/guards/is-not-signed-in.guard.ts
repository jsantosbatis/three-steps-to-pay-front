import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';

/**
 * Ensure that the user is not signed in to access to specific screens otherwise it will redirect
 * ro dashboard screen
 */
@Injectable({
  providedIn: 'root',
})
export class IsNotSignedInGuard implements CanActivate {
  constructor(private _auth: AuthService, private _router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    this._auth.isSignedIn().subscribe((isSignedIn) => {
      if (isSignedIn) {
        this._router.navigate(['portal/dashboard']);
      }
    });
    return this._auth.isSignedIn().pipe(
      map((isSignedIn) => {
        return !isSignedIn;
      })
    );
  }
}
