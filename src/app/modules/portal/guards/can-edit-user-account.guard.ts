import { Injectable } from '@angular/core'
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router'
import { AuthService } from '../services/auth.service'
@Injectable({
  providedIn: 'root',
})
export class CanEditUserAccount implements CanActivate {
  constructor(private _auth: AuthService) {}
  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean | UrlTree> {
    const user = await this._auth.getUser()
    return (!!!user.is_admin && next.params['id'] == user.id) || !!user.is_admin
  }
}
