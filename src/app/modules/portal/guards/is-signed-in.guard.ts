import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

/**
 * Ensure that the user is signed in to access to specific screens otherwise redirect to login
 * for identification
 */
@Injectable({
  providedIn: 'root',
})
export class IsSignedInGuard implements CanActivate {
  constructor(private _router: Router, private _auth: AuthService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    this._auth.isSignedIn().subscribe((isSignedIn) => {
      if (!isSignedIn) {
        this._router.navigate(['portal/login']);
      }
    });
    return this._auth.isSignedIn();
  }
}
