export function getCategoryColor(category: string) {
  switch (category) {
    case 'Bougie':
      return '#d53f8c';
    case 'Don':
      return '#ed8936';
    case 'Collecte':
      return '#68d391';
    case 'Article':
      return '#4299e1';
    case 'Ticket':
      return '#5a67d8';
    default:
      break;
  }
}
