export interface SalesPaymentMethodType {
  total: number;
  payment_method_type_name: string;
}
