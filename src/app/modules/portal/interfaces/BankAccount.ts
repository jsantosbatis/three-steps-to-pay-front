export interface BankAccount {
  id?: string;
  name: string;
  number: string;
  created_at?: number;
  updated_at?: number;
}
