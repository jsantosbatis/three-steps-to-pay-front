export interface Invoice {
  id?: string;
  user_id: number;
  file_url: string;
  file_path: string;
  invoice_amount: number;
  sales_amount: number;
  number: string;
  start_date: number;
  end_date: number;
  createdAt?: number;
  updatedAt?: number;
}
