export interface SalesCategory {
  total: number;
  product_category_name: string;
  net_amount: number;
  date: string;
}
