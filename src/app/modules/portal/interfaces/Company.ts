export interface Company {
  name: string;
  vat_number: string;
  street: string;
  postal_code: string;
  city: string;
  created_at?: number;
  updated_at?: number;
}
