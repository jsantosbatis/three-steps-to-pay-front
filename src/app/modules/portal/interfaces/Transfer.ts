export interface Transfer {
  id?: string
  amount: string
  bank_account: string
  bank_name: string
  communication: string
  product_name: string
  treated: boolean
}
