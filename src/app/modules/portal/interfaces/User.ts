import { Company } from './Company';
import { BillingDetails } from './BillingDetails';

export interface User {
  id?: string;
  google_id: string;
  email: string;
  password?: string;
  is_admin: boolean;
  use_company_details_for_billing: boolean;
  company: Company;
  billing_detail?: BillingDetails;
  created_at?: number;
  updated_at?: number;
  deleted_at?: number;
}
