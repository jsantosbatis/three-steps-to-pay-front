export interface PointOfSaleSales {
  id: string;
  name: string;
  sales_amount: number;
  sales_total: number;
}
