export interface Product {
  id?: string;
  name: string;
  price: number;
  product_category_id: number;
  qr_code_url: string;
  point_of_sale_id: string;
  main_percentage: number;
  secondary_bank_account_id?: number;
  secondary_percentage: number;
  secondary_bank_account?: { name: string; number: string };
  created_at?: number;
  updated_at?: number;
  deleted_at?: number;
}
