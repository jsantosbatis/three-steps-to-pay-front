export interface PointOfSale {
  id?: string;
  name: string;
  street: string;
  postal_code: string;
  city: string;
  bank_account: string;
  user_id?: string;
  count_products: number;
  created_at?: number;
  updated_at?: number;
  deleted_at?: number;
}
