export interface TotalSales {
  amount: number;
  total: number;
  net_amount: number;
}
