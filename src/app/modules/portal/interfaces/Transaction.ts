export interface Transaction {
  id?: string;
  amount: number;
  date: string;
  fee: number;
  net_amount: number;
  point_of_sale_name: string;
  product_name: string;
  payment_method_type_id: number;
  payment_methid_type_name: string;
}
