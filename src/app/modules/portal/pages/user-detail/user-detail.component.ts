import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, ParamMap, Router } from '@angular/router'
import { User } from '@portal/interfaces/User'
import { AuthService } from '@portal/services/auth.service'
import { UserService } from '@portal/services/user.service'
import { AlertService } from '@shared/services/alert.service'
import { NgxSpinnerService } from 'ngx-spinner'
import { SUCCESSFULLY_CREATED, SUCCESSFULLY_UPDATED } from 'src/app/lang'

@Component({
  selector: 'app-user-detail',
  template: `
    <app-container>
      <div class="bg-white shadow overflow-hidden  sm:rounded-lg">
        <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
          <form [formGroup]="myForm" (ngSubmit)="save()" novalidate>
            <div>
              <div>
                <div>
                  <h3 class="text-lg leading-6 font-medium text-gray-900">
                    Compte
                  </h3>
                  <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                    Information de connexion
                  </p>
                </div>
                <div class="mt-6 sm:mt-5">
                  <div
                    class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                  >
                    <label
                      for="email"
                      class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                    >
                      Email
                    </label>
                    <div
                      class="mt-1 sm:mt-0 sm:col-span-2  max-w-xs rounded-md shadow-sm"
                    >
                      <input
                        type="email"
                        name="email"
                        id="email"
                        autocomplete="email"
                        formControlName="email"
                        [required]="true"
                        [readOnly]="isEditMode"
                        class="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                        [ngClass]="{
                          'bg-gray-100 cursor-not-allowed': isEditMode
                        }"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div class="mt-8 border-t border-gray-200 pt-8 sm:mt-5 sm:pt-10">
                <div>
                  <h3 class="text-lg leading-6 font-medium text-gray-900">
                    Détails de la société
                  </h3>
                  <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                    Informations destinées à la facturation
                  </p>
                </div>
                <div class="mt-4">
                  <div class="flex items-start">
                    <div class="absolute flex items-center h-5">
                      <input
                        id="billingDetails"
                        type="checkbox"
                        formControlName="use_company_details_for_billing"
                        class="form-checkbox h-4 w-4 text-primary-600 transition duration-150 ease-in-out"
                      />
                    </div>
                    <div class="pl-7 text-sm leading-5">
                      <label
                        for="billingDetails"
                        class="font-medium text-gray-700"
                        >Utiliser les mêmes données pour la facturation des
                        frais</label
                      >
                    </div>
                  </div>
                </div>
                <div class="mt-6 sm:mt-5" formGroupName="company">
                  <app-form-text
                    formControlName="name"
                    placeholder="My company"
                    label="Nom"
                    [required]="true"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="vat_number"
                    placeholder="BE 0737 818 622"
                    label="Numéro de TVA"
                    [required]="true"
                    customMask="SS 0000 000 000"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="street"
                    placeholder="Main street"
                    label="Rue"
                    [required]="true"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="city"
                    placeholder="New York"
                    label="Ville"
                    [required]="true"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="postal_code"
                    placeholder="10005"
                    label="Code postal"
                    [required]="true"
                  >
                  </app-form-text>
                </div>
              </div>

              <div
                class="mt-8 border-t border-gray-200 pt-8 sm:mt-5 sm:pt-10"
                [hidden]="myForm.value.use_company_details_for_billing"
              >
                <div>
                  <h3 class="text-lg leading-6 font-medium text-gray-900">
                    Détails pour la facturation des frais
                  </h3>
                  <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                    Ces données seront utilisées pour réaliser les factures
                  </p>
                </div>
                <div class="mt-6 sm:mt-5" formGroupName="billing_detail">
                  <app-form-text
                    formControlName="name"
                    placeholder="My company"
                    label="Nom"
                    [required]="!myForm.value.use_company_details_for_billing"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="vat_number"
                    placeholder="BE 0737 818 622"
                    label="Numéro de TVA"
                    [required]="!myForm.value.use_company_details_for_billing"
                    customMask="SS 0000 000 000"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="street"
                    placeholder="Main street"
                    label="Rue"
                    [required]="!myForm.value.use_company_details_for_billing"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="city"
                    placeholder="New York"
                    label="Ville"
                    [required]="!myForm.value.use_company_details_for_billing"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="postal_code"
                    placeholder="10005"
                    label="Code postal"
                    [required]="!myForm.value.use_company_details_for_billing"
                  >
                  </app-form-text>
                </div>
              </div>
            </div>
            <div class="mt-8 border-t border-gray-200 pt-5">
              <div class="flex justify-end">
                <span class="inline-flex rounded-md shadow-sm">
                  <button
                    type="button"
                    (click)="cancel()"
                    [hidden]="!hasChange()"
                    class="button is-white"
                  >
                    Annuler
                  </button>
                </span>
                <span class="ml-3 inline-flex rounded-md shadow-sm">
                  <button
                    type="submit"
                    [disabled]="!this.myForm.valid || !hasChange()"
                    class="button is-primary"
                  >
                    Sauvegarder
                  </button>
                </span>
              </div>
            </div>
          </form>
        </div>
      </div>
    </app-container>
  `,
  styleUrls: ['./user-detail.component.scss'],
})
export class UserDetailComponent implements OnInit {
  myForm: FormGroup
  data: User
  isEditMode: boolean
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _authService: AuthService,
    private _spinner: NgxSpinnerService,
    private _alert: AlertService
  ) {}

  ngOnInit() {
    this.myForm = new FormGroup({
      id: new FormControl({ value: '', disabled: false }),
      email: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
      password: new FormControl({ value: '', disabled: false }),
      is_admin: new FormControl({ value: false, disabled: false }, [
        Validators.required,
      ]),
      use_company_details_for_billing: new FormControl(
        { value: true, disabled: false },
        [Validators.required]
      ),
      company: new FormGroup({
        name: new FormControl({ value: '', disabled: false }, [
          Validators.required,
        ]),
        vat_number: new FormControl({ value: '', disabled: false }, [
          Validators.required,
        ]),
        street: new FormControl({ value: '', disabled: false }, [
          Validators.required,
        ]),
        postal_code: new FormControl({ value: '', disabled: false }, [
          Validators.required,
        ]),
        city: new FormControl({ value: '', disabled: false }, [
          Validators.required,
        ]),
      }),
      billing_detail: new FormGroup({
        name: new FormControl({ value: '', disabled: false }, []),
        vat_number: new FormControl({ value: '', disabled: false }, []),
        street: new FormControl({ value: '', disabled: false }, []),
        postal_code: new FormControl({ value: '', disabled: false }, []),
        city: new FormControl({ value: '', disabled: false }, []),
      }),
    })
    this._spinner.show()
    this._route.paramMap.subscribe((params: ParamMap) => {
      if (params.get('id')) {
        this.isEditMode = true
        this._userService.getById(params.get('id')).subscribe((data) => {
          this.initForm(data)
          this._spinner.hide()
        })
      } else {
        this.isEditMode = false
        this._spinner.hide()
      }
    })
  }

  private initForm(data: User) {
    this.data = data
    this.myForm.setValue({
      id: data.id,
      email: data.email,
      password: '',
      is_admin: data.is_admin ?? false,
      use_company_details_for_billing: data.use_company_details_for_billing,
      company: {
        city: data.company.city,
        name: data.company.name,
        postal_code: data.company.postal_code,
        street: data.company.street,
        vat_number: data.company.vat_number,
      },
      billing_detail: {
        city: data.billing_detail ? data.billing_detail.city : null,
        name: data.billing_detail ? data.billing_detail.name : null,
        postal_code: data.billing_detail
          ? data.billing_detail.postal_code
          : null,
        street: data.billing_detail ? data.billing_detail.street : null,
        vat_number: data.billing_detail ? data.billing_detail.vat_number : null,
      },
    })
  }

  async cancel() {
    const isAdmin = await this._authService.isAdmin()
    if (isAdmin) {
      this._router.navigate(['portal/users'])
    } else {
      this._router.navigate(['portal/dashboard'])
    }
  }

  hasChange() {
    if (!this.isEditMode) return true
    if (!this.data) return false
    return (
      this.myForm.value.email != this.data.email ||
      this.myForm.value.is_admin != this.data.is_admin ||
      this.myForm.value.use_company_details_for_billing !=
        this.data.use_company_details_for_billing ||
      this.myForm.value.company.city != this.data.company.city ||
      this.myForm.value.company.name != this.data.company.name ||
      this.myForm.value.company.postal_code != this.data.company.postal_code ||
      this.myForm.value.company.street != this.data.company.street ||
      this.myForm.value.company.vat_number != this.data.company.vat_number ||
      this.myForm.value.billing_detail?.city !=
        this.data.billing_detail?.city ||
      this.myForm.value.billing_detail?.name !=
        this.data.billing_detail?.name ||
      this.myForm.value.billing_detail?.postal_code !=
        this.data.billing_detail?.postal_code ||
      this.myForm.value.billing_detail?.street !=
        this.data.billing_detail?.street ||
      this.myForm.value.billing_detail?.vat_number !=
        this.data.billing_detail?.vat_number
    )
  }
  save() {
    this._spinner.show()
    if (this.myForm.value['id']) {
      this._userService.update(this.myForm.value).subscribe((data) => {
        this.initForm(data)
        this._authService.setCurrentUser(data)
        this._spinner.hide()
        this._alert.success(SUCCESSFULLY_UPDATED)
      })
    } else {
      this._userService.insert(this.myForm.value).subscribe((data) => {
        this._alert.success(SUCCESSFULLY_CREATED)
        this._router.navigate(['portal/users'])
      })
    }
  }
}
