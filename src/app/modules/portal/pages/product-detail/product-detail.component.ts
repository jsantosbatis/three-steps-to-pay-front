import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import {
  SUCCESSFULLY_CANCELLED,
  SUCCESSFULLY_CREATED,
  SUCCESSFULLY_DELETED,
  SUCCESSFULLY_UPDATED,
} from '@lang'
import { Select, Store } from '@ngxs/store'
import { BankAccount } from '@portal/interfaces/BankAccount'
import { PointOfSale } from '@portal/interfaces/PointOfSale'
import { Product } from '@portal/interfaces/Product'
import { ProductCategory } from '@portal/interfaces/ProductCategory'
import { ModalComponent } from '@shared/components/ui/modal/modal.component'
import { AlertService } from '@shared/services/alert.service'
import { NgxSpinnerService } from 'ngx-spinner'
import { Observable, Subscription } from 'rxjs'
import { SetCurrentPointOfSale } from '../../actions/point-of-sales.action'
import {
  AddProduct,
  DeleteProduct,
  EditProduct,
  GetProductCategories,
  SetCurrentProduct,
} from '../../actions/products.action'
import { BankAccountsState } from '../../states/bank-accounts.state'
import { PointOfSalesState } from '../../states/point-of-sales.state'
import { ProductsState } from '../../states/products.state'
@Component({
  selector: 'app-product-detail',
  template: `
    <app-container>
      <div class="bg-white shadow overflow-hidden  sm:rounded-lg">
        <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
          <form [formGroup]="myForm" (ngSubmit)="save()" novalidate>
            <div>
              <h3 class="text-lg leading-6 font-medium text-gray-900">
                Détails
              </h3>
              <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                Ces informations sont nécessaire pour générer le QRCode
              </p>
            </div>
            <div class="mt-6 sm:mt-5">
              <app-form-text
                formControlName="name"
                placeholder="product name"
                label="Nom"
                [required]="true"
              >
              </app-form-text>
              <div
                class="mt-6 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
              >
                <label
                  for="price"
                  class="block text-sm font-medium leading-5 text-gray-700 sm:mt-px sm:pt-2"
                >
                  Prix *
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-xs rounded-md shadow-sm">
                    <input
                      id="price"
                      type="text"
                      placeholder="10,00 €"
                      formControlName="price"
                      currencyMask
                      class="form-input block w-full
                    transition duration-150 ease-in-out sm:text-sm sm:leading-5"
                    />
                  </div>
                </div>
              </div>
              <div
                class="mt-6 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
              >
                <label
                  for="product_category_id"
                  class="block text-sm font-medium leading-5 text-gray-700 sm:mt-px sm:pt-2"
                >
                  Catégorie *
                </label>
                <div class="mt-1 sm:mt-0 sm:col-span-2">
                  <div class="max-w-xs rounded-md shadow-sm">
                    <select
                      class="form-select sm:text-sm sm:leading-5 block w-full"
                      formControlName="product_category_id"
                    >
                      <option
                        *ngFor="
                          let category of productCategories$ | async;
                          let index = index
                        "
                        [ngValue]="category.id"
                      >
                        {{ category.name }}
                      </option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="mt-8">
              <h3 class="text-lg leading-6 font-medium text-gray-900">
                Répartition des montants
              </h3>
              <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                La répartition des montants entre les différents comptes
                bancaires
              </p>
            </div>
            <div
              class="mt-6 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:pt-5 sm:pl-7"
            >
              <label
                class="block text-sm font-medium leading-5 text-gray-700 sm:mt-px sm:pt-2"
              >
                {{ mainDispacher.name }}
                <p class="text-gray-500">
                  {{ mainDispacher.bankAccount | bankAccount }}
                </p>
              </label>
              <div class="mt-1 sm:mt-0 sm:col-span-2">
                <app-input-percent-total
                  [realValue]="myForm.value['price']"
                  formControlName="main_percentage"
                  (change)="changeDispatcher(1)"
                  [readonly]="myForm.value['secondary_bank_account_id'] == null"
                ></app-input-percent-total>
              </div>
            </div>
            <div
              class="mt-6 sm:mt-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:items-center sm:pt-5 sm:pl-7"
            >
              <select
                class="form-select sm:text-sm sm:leading-5 block mr-3 w-full"
                formControlName="secondary_bank_account_id"
                (change)="onSecondaryBankAccountChange()"
              >
                <option [ngValue]="null">Sélectionner un compte</option>
                <option
                  *ngFor="
                    let bankAccount of bankAccounts$ | async;
                    let index = index
                  "
                  [ngValue]="bankAccount.id"
                >
                  {{ bankAccount.name }} -
                  {{ bankAccount.number | bankAccount }}
                </option>
              </select>

              <div class="mt-1 sm:mt-0 sm:col-span-2">
                <app-input-percent-total
                  [realValue]="myForm.value['price']"
                  formControlName="secondary_percentage"
                  (change)="changeDispatcher(2)"
                  [readonly]="myForm.value['secondary_bank_account_id'] == null"
                ></app-input-percent-total>
              </div>
            </div>
            <div class="mt-8 border-t border-gray-200 pt-5">
              <div class="flex justify-between">
                <span class="sm:block hidden">
                  <button
                    type="button"
                    (click)="modalDelete.open()"
                    *ngIf="isEditMode"
                    class="hidden sm:block button is-secondary"
                  >
                    Supprimer
                  </button>
                </span>
                <!-- Empty div to trigger justify-between-->
                <div *ngIf="!isEditMode"></div>
                <div
                  class="flex flex-col sm:flex-row sm:justify-between sm:w-auto w-full sm:space-y-0 sm:space-x-2 space-y-2"
                >
                  <span class="sm:block hidden">
                    <button
                      type="button"
                      (click)="cancel()"
                      [hidden]="!hasChange()"
                      class="button is-white"
                    >
                      Annuler
                    </button>
                  </span>
                  <span class="sm:block hidden">
                    <button
                      type="submit"
                      [disabled]="!this.myForm.valid || !hasChange()"
                      class="button is-primary"
                    >
                      Sauvegarder
                    </button>
                  </span>
                  <!-- Mobile buttons -->
                  <span class="block sm:hidden">
                    <button
                      type="button"
                      (click)="cancel()"
                      [hidden]="!hasChange()"
                      class="button is-white w-full"
                    >
                      Annuler
                    </button>
                  </span>
                  <span class="block sm:hidden">
                    <button
                      type="button"
                      type="submit"
                      [disabled]="!this.myForm.valid || !hasChange()"
                      class="button is-primary w-full"
                    >
                      Sauvegarder
                    </button>
                  </span>
                  <span class="block sm:hidden">
                    <button
                      type="button"
                      (click)="modalDelete.open()"
                      *ngIf="isEditMode"
                      class="button is-secondary w-full"
                    >
                      Supprimer
                    </button>
                  </span>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </app-container>
    <app-modal #modalDelete>
      <div
        modalIconHeader
        class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10"
      >
        <svg
          class="h-6 w-6 text-red-600"
          stroke="currentColor"
          fill="none"
          viewBox="0 0 24 24"
        >
          <path
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width="2"
            d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"
          />
        </svg>
      </div>
      <span modalHeader>Suppression du produit</span>
      <p modalBody class="py-4">Êtes vous certain de supprimer ce produit ?</p>
      <div modalFooter class="mt-5 sm:mt-4 sm:flex sm:flex-row-reverse">
        <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
          <button
            (click)="deleteProduct()"
            type="button"
            class="justify-center w-full button is-danger"
          >
            Supprimer
          </button>
        </span>
        <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
          <button
            (click)="modalDelete.close()"
            type="button"
            class="justify-center w-full button is-white"
          >
            Annuler
          </button>
        </span>
      </div>
    </app-modal>
  `,
})
export class ProductDetailComponent
  implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalComponent
  data: Product
  myForm: FormGroup
  productId = ''
  pointOfSaleId = ''
  mainDispacher: {
    bankAccount: string
    name: string
    percentage: number
  } = {} as any
  secondaryDispacher: {
    bankAccount: string
    name: string
    percentage: number
  } = {} as any

  currentSub: Subscription
  currentPointOfSale: Subscription
  deleteSub: Subscription
  loadingSub: Subscription

  @Select(ProductsState.isLoading) isLoading$: Observable<boolean>
  @Select(ProductsState.current) current$: Observable<Product>
  @Select(ProductsState.productCategories) productCategories$: Observable<
    ProductCategory[]
  >
  @Select(BankAccountsState.items) bankAccounts$: Observable<BankAccount[]>
  @Select(PointOfSalesState.current)
  currentPointOfSale$: Observable<PointOfSale>

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _spinner: NgxSpinnerService,
    private _alert: AlertService,
    private _store: Store
  ) {}

  isEditMode = false

  ngOnInit() {
    this.myForm = new FormGroup({
      id: new FormControl({ value: '', disabled: false }, []),
      name: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
      price: new FormControl({ value: '', disabled: false }, [
        Validators.required,
        Validators.min(1.0),
      ]),
      product_category_id: new FormControl({ value: '', disabled: false }, []),
      point_of_sale_id: new FormControl({ value: '', disabled: false }, []),
      main_percentage: new FormControl({ value: '', disabled: false }, [
        Validators.max(100),
      ]),
      secondary_bank_account_id: new FormControl({
        value: '',
        disabled: false,
      }),
      secondary_percentage: new FormControl({ value: '', disabled: false }, [
        Validators.max(100),
      ]),
    })

    this._route.paramMap.subscribe((params) => {
      this.isEditMode = params.get('productId') != null
      this.productId = params.get('productId')
      this.pointOfSaleId = params.get('pointOfSaleId')

      this._store.dispatch([
        new SetCurrentProduct(this.productId),
        new SetCurrentPointOfSale(this.pointOfSaleId),
        new GetProductCategories(),
      ])

      this.currentSub = this.current$.subscribe((data) => {
        if (this.isEditMode) {
          this.initForm(data)
        } else {
          this.data = {
            point_of_sale_id: this.pointOfSaleId,
            price: 1,
            main_percentage: 100,
            secondary_percentage: 0,
            secondary_bank_account_id: null,
            product_category_id: 1,
          } as any
          this.myForm.reset(this.data)
        }
      })

      this.currentPointOfSale = this.currentPointOfSale$.subscribe((data) => {
        this.mainDispacher.name = data.name
        this.mainDispacher.bankAccount = data.bank_account
        this.secondaryDispacher.percentage = 0
      })
    })
  }

  ngOnDestroy(): void {
    this.currentSub ? this.currentSub.unsubscribe() : null
    this.currentPointOfSale ? this.currentPointOfSale.unsubscribe() : null
    this.deleteSub ? this.deleteSub.unsubscribe() : null
    this.loadingSub ? this.loadingSub.unsubscribe() : null
  }

  ngAfterViewInit(): void {
    this.loadingSub = this.isLoading$.subscribe((isLoading) => {
      isLoading ? this._spinner.show() : this._spinner.hide()
    })
  }

  private initForm(data: Product) {
    if (!data) return
    this.data = data
    this.myForm.setValue({
      id: data.id,
      price: data.price,
      product_category_id: data.product_category_id,
      name: data.name,
      point_of_sale_id: data.point_of_sale_id,
      main_percentage: data.main_percentage,
      secondary_percentage: data.secondary_percentage,
      secondary_bank_account_id: data.secondary_bank_account_id,
    })
  }

  onSecondaryBankAccountChange() {
    if (this.myForm.get('secondary_bank_account_id').value == null) {
      this.myForm.get('main_percentage').setValue(100)
      this.changeDispatcher(1)
    }
  }

  save() {
    if (this.isEditMode) {
      this._store.dispatch(new EditProduct(this.myForm.value)).subscribe(() => {
        this._alert.success(SUCCESSFULLY_UPDATED)
        this.data = this.myForm.value
      })
    } else {
      this._store.dispatch(new AddProduct(this.myForm.value)).subscribe(() => {
        this._alert.success(SUCCESSFULLY_CREATED)
      })
    }
  }

  changeDispatcher(dispatcherIndex: number) {
    if (dispatcherIndex == 1) {
      if (
        this.myForm.get('main_percentage').value > 100 ||
        typeof this.myForm.get('main_percentage').value !== 'number'
      ) {
        this.myForm.get('main_percentage').setValue(100)
      }

      this.myForm
        .get('secondary_percentage')
        .setValue(
          100 -
            (this.myForm.get('main_percentage').value > 100
              ? 100
              : this.myForm.get('main_percentage').value)
        )
    } else {
      if (
        this.myForm.get('secondary_percentage').value > 100 ||
        typeof this.myForm.get('main_percentage').value !== 'number'
      ) {
        this.myForm.get('secondary_percentage').setValue(100)
      }
      this.myForm
        .get('main_percentage')
        .setValue(
          100 -
            (this.myForm.get('secondary_percentage').value > 100
              ? 100
              : this.myForm.get('secondary_percentage').value)
        )
    }
  }

  hasChange() {
    if (!this.data) return false
    return (
      this.myForm.value.price !== this.data.price ||
      this.myForm.value.product_category_id !== this.data.product_category_id ||
      this.myForm.value.name !== this.data.name ||
      this.myForm.value.main_percentage !== this.data.main_percentage ||
      this.myForm.value.secondary_percentage !==
        this.data.secondary_percentage ||
      this.myForm.value.secondary_bank_account_id !==
        this.data.secondary_bank_account_id
    )
  }

  cancel() {
    this._alert.success(SUCCESSFULLY_CANCELLED)
    this._router.navigate([
      'portal/point-of-sale',
      this.pointOfSaleId,
      'catalog',
    ])
  }

  deleteProductModal() {
    this.modalDelete.toggle()
  }

  deleteProduct() {
    this.deleteSub = this._store
      .dispatch(new DeleteProduct(this.productId))
      .subscribe(
        () => {
          this._router.navigate([
            'portal/point-of-sale',
            this.pointOfSaleId,
            'catalog',
          ])
          this._alert.success(SUCCESSFULLY_DELETED)
        },
        () => {
          this.modalDelete.close()
        }
      )
  }
}
