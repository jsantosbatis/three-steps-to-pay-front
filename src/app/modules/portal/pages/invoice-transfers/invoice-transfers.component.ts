import { Component, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Transfer } from '@portal/interfaces/Transfer'
import { AuthService } from '@portal/services/auth.service'
import { InvoiceService } from '@portal/services/invoice.service'
import { TransferService } from '@portal/services/transfer.service'
import { NgxSpinnerService } from 'ngx-spinner'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-invoice-transfers',
  template: `
    <app-container extraClass="pt-5">
      <h2
        class="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:leading-9 sm:truncate"
      >
        Transfert
      </h2>
      <div class="my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8">
        <div
          class="align-middle inline-block min-w-full shadow-xs overflow-hidden sm:rounded-lg border-b border-gray-200"
        >
          <table class="min-w-full divide-y divide-gray-200">
            <thead>
              <tr>
                <th
                  class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                >
                  compte bancaire
                </th>
                <th
                  class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                >
                  communication
                </th>
                <th
                  class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                >
                  montant
                </th>
                <th
                  class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                >
                  traité
                </th>
              </tr>
            </thead>
            <tbody class="bg-white divide-y divide-gray-200">
              <tr class="bg-white" *ngIf="items?.length == 0">
                <td colspan="100" class="text-center py-5">
                  <p class="text-2xl text-gray-500">Aucune donnée disponible</p>
                </td>
              </tr>
              <tr *ngFor="let transfer of items">
                <td
                  class="px-6 py-3 whitespace-no-wrap border-b border-gray-200"
                >
                  <div class="flex items-center">
                    <div>
                      <div class="text-sm leading-5 font-medium text-gray-900">
                        {{ transfer.bank_name }}
                      </div>
                      <div class="text-sm leading-5 text-gray-500">
                        {{ transfer.bank_account | bankAccount }}
                      </div>
                    </div>
                  </div>
                </td>
                <td
                  class="px-6 py-3 whitespace-no-wrap border-b border-gray-200"
                >
                  <div class="text-sm leading-5 text-gray-900">
                    {{ transfer.product_name }}
                  </div>
                  <div class="text-sm leading-5 text-gray-500">
                    {{ transfer.communication }}
                  </div>
                </td>
                <td
                  class="px-6 py-3 whitespace-no-wrap border-b border-gray-200 text-primary-600 font-medium"
                >
                  {{ transfer.amount }}
                </td>
                <td
                  class="px-6 py-3 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  (click)="treated(transfer.id)"
                >
                  <span
                    class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                    *ngIf="transfer.treated"
                  >
                    Oui
                  </span>
                  <span
                    class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                    *ngIf="!transfer.treated"
                  >
                    Non
                  </span>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </app-container>
  `,
  styleUrls: ['./invoice-transfers.component.scss'],
})
export class InvoiceTransfersComponent implements OnInit, OnDestroy {
  constructor(
    private _route: ActivatedRoute,
    private _auth: AuthService,
    private _invoice: InvoiceService,
    private _transfer: TransferService,
    private _spinner: NgxSpinnerService
  ) {}

  items: Transfer[]
  sub: Subscription
  ngOnInit(): void {
    this._spinner.show()
    this._route.paramMap.subscribe((params) => {
      this.sub = this._invoice
        .getTransfers(params.get('id'))
        .subscribe((data) => {
          this.items = data
          this._spinner.hide()
        })
    })
  }

  treated(transferId: string): void {
    this._spinner.show()
    this._auth
      .isAdmin()
      .then((isAdmin) => {
        if (isAdmin) {
          this._transfer.treated(transferId).subscribe((data) => {
            this.items = this.items.map((item) => {
              if (item.id === transferId) {
                return data
              }
              return item
            })
            this._spinner.hide()
          })
        } else {
          this._spinner.hide()
        }
      })
      .catch(() => {
        this._spinner.hide()
      })
  }

  ngOnDestroy(): void {
    this.sub ? this.sub.unsubscribe() : null
  }
}
