import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { EMAIL_SENT, SUCCESSFULLY_DELETED } from '@lang';
import { User } from '@portal/interfaces/User';
import { UserService } from '@portal/services/user.service';
import { AuthService } from '@portal/services/auth.service';
import { ModalComponent } from '@shared/components/ui/modal/modal.component';
import { AlertService } from '@shared/services/alert.service';
@Component({
  selector: 'app-users',
  template: `
    <app-container>
      <app-header title="Utilisateur"></app-header>
      <div class="w-full mb-2 flex justify-end">
        <button
          type="button"
          (click)="add()"
          class="button is-primary is-medium"
        >
          <svg
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            class="-ml-1 mr-2 h-5 w-5"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M12 4v16m8-8H4"
            />
          </svg>
          Utilisateur
        </button>
      </div>
      <div class="flex flex-col">
        <div
          class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8"
        >
          <div
            class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200"
          >
            <table class="min-w-full">
              <thead>
                <tr>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    nom
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    date de création
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    admin
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50"
                  ></th>
                </tr>
              </thead>
              <tbody class="bg-white">
                <tr class="bg-white" *ngIf="users.length == 0">
                  <td colspan="100">
                    <app-empty-data></app-empty-data>
                  </td>
                </tr>
                <tr *ngFor="let user of users">
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200"
                  >
                    <div class="flex items-center">
                      <div class="flex-shrink-0 h-10 w-10">
                        <span
                          class="inline-block h-10 w-10 rounded-full overflow-hidden bg-gray-100"
                        >
                          <svg
                            class="h-full w-full text-gray-300"
                            fill="currentColor"
                            viewBox="0 0 24 24"
                          >
                            <path
                              d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z"
                            />
                          </svg>
                        </span>
                      </div>
                      <div class="ml-4">
                        <div
                          class="text-sm leading-5 font-medium text-gray-900"
                        >
                          {{ user.company.name }}
                        </div>
                        <div class="text-sm leading-5 text-gray-500">
                          {{ user.email }}
                        </div>
                      </div>
                    </div>
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    {{ user.created_at | date: 'dd-MM-yy H:mm' | empty }}
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200"
                  >
                    <span
                      *ngIf="!user.isAdmin"
                      class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-red-100 text-red-800"
                    >
                      non
                    </span>
                    <span
                      *ngIf="user.isAdmin"
                      class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                    >
                      oui
                    </span>
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap text-right border-b border-gray-200 text-sm leading-5 font-medium"
                  >
                    <div class="flex justify-end">
                      <div
                        (click)="edit(user.id)"
                        class="mr-3 cursor-pointer
                      text-primary-600 hover:text-primary-900"
                        title="Modifier"
                      >
                        <svg
                          class="-ml-1 mr-2 h-5 w-5"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z"
                          />
                        </svg>
                      </div>

                      <div
                        (click)="sendPasswordResetLink(user)"
                        class="mr-3 cursor-pointer
                      text-primary-600 hover:text-primary-900"
                        title="Réinitialiser mot de passe"
                      >
                        <svg
                          class="-ml-1 mr-2 h-5 w-5"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M8 11V7a4 4 0 118 0m-4 8v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2z"
                          />
                        </svg>
                      </div>
                      <div
                        (click)="deleteUserModal(user)"
                        class="mr-3 cursor-pointer
                        text-primary-600 hover:text-primary-900"
                        title="Supprimer"
                      >
                        <svg
                          class="-ml-1 mr-2 h-5 w-5"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
                          />
                        </svg>
                      </div>
                      <div
                        (click)="connectAs(user.id)"
                        class="mr-3 cursor-pointer
                        text-primary-600 hover:text-primary-900"
                        title="Connexion en tant que"
                      >
                        <svg
                          class="-ml-1 mr-2 h-5 w-5"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            stroke-linecap="round"
                            stroke-linejoin="round"
                            stroke-width="2"
                            d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z"
                          />
                        </svg>
                      </div>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <app-modal #modalDelete>
        <div
          modalIconHeader
          class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10"
        >
          <svg
            class="h-6 w-6 text-red-600"
            stroke="currentColor"
            fill="none"
            viewBox="0 0 24 24"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"
            />
          </svg>
        </div>
        <span modalHeader>Suppression d'utilisateur</span>
        <p modalBody class="py-4">
          Are you sure that you want to delete this user ?
        </p>
        <div modalFooter class="mt-5 sm:mt-4 sm:flex sm:flex-row-reverse">
          <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
            <button
              (click)="deleteUser()"
              type="button"
              class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-red transition ease-in-out duration-150 sm:text-sm sm:leading-5"
            >
              Supprimer
            </button>
          </span>
          <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
            <button
              (click)="modalDelete.close()"
              type="button"
              class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline transition ease-in-out duration-150 sm:text-sm sm:leading-5"
            >
              Annuler
            </button>
          </span>
        </div>
      </app-modal>
    </app-container>
  `,
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  @ViewChild('modal', { static: false }) modal: ModalComponent;
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalComponent;
  users: User[] = [];
  userToDelete: User = null;
  constructor(
    private _userService: UserService,
    private _authService: AuthService,
    private _router: Router,
    private _spinner: NgxSpinnerService,
    private _alert: AlertService
  ) {}

  ngOnInit() {
    this._spinner.show();
    this._userService.getAll().subscribe((data) => {
      this.users = data;
      this._spinner.hide();
    });
  }

  add() {
    this._router.navigate(['portal/user']);
  }

  edit(id) {
    this._router.navigate(['portal/user', id]);
  }

  sendPasswordResetLink(user) {
    this._spinner.show();
    this._userService.sendPasswordResetLink(user).subscribe((result) => {
      this._alert.success(EMAIL_SENT);
      this._spinner.hide();
    });
  }

  connectAs(userId) {
    this._userService.getById(userId).subscribe((data) => {
      this._authService.connectedAs = data;
      this._router.navigate(['portal/dashboard']);
    });
  }

  deleteUserModal(user) {
    this.userToDelete = user;
    this.modalDelete.toggle();
  }

  deleteUser() {
    this._userService.delete(this.userToDelete.id).subscribe(() => {
      this.modalDelete.close();
      this.users.splice(this.users.indexOf(this.userToDelete), 1);
      this.userToDelete = null;
      this._alert.success(SUCCESSFULLY_DELETED);
    });
  }
}
