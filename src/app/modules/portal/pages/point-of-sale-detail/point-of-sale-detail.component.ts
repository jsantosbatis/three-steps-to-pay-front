import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import {
  SUCCESSFULLY_CANCELLED,
  SUCCESSFULLY_CREATED,
  SUCCESSFULLY_DELETED,
  SUCCESSFULLY_UPDATED,
} from '@lang'
import { Select, Store } from '@ngxs/store'
import { PointOfSale } from '@portal/interfaces/PointOfSale'
import { ModalComponent } from '@shared/components/ui/modal/modal.component'
import { AlertService } from '@shared/services/alert.service'
import { NgxSpinnerService } from 'ngx-spinner'
import { Observable, Subscription } from 'rxjs'
import {
  AddPointOfSale,
  DeletePointOfSale,
  EditPointOfSale,
  SetCurrentPointOfSale,
} from '../../actions/point-of-sales.action'
import { PointOfSalesState } from '../../states/point-of-sales.state'

@Component({
  selector: 'app-point-of-sale-detail',
  template: `
    <app-container>
      <div class="bg-white shadow overflow-hidden  sm:rounded-lg">
        <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
          <form [formGroup]="myForm" (ngSubmit)="save()" novalidate>
            <div>
              <div>
                <div>
                  <h3 class="text-lg leading-6 font-medium text-gray-900">
                    Détails
                  </h3>
                  <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                    Ces informations ne seront pas publique
                  </p>
                </div>
                <div class="mt-6 sm:mt-5">
                  <app-form-text
                    formControlName="name"
                    placeholder="Item shop"
                    label="Nom"
                    [required]="true"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="street"
                    placeholder="Main street"
                    label="Rue"
                    [required]="true"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="city"
                    placeholder="New York"
                    label="Ville"
                    [required]="true"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="postal_code"
                    placeholder="10005"
                    label="Code postal"
                    [required]="true"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="bank_account"
                    placeholder="BE98 9281 9281 9281"
                    label="Compte bancaire"
                    customMask="SS00 0000 0000 0000"
                    [required]="true"
                  >
                  </app-form-text>
                </div>
              </div>
            </div>
            <div class="mt-8 border-t border-gray-200 pt-5">
              <div class="flex justify-between">
                <span class="sm:block hidden">
                  <button
                    type="button"
                    (click)="modalDelete.open()"
                    *ngIf="isEditMode()"
                    class="hidden sm:block button is-secondary"
                  >
                    Supprimer
                  </button>
                </span>

                <!-- Empty div to trigger justify-between-->
                <div *ngIf="!isEditMode()"></div>
                <div
                  class="flex flex-col sm:flex-row sm:justify-between sm:w-auto w-full space-y-2 sm:space-y-0 sm:space-x-2"
                >
                  <span class="sm:block hidden">
                    <button
                      type="button"
                      class="button is-white"
                      (click)="cancel()"
                      [hidden]="!hasChange()"
                    >
                      Annuler
                    </button>
                  </span>
                  <span class="sm:block hidden">
                    <button
                      type="submit"
                      [disabled]="!this.myForm.valid || !hasChange()"
                      class="button is-primary"
                    >
                      Sauvegarder
                    </button>
                  </span>
                  <!-- Mobile buttons -->
                  <span class="block sm:hidden">
                    <button
                      type="button"
                      class="button is-white w-full"
                      (click)="cancel()"
                      [hidden]="!hasChange()"
                    >
                      Annuler
                    </button>
                  </span>
                  <span class="block sm:hidden">
                    <button
                      type="button"
                      type="submit"
                      [disabled]="!this.myForm.valid || !hasChange()"
                      class="button is-primary w-full"
                    >
                      Sauvegarder
                    </button>
                  </span>
                  <span class="block sm:hidden">
                    <button
                      type="button"
                      (click)="modalDelete.open()"
                      *ngIf="isEditMode()"
                      class="button is-secondary w-full"
                    >
                      Supprimer
                    </button>
                  </span>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </app-container>
    <app-modal #modalDelete>
      <div
        modalIconHeader
        class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10"
      >
        <svg
          class="h-6 w-6 text-red-600"
          stroke="currentColor"
          fill="none"
          viewBox="0 0 24 24"
        >
          <path
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width="2"
            d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"
          />
        </svg>
      </div>
      <span modalHeader>Supprimer le point de vente</span>
      <p modalBody class="py-4">
        Êtes vous certain de vouloir supprimer ce point de vente ?
      </p>
      <div modalFooter class="mt-5 sm:mt-4 sm:flex sm:flex-row-reverse">
        <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
          <button
            type="button"
            (click)="delete()"
            class="justify-center w-full button is-danger"
          >
            Supprimer
          </button>
        </span>
        <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
          <button
            (click)="modalDelete.close()"
            type="button"
            class="justify-center w-full button is-white"
          >
            Annuler
          </button>
        </span>
      </div>
    </app-modal>
  `,
  styleUrls: ['./point-of-sale-detail.component.scss'],
})
export class PointOfSaleDetailComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalComponent
  data: PointOfSale
  myForm: FormGroup
  addSub: Subscription
  editSub: Subscription
  deleteSub: Subscription
  loadingSub: Subscription
  currentSub: Subscription

  @Select(PointOfSalesState.isLoading) isLoading$: Observable<boolean>
  @Select(PointOfSalesState.current) current$: Observable<PointOfSale>

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _spinner: NgxSpinnerService,
    private _alert: AlertService,
    private _store: Store
  ) {}

  ngOnDestroy(): void {
    this.addSub ? this.addSub.unsubscribe() : null
    this.editSub ? this.editSub.unsubscribe() : null
    this.loadingSub ? this.loadingSub.unsubscribe() : null
    this.currentSub ? this.currentSub.unsubscribe() : null
    this.deleteSub ? this.deleteSub.unsubscribe() : null
  }

  ngOnInit() {
    this.myForm = new FormGroup({
      id: new FormControl({ value: '', disabled: false }),
      name: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
      street: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
      city: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
      postal_code: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
      bank_account: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
    })

    this.currentSub = this.current$.subscribe((data) => {
      this.initForm(data)
      this._resetFormStatus()
    })

    this._route.parent.paramMap.subscribe((params) => {
      this._store.dispatch(new SetCurrentPointOfSale(params.get('id')))
    })
  }

  ngAfterViewInit(): void {
    this.loadingSub = this.isLoading$.subscribe((isLoading) => {
      isLoading ? this._spinner.show() : this._spinner.hide()
    })
  }

  private initForm(result: PointOfSale) {
    this.data = result
    if (result && result.id) {
      this.myForm.setValue({
        id: result.id,
        bank_account: result.bank_account,
        city: result.city,
        name: result.name,
        postal_code: result.postal_code,
        street: result.street,
      } as PointOfSale)
    } else {
      this.myForm.reset({})
    }
  }

  isEditMode() {
    return !!this.myForm.value.id
  }

  hasChange() {
    if (!this.isEditMode()) return true
    if (!this.data) return false
    return (
      this.myForm.value.bank_account != this.data.bank_account ||
      this.myForm.value.city != this.data.city ||
      this.myForm.value.name != this.data.name ||
      this.myForm.value.postal_code != this.data.postal_code ||
      this.myForm.value.street != this.data.street
    )
  }

  save() {
    if (this.myForm.value['id']) {
      this.editSub = this._store
        .dispatch(new EditPointOfSale(this.myForm.value))
        .subscribe(() => {
          this._resetFormStatus()
          this._alert.success(SUCCESSFULLY_UPDATED)
        })
    } else {
      this.addSub = this._store
        .dispatch(new AddPointOfSale(this.myForm.value))
        .subscribe(() => {
          this._alert.success(SUCCESSFULLY_CREATED)
        })
    }
  }

  cancel() {
    if (this.myForm.value['id']) {
      this.myForm.patchValue(this.data)
      this._alert.success(SUCCESSFULLY_CANCELLED)
    } else {
      this._router.navigate(['portal/point-of-sales'])
    }
  }

  delete() {
    this.deleteSub = this._store
      .dispatch(new DeletePointOfSale(this.data.id))
      .subscribe(
        () => {
          this.modalDelete.close()
          this._router.navigate(['portal/point-of-sales'])
          this._alert.success(SUCCESSFULLY_DELETED)
        },
        () => {
          this.modalDelete.close()
        }
      )
  }

  private _resetFormStatus() {
    this.myForm.markAsPristine()
    this.myForm.markAsUntouched()
  }
}
