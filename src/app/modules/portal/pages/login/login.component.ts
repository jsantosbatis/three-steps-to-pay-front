import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { AuthService } from '@portal/services/auth.service'
@Component({
  selector: 'app-login',
  template: `
    <div
      class="min-h-screen bg-gray-50 flex flex-col justify-center py-12 sm:px-6 lg:px-8"
    >
      <div class="sm:mx-auto sm:w-full sm:max-w-md">
        <div class="flex items-center justify-center">
          <img
            class="h-32 w-auto"
            src="assets/icon-app.png"
            alt="3 Steps To Pay"
          />
        </div>
        <h2
          class="mt-6 text-center text-3xl leading-9 font-extrabold text-gray-900"
        >
          Identifiez vous
        </h2>
      </div>

      <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
        <div class="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
          <form [formGroup]="myForm" (ngSubmit)="login()" novalidate>
            <app-form-text
              formControlName="email"
              placeholder="Email"
              label="Email"
              direction="vertical"
              [fullWidth]="true"
              [marginTop]="false"
            ></app-form-text>
            <app-form-text
              formControlName="password"
              placeholder="Password"
              label="Mot de passe"
              type="password"
              direction="vertical"
              [fullWidth]="true"
            >
            </app-form-text>
            <div class="mt-6" [hidden]="true">
              <div class="text-sm leading-5">
                <a
                  href="#"
                  class="font-medium text-primary-600 hover:text-primary-500 focus:outline-none focus:underline transition ease-in-out duration-150"
                >
                  Mot de passe oublié ?
                </a>
              </div>
            </div>

            <div class="mt-6">
              <span class="block w-full rounded-md shadow-sm">
                <button
                  type="submit"
                  [disabled]="!this.myForm.valid"
                  class="w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-primary-600 hover:bg-primary-500 focus:outline-none focus:border-primary-700 focus:shadow-outline-primary active:bg-primary-700 transition duration-150 ease-in-out disabled:bg-primary-300 disabled:cursor-not-allowed"
                >
                  Connexion
                </button>
              </span>
            </div>
          </form>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  myForm: FormGroup

  constructor(private _router: Router, private _auth: AuthService) {}

  ngOnInit() {
    this.myForm = new FormGroup({
      email: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
      password: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
    })
  }

  login() {
    this._auth
      .signInWithEmailAndPassword(
        this.myForm.value.email,
        this.myForm.value.password
      )
      .then((result) => {
        this._router.navigate(['portal/dashboard'])
      })
      .catch(() => {
        alert('Identifiant incorrect')
      })
  }
}
