import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { PointOfSale } from '@portal/interfaces/PointOfSale';
import { Observable } from 'rxjs';
import { PointOfSaleService } from '@portal/services/point-of-sale.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { saveAs } from 'file-saver';
import { Select, Store } from '@ngxs/store';
import { PointOfSalesState } from '../../states/point-of-sales.state';
import { SetCurrentPointOfSale } from '../../actions/point-of-sales.action';

@Component({
  selector: 'app-point-of-sale',
  template: `
    <app-container extraClass="pt-5">
      <div class="flex flex-col md:flex-row md:items-center mb-4">
        <h2
          class="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:leading-9 sm:truncate"
        >
          {{ (current$ | async)?.name }}
        </h2>
        <div class="mt-4 flex md:mt-0 md:ml-4">
          <button
            type="button"
            class="button is-white is-small"
            (click)="downloadQRCode()"
          >
            <svg
              fill="none"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              viewBox="0 0 24 24"
              stroke="currentColor"
              class="-ml-1 mr-2 h-4 w-4"
            >
              <path
                d="M12 4v1m6 11h2m-6 0h-2v4m0-11v3m0 0h.01M12 12h4.01M16 20h4M4 12h4m12 0h.01M5 8h2a1 1 0 001-1V5a1 1 0 00-1-1H5a1 1 0 00-1 1v2a1 1 0 001 1zm12 0h2a1 1 0 001-1V5a1 1 0 00-1-1h-2a1 1 0 00-1 1v2a1 1 0 001 1zM5 20h2a1 1 0 001-1v-2a1 1 0 00-1-1H5a1 1 0 00-1 1v2a1 1 0 001 1z"
              ></path>
            </svg>
            Lien vers boutique
          </button>
        </div>
      </div>
      <div class="sm:hidden">
        <select
          class="form-select block w-full"
          [(ngModel)]="crtRoute"
          (ngModelChange)="navigate()"
        >
          <option value="detail">Détails</option>
          <option value="catalog">Catalogue</option>
          <option value="transaction">Transaction</option>
        </select>
      </div>
      <div class="hidden sm:block">
        <nav class="flex">
          <a
            [routerLink]="['detail']"
            [routerLinkActive]="['text-gray-800', 'bg-gray-200']"
            class="px-3 py-2 font-medium text-sm leading-5 rounded-md text-gray-600 hover:text-gray-800 focus:outline-none focus:text-gray-800 focus:bg-gray-200"
          >
            Détails
          </a>
          <a
            [routerLink]="['catalog']"
            [routerLinkActive]="['text-gray-800', 'bg-gray-200']"
            class="ml-4 px-3 py-2 font-medium text-sm leading-5 rounded-md text-gray-600 hover:text-gray-800 focus:outline-none focus:text-gray-800 focus:bg-gray-200"
          >
            Catalogue
          </a>
          <a
            [routerLink]="['transaction']"
            [routerLinkActive]="['text-gray-800', 'bg-gray-200']"
            class="ml-4 px-3 py-2 font-medium text-sm leading-5 rounded-md text-gray-600 hover:text-gray-800 focus:outline-none focus:text-gray-800 focus:bg-gray-200"
          >
            Transaction
          </a>
        </nav>
      </div>
    </app-container>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./point-of-sale.component.scss'],
})
export class PointOfSaleComponent implements OnInit, OnDestroy {
  crtRoute = 'detail';
  pointOfSaleId = '';
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _store: Store,
    private _pointOfSaleService: PointOfSaleService,

    private _spinner: NgxSpinnerService
  ) {}

  @Select(PointOfSalesState.current) current$: Observable<PointOfSale>;

  ngOnInit() {
    this._router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.crtRoute = event.url.split('/')[4];
      }
    });

    //Only called at the first navigation to the children
    this._route.firstChild.url.subscribe((url) => {
      this.crtRoute = url.toString();
    });

    this._route.paramMap.subscribe((params) => {
      this.pointOfSaleId = params.get('id');
      this._store.dispatch(new SetCurrentPointOfSale(this.pointOfSaleId));
    });
  }

  downloadQRCode() {
    this._spinner.show();
    this._pointOfSaleService.generateQRCode(this.pointOfSaleId).subscribe({
      next: (content) => {
        const blob = new Blob([content], { type: 'image/png' });
        saveAs(blob, `qr_code_${this.pointOfSaleId}.png`);
        this._spinner.hide();
      },
      error: () => this._spinner.hide()
    }
    );
  }

  navigate() {
    this._router.navigate([
      'app/point-of-sale',
      this.pointOfSaleId,
      this.crtRoute,
    ]);
  }

  ngOnDestroy() {}
}
