import { HttpErrorResponse } from '@angular/common/http'
import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { AuthService } from '@portal/services/auth.service'
import { UserService } from '@portal/services/user.service'

@Component({
  selector: 'app-reset-password',
  template: `
    <div
      class="min-h-screen bg-gray-50 flex flex-col justify-center py-12 sm:px-6 lg:px-8"
    >
      <div class="sm:mx-auto sm:w-full sm:max-w-md">
        <div class="flex items-center justify-center">
          <img
            class="h-32 w-auto"
            src="assets/icon-app.png"
            alt="3 Steps To Pay"
          />
        </div>
        <h2
          class="mt-6 text-center text-3xl leading-9 font-extrabold text-gray-900"
        >
          Réinitialisation de votre mot de passe
        </h2>
      </div>

      <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
        <div class="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
          <form [formGroup]="myForm" (ngSubmit)="reset()" novalidate>
            <app-form-text
              formControlName="email"
              placeholder="Email"
              label="Email"
              direction="vertical"
              [fullWidth]="true"
              [marginTop]="false"
              readonly="true"
            ></app-form-text>
            <app-form-text
              formControlName="password"
              placeholder="Password"
              label="Mot de passe"
              type="password"
              direction="vertical"
              [fullWidth]="true"
            >
            </app-form-text>
            <div class="mt-6">
              <span class="block w-full rounded-md shadow-sm">
                <button
                  type="submit"
                  [disabled]="!this.myForm.valid"
                  class="w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-primary-600 hover:bg-primary-500 focus:outline-none focus:border-primary-700 focus:shadow-outline-primary active:bg-primary-700 transition duration-150 ease-in-out disabled:bg-primary-300 disabled:cursor-not-allowed"
                >
                  Réinitialiser
                </button>
              </span>
            </div>
          </form>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {
  myForm: FormGroup
  oobCode: string
  constructor(
    private _auth: AuthService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
  ) {}

  ngOnInit() {
    this._auth.signOut(false)
    this.myForm = new FormGroup({
      email: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
      password: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
    })

    this._route.queryParams.subscribe((params) => {
      this._auth.checkResetPasswordCode(params.oobCode).then((info) => {
        if (info.isValid) {
          this.oobCode = params.oobCode
          this.myForm.setValue({
            email: info.email,
            password: '',
          })
        } else {
          alert('Le lien a expiré, veuillez vous identifier')
          this._router.navigate(['portal', 'login'])
        }
      })
    })
  }

  reset() {
    this._userService
      .resetPassword(this.oobCode, this.myForm.value['password'])
      .subscribe(
        () => {
          this._router.navigate(['portal', 'login'])
        },
        (error: HttpErrorResponse) => {
          alert(error.error.errors)
        }
      )
  }
}
