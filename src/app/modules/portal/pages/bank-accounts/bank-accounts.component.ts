import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { BankAccountsState } from '../../states/bank-accounts.state';
import { Observable, Subscription } from 'rxjs';
import { BankAccount } from '@portal/interfaces/BankAccount';
import { Select, Store } from '@ngxs/store';
import { GetAll } from '../../actions/bank-accounts.action';

@Component({
  selector: 'app-bank-accounts',
  template: `
    <app-container>
      <app-header title="Compte bancaire"></app-header>
      <div class="w-full mb-2 flex justify-end">
        <button
          type="button"
          (click)="addBankAccount()"
          class="button is-primary is-medium"
        >
          <svg
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            class="-ml-1 mr-2 h-5 w-5"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M12 4v16m8-8H4"
            />
          </svg>
          Compte bancaire
        </button>
      </div>
      <app-empty-data *ngIf="(items$ | async)?.length == 0"></app-empty-data>
      <div class="grid grid-cols-1 md:grid-cols-3 gap-4">
        <div *ngFor="let bankAccount of items$ | async">
          <app-bank-account-tile [data]="bankAccount"></app-bank-account-tile>
        </div>
      </div>
    </app-container>
  `,
  styleUrls: ['./bank-accounts.component.scss'],
})
export class BankAccountsComponent implements OnInit, AfterViewInit, OnDestroy {
  subLoading: Subscription;
  @Select(BankAccountsState.items) items$: Observable<BankAccount[]>;
  @Select(BankAccountsState.isLoading) isLoading$: Observable<boolean>;

  constructor(
    private _store: Store,
    private _spinner: NgxSpinnerService,
    private _router: Router
  ) {}

  ngOnDestroy(): void {
    this.subLoading ? this.subLoading.unsubscribe() : null;
  }

  ngOnInit() {
    this._store.dispatch(new GetAll());
  }

  ngAfterViewInit(): void {
    this.subLoading = this.isLoading$.subscribe((isLoading) => {
      isLoading ? this._spinner.show() : this._spinner.hide();
    });
  }

  addBankAccount() {
    this._router.navigate(['portal/bank-account']);
  }
}
