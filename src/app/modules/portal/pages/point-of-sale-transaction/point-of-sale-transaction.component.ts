import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core'
import { ActivatedRoute, ParamMap } from '@angular/router'
import { CategoryDonutChartComponent } from '@portal/components/category-donut-chart/category-donut-chart.component'
import { CategoryLineChartComponent } from '@portal/components/category-line-chart/category-line-chart.component'
import { PaymentMethodTypeDonutChartComponent } from '@portal/components/payment-method-type-donut-chart/payment-method-type-donut-chart.component'
import { TotalSales } from '@portal/interfaces/TotalSales'
import { PointOfSaleService } from '@portal/services/point-of-sale.service'
import * as moment from 'moment'
import { NgxSpinnerService } from 'ngx-spinner'
import { combineLatest } from 'rxjs'
import { environment } from 'src/environments/environment'
@Component({
  selector: 'app-point-of-sale-transaction',
  template: `
    <app-container>
      <div class="w-full flex justify-end">
        <select
          class="form-select block mr-3"
          [(ngModel)]="selectedMonth"
          (ngModelChange)="onFilterChange()"
        >
          <option
            *ngFor="let month of monthsFilter; let index = index"
            [ngValue]="index"
          >
            {{ month }}
          </option>
        </select>
        <select
          class="form-select block mr-3"
          [(ngModel)]="selectedYear"
          (ngModelChange)="onFilterChange()"
        >
          <option *ngFor="let year of yearsFilter" [ngValue]="year">
            {{ year }}
          </option>
        </select>
      </div>
      <div class="w-full">
        <div class="flex flex-col sm:flex-row justify-around py-2">
          <div class="px-4 py-3">
            <p class="text-sm text-gray-500 uppercase tracking-wider">ventes</p>
            <p class="text-4xl text-primary-600">{{ totalSales.total }}</p>
          </div>

          <div class="px-4 py-3">
            <p class="text-sm text-gray-500 uppercase tracking-wider">
              total (brut)
            </p>
            <p class="text-4xl text-primary-600">
              {{ totalSales.amount | currency: 'EUR' | replace: ',':'.' }}
            </p>
          </div>
          <div class="px-4 py-3">
            <p class="text-sm text-gray-500 uppercase tracking-wider">
              total (net)
            </p>
            <p class="text-4xl text-primary-600">
              {{ totalSales.net_amount | currency: 'EUR' | replace: ',':'.' }}
            </p>
          </div>
        </div>
      </div>
      <p class="text-xl text-gray-600 my-2">Transactions récentes</p>
      <div class="flex flex-col mb-4">
        <div
          class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8"
        >
          <div
            class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200"
          >
            <table class="min-w-full">
              <thead>
                <tr>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    point de vente
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    produit
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    prix (brut)
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    frais
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    prix (net)
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Méthode de paiement
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    date / heure
                  </th>
                </tr>
              </thead>
              <tbody class="bg-white">
                <tr class="bg-white" [hidden]="transactions.length > 0">
                  <td colspan="100">
                    <app-empty-data></app-empty-data>
                  </td>
                </tr>
                <tr
                  *ngFor="let transaction of transactions"
                  [hidden]="transactions.length == 0"
                  class="bg-white"
                >
                  <td
                    class="py-4 px-6  whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900"
                  >
                    {{ transaction.point_of_sale_name }}
                  </td>
                  <td
                    class="px-6 py-4 flex whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    <span *ngIf="transaction.product_count === 0">{{
                      transaction.product_name
                    }}</span>
                    <span
                      *ngIf="transaction.product_count > 0"
                      class="inline-flex items-center px-2.5 py-0.5 rounded-md text-sm font-medium bg-blue-100 text-blue-800"
                    >
                      {{ transaction.product_count }} produits
                    </span>
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    <span
                      class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-gray-100 text-gray-800"
                    >
                      {{
                        transaction.amount | currency: 'EUR' | replace: ',':'.'
                      }}
                    </span>
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    <span
                      class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                    >
                      {{ transaction.fee | currency: 'EUR' | replace: ',':'.' }}
                    </span>
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    <span
                      class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                    >
                      {{
                        transaction.net_amount
                          | currency: 'EUR'
                          | replace: ',':'.'
                      }}
                    </span>
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    {{ transaction.payment_method_type_name }}
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    {{ transaction.date | date: 'dd-MM-yy H:mm' }}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <p class="text-xl text-gray-600 my-2">Statistiques</p>
      <div class="grid grid-cols-1 sm:grid-cols-3 gap-4 my-4">
        <div class="bg-white rounded-lg shadow-md p-5">
          <p class="text-lg font-medium text-cool-gray-600">
            Total par catégorie de produits
          </p>
          <div class="my-2">
            <app-category-donut-chart
              #categoryDonutChart
            ></app-category-donut-chart>
          </div>
        </div>
        <div class="bg-white rounded-lg shadow-md p-5">
          <p class="text-lg font-medium text-cool-gray-600">
            Total par méthode de paiement
          </p>
          <div class="my-2">
            <app-payment-method-type-donut-chart
              #paymentMethodTypeDonutChart
            ></app-payment-method-type-donut-chart>
          </div>
        </div>
        <div class="bg-white rounded-lg shadow-md p-5">
          <p class="text-lg font-medium text-cool-gray-600">A venir</p>
          <app-empty-data></app-empty-data>
        </div>
      </div>
      <div class="w-full my-4">
        <div class="bg-white rounded-lg shadow-md p-5">
          <p class="text-lg font-medium text-cool-gray-600">
            Total journalier par catégorie de produits
          </p>
          <div class="my-2">
            <app-category-line-chart
              #categoryLineChart
            ></app-category-line-chart>
          </div>
        </div>
      </div>
    </app-container>
  `,
  styleUrls: ['./point-of-sale-transaction.component.scss'],
})
export class PointOfSaleTransactionComponent implements OnInit, AfterViewInit {
  pointOfSaleId = ''
  transactions = []
  monthsFilter = []
  yearsFilter = []
  selectedMonth = 0
  selectedYear = 0
  totalSales: TotalSales = { amount: 0, net_amount: 0, total: 0 }
  @ViewChild('categoryDonutChart', { static: false })
  categoryDonutChart: CategoryDonutChartComponent
  @ViewChild('categoryLineChart', { static: false })
  categoryLineChart: CategoryLineChartComponent
  @ViewChild('paymentMethodTypeDonutChart', { static: false })
  paymentMethodTypeDonutChart: PaymentMethodTypeDonutChartComponent
  constructor(
    private _route: ActivatedRoute,
    private _pointOfSaleService: PointOfSaleService,
    private _spinner: NgxSpinnerService
  ) {}

  ngAfterViewInit(): void {
    this._route.parent.paramMap.subscribe((params: ParamMap) => {
      this.pointOfSaleId = params.get('id')
      this.applyFilter()
    })
  }

  ngOnInit() {
    this.initFilters()
  }

  private applyFilter() {
    this._spinner.show()
    const startDate = moment()
      .month(this.selectedMonth)
      .year(this.selectedYear)
      .startOf('month')
      .format('YYYY-MM-DD')
    const endDate = moment()
      .month(this.selectedMonth)
      .year(this.selectedYear)
      .endOf('month')
      .format('YYYY-MM-DD')
    combineLatest([
      this._pointOfSaleService.getLastSales(
        this.pointOfSaleId,
        startDate,
        endDate
      ),
      this._pointOfSaleService.getTotalSales(
        this.pointOfSaleId,
        startDate,
        endDate
      ),
      this._pointOfSaleService.getTotalSalesCategory(
        this.pointOfSaleId,
        startDate,
        endDate
      ),
      this._pointOfSaleService.getTotalSalesPaymentMethodType(
        this.pointOfSaleId,
        startDate,
        endDate
      ),
    ]).subscribe({
      next: ([
        transactions,
        totalSales,
        totalSalesCategory,
        totalSalesPaymentMethodType,
      ]) => {
        this.transactions = transactions
        this.totalSales = totalSales
        this.categoryLineChart.initChart(totalSalesCategory)
        this.categoryDonutChart.initChart(totalSalesCategory)
        this.paymentMethodTypeDonutChart.initChart(totalSalesPaymentMethodType)
      },
      error: null,
      complete: () => this._spinner.hide(),
    })
  }

  onFilterChange() {
    this.applyFilter()
  }

  private initFilters() {
    this.monthsFilter = moment.months()
    this.selectedMonth = moment().month()
    this.selectedYear = moment().year()
    this.yearsFilter = [environment.constants.INIT_YEAR]
    if (moment().year() !== environment.constants.INIT_YEAR) {
      const firstYear = moment().year(environment.constants.INIT_YEAR)
      const now = moment()
      const diff = now.diff(firstYear, 'years')
      for (let index = 0; index < diff; index++) {
        this.yearsFilter.push(now.year())
        now.subtract('1', 'year')
      }
      this.yearsFilter.sort()
    }
  }
}
