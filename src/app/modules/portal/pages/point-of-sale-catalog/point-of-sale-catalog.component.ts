import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core'
import { ActivatedRoute, ParamMap, Router } from '@angular/router'
import { Select, Store } from '@ngxs/store'
import { Product } from '@portal/interfaces/Product'
import { ProductService } from '@portal/services/product.service'
import { ModalComponent } from '@shared/components/ui/modal/modal.component'
import { saveAs } from 'file-saver'
import { NgxSpinnerService } from 'ngx-spinner'
import { Observable, Subscription } from 'rxjs'
import { map } from 'rxjs/operators'
import { DeleteProduct, GetProducts } from '../../actions/products.action'
import { ProductsState } from '../../states/products.state'

@Component({
  selector: 'app-point-of-sale-catalog',
  template: `
    <app-container>
      <div class="w-full mb-2 flex justify-end">
        <button
          type="button"
          (click)="add()"
          class="button is-primary is-medium"
        >
          <svg
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            class="-ml-1 mr-2 h-5 w-5"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="M12 4v16m8-8H4"
            />
          </svg>
          Produit
        </button>
      </div>
      <app-empty-data *ngIf="(items$ | async)?.length == 0"></app-empty-data>
      <div class="grid grid-cols-2 md:grid-cols-5 gap-4">
        <div *ngFor="let product of items$ | async">
          <div class="bg-white shadow rounded-lg h-full relative">
            <div class=" px-4 pt-5 pb-2 flex">
              <div class="flex-1">
                <p class="text-gray-900">
                  {{ product.name }}
                </p>
                <p class="text-2xl text-primary-700">
                  {{ product.price | currency: 'EUR' | replace: ',':'.' }}
                </p>
                <span
                  class="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium leading-4 bg-gray-100 text-gray-800"
                >
                  {{ product.product_category_name }}
                </span>
              </div>
            </div>
            <div
              class="w-full bg-cool-gray-050 rounded-b px-3 pt-2 pb-3 flex justify-around sm:justify-end"
            >
              <div
                (click)="deleteProductModal(product)"
                class="sm:mr-5 cursor-pointer text-primary-600 hover:text-primary-900"
                title="Supprimer"
              >
                <div
                  class="h-5 w-5"
                  aria-label="Delete"
                  [inlineSVG]="'/assets/images/heroic-icons/outline/trash.svg'"
                ></div>
              </div>
              <div
                (click)="downloadQRCode(product)"
                class="sm:mr-5 cursor-pointer text-primary-600 hover:text-primary-900"
                title="QRCode"
              >
                <div
                  class="h-5 w-5"
                  aria-label="Download"
                  [inlineSVG]="
                    '/assets/images/heroic-icons/outline/download.svg'
                  "
                ></div>
              </div>
              <div
                (click)="downloadPayconiqQRCode(product)"
                class="sm:mr-5 cursor-pointer text-pink-600 hover:text-pink-900"
                title="QRCode Payconiq"
              >
                <div
                  class="h-5 w-5"
                  aria-label="Download"
                  [inlineSVG]="'/assets/images/heroic-icons/outline/qrcode.svg'"
                ></div>
              </div>
              <div
                (click)="edit(product)"
                class=" cursor-pointer text-primary-600 hover:text-primary-900"
                title="Modifier"
              >
                <div
                  class="h-5 w-5"
                  aria-label="Pencil"
                  [inlineSVG]="'/assets/images/heroic-icons/outline/pencil.svg'"
                ></div>
              </div>
            </div>
          </div>
        </div>
        <app-modal #modalDelete>
          <div
            modalIconHeader
            class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10"
          >
            <svg
              class="h-6 w-6 text-red-600"
              stroke="currentColor"
              fill="none"
              viewBox="0 0 24 24"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"
              />
            </svg>
          </div>
          <span modalHeader>Suppression du produit</span>
          <p modalBody class="py-4">
            Êtes vous certain de supprimer ce produit ?
          </p>
          <div modalFooter class="mt-5 sm:mt-4 sm:flex sm:flex-row-reverse">
            <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
              <button
                (click)="deleteProduct()"
                type="button"
                class="inline-flex justify-center w-full button is-danger"
              >
                Supprimer
              </button>
            </span>
            <span
              class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto"
            >
              <button
                (click)="modalDelete.close()"
                type="button"
                class="inline-flex justify-center w-full button is-white"
              >
                Annuler
              </button>
            </span>
          </div>
        </app-modal>
      </div>
    </app-container>
  `,
  styleUrls: ['./point-of-sale-catalog.component.scss'],
})
export class PointOfSaleCatalogComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalComponent
  pointOfSaleId = ''
  productToDelete: Product = null
  products = []

  items$: Observable<Product[]>
  isLoadingSub: Subscription
  deleteSub: Subscription

  @Select(ProductsState.isLoading) $isLoading: Observable<boolean>

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _productService: ProductService,
    private _spinner: NgxSpinnerService,
    private _store: Store
  ) {}

  ngOnInit() {
    this._route.parent.paramMap.subscribe((params: ParamMap) => {
      this.pointOfSaleId = params.get('id')
      this.items$ = this._store
        .select(ProductsState.items)
        .pipe(map((filterFn) => filterFn(params.get('id'))))

      this._store.dispatch(new GetProducts(this.pointOfSaleId))
    })
  }

  ngAfterViewInit(): void {
    this.isLoadingSub = this.$isLoading.subscribe((isLoading) => {
      isLoading ? this._spinner.show() : this._spinner.hide()
    })
  }

  ngOnDestroy(): void {
    this.isLoadingSub ? this.isLoadingSub.unsubscribe() : null
    this.deleteSub ? this.deleteSub.unsubscribe() : null
  }

  add() {
    this._router.navigate(['portal/product', this.pointOfSaleId])
  }

  edit(product: Product) {
    this._router.navigate(['portal/product', this.pointOfSaleId, product.id])
  }

  downloadQRCode(product) {
    this._spinner.show()
    this._productService.generateQRCode(product.id).subscribe({
      next: (content) => {
        const blob = new Blob([content], { type: 'image/png' })
        saveAs(blob, `qr_code_produit_${product.id}.png`)
        this._spinner.hide()
      },
      error: () => this._spinner.hide(),
    })
  }

  downloadPayconiqQRCode(product) {
    this._spinner.show()
    this._productService.generatePayconiqQRCode(product.id).subscribe({
      next: (content) => {
        const blob = new Blob([content], { type: 'image/png' })
        saveAs(blob, `qr_code_produit_payconiq_${product.id}.png`)
        this._spinner.hide()
      },
      error: () => this._spinner.hide(),
    })
  }

  deleteProductModal(product) {
    this.productToDelete = product
    this.modalDelete.toggle()
  }

  deleteProduct() {
    this.deleteSub = this._store
      .dispatch(new DeleteProduct(this.productToDelete.id))
      .subscribe(
        () => {
          this.modalDelete.close()
          this.productToDelete = null
        },
        (error) => {
          this.modalDelete.close()
          if (error.error.statusCode === 404) {
            alert(
              `Suppression impossible parcequ'il existe au moins une transaction pour ce produit`
            )
          }
        }
      )
  }
}
