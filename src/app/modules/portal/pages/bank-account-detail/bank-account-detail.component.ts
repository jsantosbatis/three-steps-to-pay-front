import { Component, OnInit, ViewChild } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { Select, Store } from '@ngxs/store'
import { BankAccount } from '@portal/interfaces/BankAccount'
import { ModalComponent } from '@shared/components/ui/modal/modal.component'
import { NgxSpinnerService } from 'ngx-spinner'
import { Observable, Subscription } from 'rxjs'
import {
  SUCCESSFULLY_CANCELLED,
  SUCCESSFULLY_CREATED,
  SUCCESSFULLY_DELETED,
  SUCCESSFULLY_UPDATED,
} from 'src/app/lang'
import { AlertService } from 'src/app/modules/shared/services/alert.service'
import {
  AddBankAccount,
  DeleteBankAccount,
  EditBankAccount,
  ResetDeleteError,
  SetCurrentBankAccount,
} from '../../actions/bank-accounts.action'
import { BankAccountsState } from '../../states/bank-accounts.state'

@Component({
  selector: 'app-back-account-detail',
  template: `
    <app-container>
      <div class="bg-white shadow overflow-hidden  sm:rounded-lg">
        <div class="px-4 py-5 border-b border-gray-200 sm:px-6">
          <form [formGroup]="myForm" (ngSubmit)="save()" novalidate>
            <div>
              <div>
                <div>
                  <h3 class="text-lg leading-6 font-medium text-gray-900">
                    Détails
                  </h3>
                  <p class="mt-1 max-w-2xl text-sm leading-5 text-gray-500">
                    Informations du compte bancaire
                  </p>
                </div>
                <div class="mt-6 sm:mt-5">
                  <app-form-text
                    formControlName="name"
                    placeholder="Compte ING"
                    label="Nom"
                    [required]="true"
                  >
                  </app-form-text>
                  <app-form-text
                    formControlName="number"
                    placeholder="BE98 9281 9281 9281"
                    label="Numéro de compte"
                    customMask="SS00 0000 0000 0000"
                    [required]="true"
                  >
                  </app-form-text>
                </div>
              </div>
            </div>
            <div class="mt-8 border-t border-gray-200 pt-5">
              <div class="flex justify-between">
                <span class="hidden sm:block ">
                  <button
                    type="button"
                    (click)="modalDelete.open()"
                    *ngIf="isEditMode()"
                    class="button is-secondary"
                  >
                    Supprimer
                  </button>
                </span>
                <!-- Empty div to trigger justify-between-->
                <div *ngIf="!isEditMode()"></div>
                <div
                  class="flex flex-col sm:flex-row sm:justify-between sm:w-auto w-full sm:space-y-0 sm:space-x-2 space-y-2"
                >
                  <span
                    class="hidden sm:inline-flex sm:w-auto w-full rounded-md shadow-sm"
                  >
                    <button
                      type="button"
                      (click)="cancel()"
                      class="button is-white"
                    >
                      Annuler
                    </button>
                  </span>
                  <span
                    class="hidden sm:ml-3 sm:inline-flex sm:w-auto w-full rounded-md shadow-sm space-y-2 sm:space-y-0 sm:space-x-2"
                  >
                    <button
                      type="submit"
                      [disabled]="!this.myForm.valid || !hasChange()"
                      class="button is-primary"
                    >
                      Sauvegarder
                    </button>
                  </span>
                  <!-- Mobile buttons -->
                  <span class="block sm:hidden">
                    <button
                      type="button"
                      (click)="cancel()"
                      class="button is-white w-full"
                    >
                      Annuler
                    </button>
                  </span>
                  <span class="block sm:hidden">
                    <button
                      type="button"
                      type="submit"
                      [disabled]="!this.myForm.valid || !hasChange()"
                      class="button is-primary w-full"
                    >
                      Sauvegarder
                    </button>
                  </span>
                  <span class="block sm:hidden">
                    <button
                      type="button"
                      (click)="modalDelete.open()"
                      *ngIf="isEditMode()"
                      class="button is-secondary w-full"
                    >
                      Supprimer
                    </button>
                  </span>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </app-container>
    <app-modal #modalDelete>
      <div
        modalIconHeader
        class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10"
      >
        <svg
          class="h-6 w-6 text-red-600"
          stroke="currentColor"
          fill="none"
          viewBox="0 0 24 24"
        >
          <path
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width="2"
            d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"
          />
        </svg>
      </div>
      <span modalHeader>Supprimer le compte bancaire</span>
      <p modalBody class="py-4">
        Êtes vous certain de vouloir supprimer ce compte bancaire?
      </p>
      <div modalFooter class="mt-5 sm:mt-4 sm:flex sm:flex-row-reverse">
        <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
          <button
            type="button"
            (click)="delete()"
            class="justify-center w-full button is-danger"
          >
            Supprimer
          </button>
        </span>
        <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
          <button
            (click)="modalDelete.close()"
            type="button"
            class="justify-center w-full button is-white"
          >
            Annuler
          </button>
        </span>
      </div>
    </app-modal>
  `,
  styleUrls: ['./bank-account-detail.component.scss'],
})
export class BankAccountDetailComponent implements OnInit {
  @ViewChild('modalDelete', { static: false }) modalDelete: ModalComponent
  data: BankAccount
  myForm: FormGroup
  addSub: Subscription
  editSub: Subscription
  deleteSub: Subscription
  loadingSub: Subscription
  currentSub: Subscription
  errorSub: Subscription

  @Select(BankAccountsState.isLoading) isLoading$: Observable<boolean>
  @Select(BankAccountsState.current) current$: Observable<BankAccount>
  @Select(BankAccountsState.error) error$: Observable<string>
  @Select(BankAccountsState.deleted) deleted$: Observable<boolean>

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _store: Store,
    private _spinner: NgxSpinnerService,
    private _alert: AlertService
  ) {}

  ngOnDestroy(): void {
    this.addSub ? this.addSub.unsubscribe() : null
    this.editSub ? this.editSub.unsubscribe() : null
    this.loadingSub ? this.loadingSub.unsubscribe() : null
    this.currentSub ? this.currentSub.unsubscribe() : null
    this.deleteSub ? this.deleteSub.unsubscribe() : null
    this.errorSub ? this.errorSub.unsubscribe() : null
  }

  ngOnInit() {
    this.myForm = new FormGroup({
      id: new FormControl({ value: '', disabled: false }),
      name: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
      number: new FormControl({ value: '', disabled: false }, [
        Validators.required,
      ]),
    })

    this.deleteSub = this.deleted$.subscribe((deleted) => {
      if (deleted === true) {
        this.modalDelete.close()
        this._router.navigate(['portal/bank-accounts'])
        this._alert.success(SUCCESSFULLY_DELETED)
      }
    })

    this.errorSub = this.error$.subscribe((message) => {
      if (message) {
        alert(message)
        this.modalDelete.close()
        this._store.dispatch(new ResetDeleteError())
      }
    })

    this.loadingSub = this.isLoading$.subscribe((isLoading) => {
      isLoading ? this._spinner.show() : this._spinner.hide()
    })

    this.currentSub = this.current$.subscribe((data) => {
      this.initForm(data)
    })

    this._route.paramMap.subscribe((params) => {
      this._store.dispatch(new SetCurrentBankAccount(params.get('id')))
    })
  }

  private initForm(result: BankAccount) {
    this.data = result
    if (result && result.id) {
      this.myForm.setValue({
        id: result.id,
        number: result.number,
        name: result.name,
      } as BankAccount)
    } else {
      this.data = { name: '', number: '' }
      this.myForm.reset({})
    }
  }

  isEditMode() {
    return !!this.myForm.value.id
  }

  hasChange() {
    if (!this.data) return false
    return (
      this.myForm.value.name != this.data.name ||
      this.myForm.value.number != this.data.number
    )
  }

  save() {
    this._spinner.show()
    if (this.myForm.value['id']) {
      this.editSub = this._store
        .dispatch(new EditBankAccount(this.myForm.value))
        .subscribe(() => {
          this._alert.success(SUCCESSFULLY_UPDATED)
        })
    } else {
      this.addSub = this._store
        .dispatch(new AddBankAccount(this.myForm.value))
        .subscribe(() => {
          this._alert.success(SUCCESSFULLY_CREATED)
        })
    }
  }

  cancel() {
    if (this.myForm.value['id']) {
      if (this.hasChange()) {
        this.myForm.patchValue(this.data)
        this._alert.success(SUCCESSFULLY_CANCELLED)
      } else {
        this._router.navigate(['portal/bank-accounts'])
      }
    } else {
      this._router.navigate(['portal/bank-accounts'])
    }
  }

  delete() {
    this._store.dispatch(new DeleteBankAccount(this.data.id))
  }
}
