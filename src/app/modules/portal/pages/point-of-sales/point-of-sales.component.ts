import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Select, Store } from '@ngxs/store';
import { PointOfSalesState } from '../../states/point-of-sales.state';
import { Observable, Subscription } from 'rxjs';
import { PointOfSale } from '@portal/interfaces/PointOfSale';
import { GetAll } from '../../actions/point-of-sales.action';

@Component({
  selector: 'app-point-of-sales',
  template: `
    <app-container>
      <app-header title="Point de vente"></app-header>
      <div class="w-full mb-2 flex justify-end">
        <button
          type="button"
          (click)="addPointOfSale()"
          class="button is-primary is-medium"
        >
          <div
            class="-ml-1 mr-2 h-5 w-5"
            aria-label="plus"
            [inlineSVG]="'/assets/images/heroic-icons/outline/plus.svg'"
          ></div>
          Point de vente
        </button>
      </div>
      <app-empty-data *ngIf="(items$ | async)?.length == 0"></app-empty-data>
      <div class="grid grid-cols-1 md:grid-cols-3 gap-4">
        <div *ngFor="let pointOfSale of items$ | async">
          <app-point-of-sale-tile [data]="pointOfSale">
          </app-point-of-sale-tile>
        </div>
      </div>
    </app-container>
  `,
  styleUrls: ['./point-of-sales.component.scss'],
})
export class PointOfSalesComponent implements OnInit, AfterViewInit, OnDestroy {
  @Select(PointOfSalesState.items) items$: Observable<PointOfSale[]>;
  @Select(PointOfSalesState.isLoading) isLoading$: Observable<boolean>;

  sub: Subscription;
  constructor(
    private _router: Router,
    private _spinner: NgxSpinnerService,
    private _store: Store
  ) {}

  ngOnInit() {
    this._store.dispatch(new GetAll());
  }

  ngAfterViewInit(): void {
    this.sub = this.isLoading$.subscribe((isLoading) => {
      isLoading ? this._spinner.show() : this._spinner.hide();
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  addPointOfSale() {
    this._router.navigate(['portal/point-of-sale']);
  }
}
