import { Component, OnInit } from '@angular/core'
import { PointOfSaleSales } from '@portal/interfaces/PointOfSaleSales'
import { TotalSales } from '@portal/interfaces/TotalSales'
import { Transaction } from '@portal/interfaces/Transaction'
import { AuthService } from '@portal/services/auth.service'
import { TransactionService } from '@portal/services/transaction.service'
import * as moment from 'moment'
import { NgxSpinnerService } from 'ngx-spinner'
import { combineLatest } from 'rxjs'

@Component({
  selector: 'app-dashboard',
  template: `
    <div class="w-full">
      <div class="flex flex-col sm:flex-row justify-around py-2">
        <div class="px-4 py-3">
          <p class="text-sm text-gray-500 uppercase tracking-wider">ventes</p>
          <p class="text-4xl text-primary-600">{{ totalSales.total }}</p>
        </div>

        <div class="px-4 py-3">
          <p class="text-sm text-gray-500 uppercase tracking-wider">
            total (brut)
          </p>
          <p class="text-4xl text-primary-600">
            {{ totalSales.amount | currency: 'EUR' | replace: ',':'.' }}
          </p>
        </div>
        <div class="px-4 py-3">
          <p class="text-sm text-gray-500 uppercase tracking-wider">
            total (net)
          </p>
          <p class="text-4xl text-primary-600">
            {{ totalSales.net_amount | currency: 'EUR' | replace: ',':'.' }}
          </p>
        </div>
      </div>
    </div>
    <app-container>
      <p class="text-xl text-gray-600 my-2">Transactions récentes</p>
      <div class="flex flex-col">
        <div
          class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8"
        >
          <div
            class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200"
          >
            <table class="min-w-full">
              <thead>
                <tr>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    point de vente
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    produit
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    prix (brut)
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    frais
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    prix (net)
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Méthode de paiement
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    date / heure
                  </th>
                </tr>
              </thead>
              <tbody class="bg-white">
                <tr class="bg-white" [hidden]="lastSales.length > 0">
                  <td colspan="100">
                    <app-empty-data></app-empty-data>
                  </td>
                </tr>
                <tr
                  *ngFor="let sale of lastSales"
                  [hidden]="lastSales.length == 0"
                  class="bg-white"
                >
                  <td
                    class="py-4 px-6 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900"
                  >
                    {{ sale.point_of_sale_name }}
                  </td>
                  <td
                    class="px-6 py-4 flex whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    <span *ngIf="sale.product_count === 0">{{
                      sale.product_name
                    }}</span>
                    <span
                      *ngIf="sale.product_count > 0"
                      class="inline-flex items-center px-2.5 py-0.5 rounded-md text-sm font-medium bg-blue-100 text-blue-800"
                    >
                      {{ sale.product_count }} produits
                    </span>
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    <span
                      class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-gray-100 text-gray-800"
                    >
                      {{ sale.amount | currency: 'EUR' | replace: ',':'.' }}
                    </span>
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    <span
                      class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-100 text-red-800"
                    >
                      {{ sale.fee | currency: 'EUR' | replace: ',':'.' }}
                    </span>
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    <span
                      class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                    >
                      {{ sale.net_amount | currency: 'EUR' | replace: ',':'.' }}
                    </span>
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    {{ sale.payment_method_type_name }}
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    {{ sale.date | date: 'dd-MM-yy H:mm' }}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </app-container>
  `,
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  user: any
  totalSales: TotalSales = { amount: 0, net_amount: 0, total: 0 }
  lastSales: Transaction[] = []
  pointOfSaleSales: PointOfSaleSales[] = []
  constructor(
    private _auth: AuthService,
    private _transactionService: TransactionService,
    private _spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this._spinner.show()
    combineLatest([
      this._transactionService.getTotalSales(
        moment().startOf('month').format('YYYY-MM-DD'),
        moment().endOf('month').format('YYYY-MM-DD')
      ),
      this._transactionService.getLastSales(),
    ]).subscribe(([totalSales, lastSales]) => {
      this.totalSales = totalSales
      this.lastSales = lastSales
      this._spinner.hide()
    })
  }

  signOut() {
    this._auth.signOut()
  }
}
