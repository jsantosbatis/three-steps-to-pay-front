import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';
import { saveAs } from 'file-saver';
import { NgxSpinnerService } from 'ngx-spinner';
import { Invoice } from '@portal/interfaces/Invoice';
import { InvoiceService } from '@portal/services/invoice.service';
import { Store, Select } from '@ngxs/store';
import { InvoicesState } from '../../states/invoices.state';
import { Observable, Subscription } from 'rxjs';
import { GetByPeriod } from '../../actions/invoices.action';

@Component({
  selector: 'app-invoices',
  template: `
    <app-container extraClass="pt-5">
      <app-header title="Facture"></app-header>
      <div class="w-full flex justify-end mb-2">
        <div class="max-w-xs rounded-md shadow-sm">
          <select
            [(ngModel)]="selectedYear"
            (ngModelChange)="applyFilter()"
            class="block form-select w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5"
          >
            <option *ngFor="let year of yearsFilter" [ngValue]="year">
              {{ year }}
            </option>
          </select>
        </div>
      </div>
      <div class="flex flex-col">
        <div
          class="-my-2 py-2 overflow-x-auto sm:-mx-6 sm:px-6 lg:-mx-8 lg:px-8"
        >
          <div
            class="align-middle inline-block min-w-full shadow overflow-hidden sm:rounded-lg border-b border-gray-200"
          >
            <table class="min-w-full">
              <thead>
                <tr>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    période
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    numéro de facture
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    montant à payer (TTC)
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    montant des ventes
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider"
                  >
                    date / heure
                  </th>
                  <th
                    class="px-6 py-3 border-b border-gray-200 bg-gray-50"
                  ></th>
                </tr>
              </thead>
              <tbody class="bg-white">
                <tr class="bg-white" *ngIf="(items$ | async)?.length == 0">
                  <td colspan="100">
                    <app-empty-data></app-empty-data>
                  </td>
                </tr>

                <tr *ngFor="let invoice of items$ | async">
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 font-medium text-gray-900"
                  >
                    {{ invoice.start_date | date: 'dd/MM/yyyy' }}
                    -
                    {{ invoice.end_date | date: 'dd/MM/yyyy' }}
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    {{ invoice.number | empty }}
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    {{ invoice.amount | empty }}
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    {{ invoice.sales_amount | empty }}
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap border-b border-gray-200 text-sm leading-5 text-gray-500"
                  >
                    {{ invoice.created_at | date: 'dd-MM-yy H:mm' }}
                  </td>
                  <td
                    class="px-6 py-4 whitespace-no-wrap  border-b border-gray-200 text-right text-sm leading-5 font-medium flex space-x-3"
                  >
                    <div
                      class="w-5 h-5 text-primary-600 hover:text-primary-900 focus:outline-none focus:underline cursor-pointer"
                      title="Télécharger"
                      (click)="download(invoice)"
                      [inlineSVG]="
                        '/assets/images/heroic-icons/outline/download.svg'
                      "
                    ></div>
                    <div
                      class="w-5 h-5 text-primary-600 hover:text-primary-900 focus:outline-none focus:underline cursor-pointer"
                      [routerLink]="[
                        '/portal/invoices',
                        invoice.id,
                        'transfers'
                      ]"
                      title="Transferts"
                      [inlineSVG]="
                        '/assets/images/heroic-icons/outline/switch-horizontal.svg'
                      "
                    ></div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </app-container>
  `,
  styleUrls: ['./invoices.component.scss'],
})
export class InvoicesComponent implements OnInit, OnDestroy, AfterViewInit {
  invoices: Invoice[] = [];
  selectedYear = null;
  yearsFilter = [];
  subLoading: Subscription;

  @Select(InvoicesState.items) items$: Observable<Invoice[]>;
  @Select(InvoicesState.isLoading) isLoading$: Observable<boolean>;

  constructor(
    private _invoiceService: InvoiceService,
    private _store: Store,
    private _spinner: NgxSpinnerService
  ) {}

  ngOnDestroy(): void {
    this.subLoading.unsubscribe();
  }

  ngOnInit() {
    this.initFilters();
    this.applyFilter();
  }

  ngAfterViewInit(): void {
    this.subLoading = this.isLoading$.subscribe((isLoading) => {
      isLoading ? this._spinner.show() : this._spinner.hide();
    });
  }

  applyFilter() {
    this._store.dispatch(
      new GetByPeriod(
        moment().year(this.selectedYear).startOf('year').format('YYYY-MM-DD'),
        moment().year(this.selectedYear).endOf('year').format('YYYY-MM-DD')
      )
    );
  }

  download(invoice: Invoice) {
    this._spinner.show();
    this._invoiceService.download(invoice.id).subscribe((content) => {
      this._spinner.hide();
      const blob = new Blob([content], { type: 'application/pdf' });
      saveAs(blob, `invoice_${invoice.start_date}_${invoice.end_date}`);
    });
  }

  private initFilters() {
    this.selectedYear = moment().year();
    this.yearsFilter = [environment.constants.INIT_YEAR];
    if (moment().year() !== environment.constants.INIT_YEAR) {
      const firstYear = moment().year(environment.constants.INIT_YEAR);
      const now = moment();
      const diff = now.diff(firstYear, 'years');
      for (let index = 0; index < diff; index++) {
        this.yearsFilter.push(now.year());
        now.subtract('1', 'year');
      }
      this.yearsFilter.sort();
    }
  }
}
