import { BankAccount } from '@portal/interfaces/BankAccount';

export class GetAll {
  static readonly type =
    '[Portal : Bank accounts API] Get all bank accounts of current user';
  constructor() {}
}

export class AddBankAccount {
  static readonly type = '[Portal : Bank accounts API] Add bank account';
  constructor(public data: BankAccount) {}
}

export class EditBankAccount {
  static readonly type = '[Portal : Bank accounts API] Edit bank account';
  constructor(public data: BankAccount) {}
}

export class DeleteBankAccount {
  static readonly type = '[Portal : Bank accounts API] Delete bank account';
  constructor(public id: string) {}
}

export class ResetDeleteError {
  static readonly type =
    '[Portal : Bank detail Page] Reset error after deletion';
  constructor() {}
}

export class SetCurrentBankAccount {
  static readonly type =
    '[Portal : Bank accounts API] Set current bank account';
  constructor(public id: string) {}
}
