export class GetByPeriod {
  static readonly type = '[Portal : Invoices API] Get invoices of current user';
  constructor(public startDate: string, public endDate: string) {}
}
