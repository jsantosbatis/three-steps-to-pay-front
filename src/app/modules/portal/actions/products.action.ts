import { Product } from '@portal/interfaces/Product';

export class GetProducts {
  static readonly type =
    '[Portal : Products API] Get products of a point of sale';
  constructor(public id: string) {}
}

export class AddProduct {
  static readonly type = '[Portal : Products API] Add product';
  constructor(public data: Product) {}
}

export class EditProduct {
  static readonly type = '[Portal : Products API] Edit prpduct';
  constructor(public data: Product) {}
}

export class DeleteProduct {
  static readonly type = '[Portal : Products API] Delete product';
  constructor(public id: string) {}
}

export class SetCurrentProduct {
  static readonly type = '[Portal : Products API] Set current product';
  constructor(public id: string) {}
}

export class GetProductCategories {
  static readonly type = '[Portal : Products API] Get product categories';
  constructor() {}
}
