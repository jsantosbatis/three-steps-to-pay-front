import { PointOfSale } from '@portal/interfaces/PointOfSale';

export class GetAll {
  static readonly type =
    '[Portal : Point of sales API] Get all point of sales of current user';
  constructor() {}
}

export class AddPointOfSale {
  static readonly type = '[Portal : Point of sales API] Add point of sale';
  constructor(public data: PointOfSale) {}
}

export class EditPointOfSale {
  static readonly type = '[Portal : Point of sales API] Edit point of sale';
  constructor(public data: PointOfSale) {}
}

export class DeletePointOfSale {
  static readonly type = '[Portal : Point of sales API] Delete point of sale';
  constructor(public id: string) {}
}

export class SetCurrentPointOfSale {
  static readonly type =
    '[Portal : Point of sales API] Set current point of sale';
  constructor(public id: string) {}
}
