import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { IsNotSignedInGuard } from './guards/is-not-signed-in.guard';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { IsSignedInGuard } from './guards/is-signed-in.guard';
import { AppPortalContainerComponent } from './components/portal-container/portal-container.component';
import { PointOfSalesComponent } from './pages/point-of-sales/point-of-sales.component';
import { PointOfSaleDetailComponent } from './pages/point-of-sale-detail/point-of-sale-detail.component';
import { PointOfSaleTransactionComponent } from './pages/point-of-sale-transaction/point-of-sale-transaction.component';
import { PointOfSaleCatalogComponent } from './pages/point-of-sale-catalog/point-of-sale-catalog.component';
import { PointOfSaleComponent } from './pages/point-of-sale/point-of-sale.component';
import { ProductDetailComponent } from './pages/product-detail/product-detail.component';
import { BankAccountsComponent } from './pages/bank-accounts/bank-accounts.component';
import { BankAccountDetailComponent } from './pages/bank-account-detail/bank-account-detail.component';
import { UsersComponent } from './pages/users/users.component';
import { IsAdminGuard } from './guards/is-admin.guard';
import { UserDetailComponent } from './pages/user-detail/user-detail.component';
import { CanEditUserAccount } from './guards/can-edit-user-account.guard';
import { InvoicesComponent } from './pages/invoices/invoices.component';
import { InvoiceTransfersComponent } from './pages/invoice-transfers/invoice-transfers.component';

const routes: Routes = [
  {
    path: '',
    component: AppPortalContainerComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [IsSignedInGuard],
      },
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: 'bank-accounts',
        component: BankAccountsComponent,
        canActivate: [IsSignedInGuard],
      },
      {
        path: 'bank-account/:id',
        component: BankAccountDetailComponent,
        canActivate: [IsSignedInGuard],
      },
      {
        path: 'bank-account',
        component: BankAccountDetailComponent,
        canActivate: [IsSignedInGuard],
      },
      {
        path: 'invoices',
        component: InvoicesComponent,
        canActivate: [IsSignedInGuard],
      },
      {
        path: 'invoices/:id/transfers',
        component: InvoiceTransfersComponent,
        canActivate: [IsSignedInGuard],
      },

      {
        path: 'point-of-sales',
        component: PointOfSalesComponent,
        canActivate: [IsSignedInGuard],
      },
      {
        path: 'point-of-sale',
        component: PointOfSaleDetailComponent,
        canActivate: [IsSignedInGuard],
      },
      {
        path: 'point-of-sale/:id',
        component: PointOfSaleComponent,
        canActivate: [IsSignedInGuard],
        children: [
          { path: '', redirectTo: 'catalog', pathMatch: 'full' },
          { path: 'detail', component: PointOfSaleDetailComponent },
          { path: 'catalog', component: PointOfSaleCatalogComponent },
          { path: 'transaction', component: PointOfSaleTransactionComponent },
        ],
      },
      {
        path: 'product/:pointOfSaleId',
        component: ProductDetailComponent,
        canActivate: [IsSignedInGuard],
      },
      {
        path: 'product/:pointOfSaleId/:productId',
        component: ProductDetailComponent,
        canActivate: [IsSignedInGuard],
      },
      {
        path: 'users',
        component: UsersComponent,
        canActivate: [IsSignedInGuard, IsAdminGuard],
      },
      {
        path: 'user/:id',
        component: UserDetailComponent,
        canActivate: [IsSignedInGuard, CanEditUserAccount],
      },
      {
        path: 'user',
        component: UserDetailComponent,
        canActivate: [IsSignedInGuard, IsAdminGuard],
      },
    ],
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [IsNotSignedInGuard],
  },
  { path: 'resetPassword', component: ResetPasswordComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
