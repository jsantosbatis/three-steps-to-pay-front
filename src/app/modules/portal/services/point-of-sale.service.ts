import { Injectable } from '@angular/core';
import { MyService } from './my-service.service';
import { HttpClient } from '@angular/common/http';
import { PointOfSale } from '../interfaces/PointOfSale';
import { Observable } from 'rxjs';
import { Product } from '../interfaces/Product';
import { Transaction } from '../interfaces/Transaction';
import { TotalSales } from '../interfaces/TotalSales';
import { SalesCategory } from '@portal/interfaces/SalesCategory';
import { SalesPaymentMethodType } from '@portal/interfaces/SalesPaymentMethodType';

@Injectable({
  providedIn: 'root',
})
export class PointOfSaleService extends MyService<PointOfSale> {
  constructor(protected http: HttpClient) {
    super(http);
    this.apiPath = '/api/point_of_sales';
  }

  getProducts(id: string): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.apiPath}/${id}/products`);
  }

  getLastSales(id: string, startDate: string, endDate: string) {
    return this.http.get<Transaction[]>(`${this.apiPath}/${id}/last_sales`, {
      params: {
        start_date: startDate,
        end_date: endDate,
      } as any,
    });
  }

  getTotalSales(id: string, startDate: string, endDate: string) {
    return this.http.get<TotalSales>(`${this.apiPath}/${id}/total_sales`, {
      params: {
        start_date: startDate,
        end_date: endDate,
      } as any,
    });
  }

  getTotalSalesCategory(id: string, startDate: string, endDate: string) {
    return this.http.get<SalesCategory[]>(
      `${this.apiPath}/${id}/total_sales_category`,
      {
        params: {
          start_date: startDate,
          end_date: endDate,
        } as any,
      }
    );
  }
  getTotalSalesPaymentMethodType(
    id: string,
    startDate: string,
    endDate: string
  ) {
    return this.http.get<SalesPaymentMethodType[]>(
      `${this.apiPath}/${id}/total_sales_payment_method_type`,
      {
        params: {
          start_date: startDate,
          end_date: endDate,
        } as any,
      }
    );
  }

  generateQRCode(id: string): Observable<ArrayBuffer> {
    return this.http.get(`${this.apiPath}/generate_qr_code/${id}`, {
      responseType: 'arraybuffer',
    });
  }
}
