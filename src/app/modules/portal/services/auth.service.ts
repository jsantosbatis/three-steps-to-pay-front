import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { UserService } from './user.service';
import { BehaviorSubject } from 'rxjs';
import { User } from '@portal/interfaces/User';
import { isNullOrUndefined } from '@util';
@Injectable({
  providedIn: 'any',
})
export class AuthService {
  private _connectedAs: User;
  private _user: User = null;
  private readonly currentUser = new BehaviorSubject<User>(null);
  readonly currentUser$ = this.currentUser.asObservable();
  constructor(
    private _firebaseAuth: AngularFireAuth,
    private _router: Router,
    private _userService: UserService
  ) {}

  signUpWithEmailAndPassword(email: string, password: string) {
    return this._firebaseAuth.createUserWithEmailAndPassword(email, password);
  }

  signInWithEmailAndPassword(email: string, password: string) {
    return this._firebaseAuth.signInWithEmailAndPassword(email, password);
  }

  signInWithTwitter() {
    return this._firebaseAuth.signInWithRedirect(
      new firebase.auth.TwitterAuthProvider()
    );
  }

  signInWithFacebook() {
    return this._firebaseAuth.signInWithRedirect(
      new firebase.auth.FacebookAuthProvider()
    );
  }

  signInWithGoogle() {
    return this._firebaseAuth.signInWithRedirect(
      new firebase.auth.GoogleAuthProvider()
    );
  }

  async getToken(): Promise<string> {
    if (await this._firebaseAuth.currentUser) {
      return await (await this._firebaseAuth.currentUser).getIdToken();
    } else {
      return '';
    }
  }

  async getUser(): Promise<User> {
    if (!this._user) {
      const fbUser = await this._firebaseAuth.currentUser;
      if (fbUser) {
        this.setCurrentUser(
          await this._userService.findUserByGoogleId(fbUser.uid).toPromise()
        );
      }
    }
    return this._user;
  }

  async isAdmin(): Promise<boolean> {
    const user = await this.getUser();
    return user ? user.is_admin : false;
  }

  isSignedIn() {
    return this._firebaseAuth.user.pipe(
      map((user: firebase.User) => {
        if (user == null) {
          return false;
        } else {
          return user.uid !== '';
        }
      })
    );
  }

  signOut(withRedirect: boolean = true) {
    this._firebaseAuth.signOut().then((res) => {
      if (withRedirect) {
        this._router.navigate(['/login']);
      }
    });
  }

  checkResetPasswordCode(oobCode: string) {
    return this._firebaseAuth
      .checkActionCode(oobCode)
      .then((actionCodeInfo) => {
        if (actionCodeInfo.operation === 'PASSWORD_RESET') {
          return { isValid: true, email: actionCodeInfo.data.email };
        }
      })
      .catch(() => {
        return { isValid: false, email: '' };
      });
  }

  get connectedAs(): User {
    return this._connectedAs;
  }

  set connectedAs(user: User) {
    this._connectedAs = user;
  }

  setCurrentUser(user: User) {
    if (isNullOrUndefined(this._user) || user.id == this._user.id) {
      this._user = user;
      this.currentUser.next(user);
    }
  }
}
