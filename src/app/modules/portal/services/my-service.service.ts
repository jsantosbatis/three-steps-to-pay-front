import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MayHaveId } from '../interfaces/MayHaveId';

@Injectable({
  providedIn: 'root',
})
export class MyService<T extends MayHaveId> {
  protected apiPath: string;
  constructor(protected http: HttpClient) {}

  getAll(): Observable<T[]> {
    return this.http.get<T[]>(this.apiPath);
  }
  getById(id: string): Observable<T> {
    return this.http.get<T>(`${this.apiPath}/${id}`);
  }
  insert(data: T): Observable<T> {
    return this.http.post<T>(this.apiPath, data);
  }
  update(data: T): Observable<T> {
    return this.http.put<T>(`${this.apiPath}/${data.id}`, data);
  }
  delete(id: string): Observable<Object> {
    return this.http.delete(`${this.apiPath}/${id}`);
  }
}
