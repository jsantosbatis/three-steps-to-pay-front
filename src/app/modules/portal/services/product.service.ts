import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../interfaces/Product';
import { MyService } from './my-service.service';

@Injectable({
  providedIn: 'root',
})
export class ProductService extends MyService<Product> {
  constructor(protected http: HttpClient) {
    super(http);
    this.apiPath = '/api/products';
  }

  generateQRCode(id: string): Observable<ArrayBuffer> {
    return this.http.get(`${this.apiPath}/qrcode/${id}`, {
      responseType: 'arraybuffer',
    });
  }

  generatePayconiqQRCode(id: string): Observable<ArrayBuffer> {
    return this.http.get(`${this.apiPath}/qrcode/payconiq/${id}`, {
      responseType: 'arraybuffer',
    });
  }
}
