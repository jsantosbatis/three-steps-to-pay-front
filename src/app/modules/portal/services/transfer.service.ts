import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Transfer } from '@portal/interfaces/Transfer'
import { Observable } from 'rxjs'
import { MyService } from './my-service.service'

@Injectable({
  providedIn: 'root',
})
export class TransferService extends MyService<Transfer> {
  constructor(protected http: HttpClient) {
    super(http)
    this.apiPath = '/api/transfers'
  }

  treated(id: string): Observable<Transfer> {
    return this.http.post<Transfer>(`${this.apiPath}/${id}/treated`, {})
  }
}
