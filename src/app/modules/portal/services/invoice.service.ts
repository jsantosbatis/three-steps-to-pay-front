import { Injectable } from '@angular/core';
import { MyService } from './my-service.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Invoice } from '../interfaces/Invoice';
import { Transfer } from '@portal/interfaces/Transfer';

@Injectable({
  providedIn: 'root',
})
export class InvoiceService extends MyService<Invoice> {
  constructor(protected http: HttpClient) {
    super(http);
    this.apiPath = '/api/invoices';
  }

  getByPeriod(startDate: string, endDate: string): Observable<Invoice[]> {
    return this.http.get<Invoice[]>(`${this.apiPath}`, {
      params: {
        start_date: startDate,
        end_date: endDate,
      } as any,
    });
  }

  getTransfers(id: string | number): Observable<Transfer[]> {
    return this.http.get<Transfer[]>(`${this.apiPath}/${id}/transfers`);
  }

  download(id: string): Observable<ArrayBuffer> {
    return this.http.get(`${this.apiPath}/${id}/download`, {
      responseType: 'arraybuffer',
    });
  }
}
