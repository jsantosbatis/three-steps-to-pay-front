import { Injectable } from '@angular/core';
import { MyService } from './my-service.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Transaction } from '../interfaces/Transaction';
import { TotalSales } from '../interfaces/TotalSales';

@Injectable({
  providedIn: 'root',
})
export class TransactionService extends MyService<Transaction> {
  constructor(protected http: HttpClient) {
    super(http);
    this.apiPath = '/api/transactions';
  }

  getTotalSales(startDate: string, endDate: string): Observable<TotalSales> {
    return this.http.get<TotalSales>(`${this.apiPath}/total_sales`, {
      params: {
        start_date: startDate,
        end_date: endDate,
      } as any,
    });
  }

  getLastSales(): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(`${this.apiPath}/last_sales`);
  }
}
