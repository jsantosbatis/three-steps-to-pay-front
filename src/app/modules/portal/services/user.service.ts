import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyService } from './my-service.service';
import { Observable } from 'rxjs';
import { User } from '../interfaces/User';

@Injectable({
  providedIn: 'root',
})
export class UserService extends MyService<User> {
  constructor(protected http: HttpClient) {
    super(http);
    this.apiPath = '/api/users';
  }

  findUserByGoogleId(id: string): Observable<User> {
    return this.http.get<User>(`${this.apiPath}/find_by_google_id/${id}`);
  }

  sendPasswordResetLink(user: User): Observable<boolean> {
    return this.http.get<boolean>(
      `${this.apiPath}/send_password_reset_link/${user.id}`
    );
  }

  resetPassword(code: string, password: string): Observable<boolean> {
    return this.http.post<boolean>(`${this.apiPath}/reset_password`, {
      code: code,
      password: password,
    });
  }
}
