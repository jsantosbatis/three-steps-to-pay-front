import { Injectable } from '@angular/core';
import { MyService } from './my-service.service';
import { Product } from '@app/api-interfaces';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PayconiqService extends MyService<Product> {
  constructor(protected http: HttpClient) {
    super(http);
    this.apiPath = '/api/payconiq';
  }
}
