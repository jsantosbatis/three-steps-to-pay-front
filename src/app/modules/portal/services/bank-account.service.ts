import { Injectable } from '@angular/core';
import { MyService } from './my-service.service';
import { HttpClient } from '@angular/common/http';
import { BankAccount } from '../interfaces/BankAccount';

@Injectable({
  providedIn: 'root',
})
export class BankAccountService extends MyService<BankAccount> {
  constructor(protected http: HttpClient) {
    super(http);
    this.apiPath = '/api/bank_accounts';
  }
}
