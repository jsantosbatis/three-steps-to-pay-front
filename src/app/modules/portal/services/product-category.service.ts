import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyService } from './my-service.service';
import { ProductCategory } from '@portal/interfaces/ProductCategory';

@Injectable({
  providedIn: 'root',
})
export class ProductCategoryService extends MyService<ProductCategory> {
  constructor(protected http: HttpClient) {
    super(http);
    this.apiPath = '/api/product_categories';
  }

  fromLang(lang: string) {
    return this.http.get<ProductCategory[]>(
      `${this.apiPath}/from_lang/${lang}`
    );
  }
}
