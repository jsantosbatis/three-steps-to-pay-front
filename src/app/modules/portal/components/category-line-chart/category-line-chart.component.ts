import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { SalesCategory } from '@portal/interfaces/SalesCategory';
import { getCategoryColor } from '../../util';
import { Chart } from 'chart.js';
import { uniq, groupBy } from 'lodash-es';

@Component({
  selector: 'app-category-line-chart',
  template: `<div class="h-full w-full">
    <canvas id="categoryLineChart" [hidden]="!hasData"></canvas>
    <app-empty-data *ngIf="!hasData"></app-empty-data>
  </div>`,
  styleUrls: ['./category-line-chart.component.scss'],
})
export class CategoryLineChartComponent implements OnInit, OnDestroy {
  private chart: Chart;
  hasData: boolean = false;
  constructor(private element: ElementRef) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    if (this.chart) {
      this.chart.clear();
      this.chart.destroy();
    }
  }

  public initChart(data: SalesCategory[]) {
    if (this.chart) this.chart.destroy();
    this.hasData = data && data.length > 0;
    if (!this.hasData) {
      return;
    }
    let lineCtx: any = this.element.nativeElement.querySelector(
      '#categoryLineChart'
    );
    const labels = uniq(data.map((item) => item.date)); //Dates
    let myData = groupBy(data, 'product_category_name');
    let myDataSet = [];
    //Build dataSets array for chart js
    for (const key in myData) {
      const dataSet = {
        data: [],
        fill: false,
        label: key,
        backgroundColor: getCategoryColor(key),
        borderColor: getCategoryColor(key),
      };
      myDataSet.push(dataSet);
      for (const item of myData[key]) {
        myDataSet[myDataSet.length - 1].data.push({
          x: item.date,
          y: item.total,
        });
      }
    }

    this.chart = new Chart(lineCtx, {
      type: 'line',
      data: {
        labels: labels,
        datasets: myDataSet,
      },
      options: {
        responsive: true,
        legend: {
          position: 'bottom',
          labels: {
            fontSize: 16,
          },
        },
        scales: {
          yAxes: [{ ticks: { stepSize: 5 } }],
        },
      },
    });
  }
}
