import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { getCategoryColor } from '../../util';
import { Chart } from 'chart.js';
import { sumBy, groupBy } from 'lodash-es';
import { SalesPaymentMethodType } from '@portal/interfaces/SalesPaymentMethodType';

@Component({
  selector: 'app-payment-method-type-donut-chart',
  template: `<div class="h-full w-full">
    <canvas id="paymentMethodTypeChart" [hidden]="!hasData"></canvas>
    <app-empty-data *ngIf="!hasData"></app-empty-data>
  </div>`,
  styleUrls: ['./payment-method-type-donut-chart.component.scss'],
})
export class PaymentMethodTypeDonutChartComponent implements OnInit, OnDestroy {
  private chart: Chart;
  hasData: boolean = false;
  constructor(private element: ElementRef) {}

  ngOnDestroy(): void {
    if (this.chart) {
      this.chart.clear();
      this.chart.destroy();
    }
  }

  ngOnInit(): void {}

  public initChart(data: SalesPaymentMethodType[]) {
    if (this.chart) this.chart.destroy();
    this.hasData = data && data.length > 0;
    if (!this.hasData) {
      return;
    }
    let donutCtx: any = this.element.nativeElement.querySelector(
      '#paymentMethodTypeChart'
    );
    const myLabels: string[] = data.map(
      (item) => item.payment_method_type_name
    );
    let myData = groupBy(data, 'payment_method_type_name');
    let myDataSet = [];
    for (const label of myLabels) {
      myDataSet.push(sumBy(myData[label], 'total'));
    }
    this.chart = new Chart(donutCtx, {
      type: 'doughnut',
      data: {
        labels: myLabels,
        datasets: [
          {
            data: myDataSet,
            backgroundColor: [
              getCategoryColor('Bougie'),
              getCategoryColor('Don'),
              getCategoryColor('Collecte'),
              getCategoryColor('Article'),
              getCategoryColor('Ticket'),
            ],
            borderColor: [
              getCategoryColor('Bougie'),
              getCategoryColor('Don'),
              getCategoryColor('Collecte'),
              getCategoryColor('Article'),
              getCategoryColor('Ticket'),
            ],
          },
        ],
      },
      options: {
        responsive: true,
        legend: {
          position: 'bottom',
          labels: {
            fontSize: 16,
          },
        },
      },
    });
  }
}
