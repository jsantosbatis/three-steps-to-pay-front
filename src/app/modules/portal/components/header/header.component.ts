import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
    <h2
      class="text-2xl font-bold leading-7 text-gray-900 my-2 sm:text-3xl sm:leading-9 sm:truncate"
    >
      {{ title }}
    </h2>
  `,
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() title: string;
  constructor() {}

  ngOnInit(): void {}
}
