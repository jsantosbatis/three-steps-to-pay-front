import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { SalesCategory } from '@portal/interfaces/SalesCategory';
import { Chart } from 'chart.js';
import { uniq, groupBy, sumBy } from 'lodash-es';
import { getCategoryColor } from '../../util';
@Component({
  selector: 'app-category-donut-chart',
  template: `<div class="h-full w-full">
    <canvas id="categoryDonutChart" [hidden]="!hasData"></canvas>
    <app-empty-data *ngIf="!hasData"></app-empty-data>
  </div>`,
  styleUrls: ['./category-donut-chart.component.scss'],
})
export class CategoryDonutChartComponent implements OnInit, OnDestroy {
  private chart: Chart;
  hasData: boolean = false;

  constructor(private element: ElementRef) {}

  ngOnDestroy(): void {
    if (this.chart) {
      this.chart.clear();
      this.chart.destroy();
    }
  }

  ngOnInit(): void {}

  public initChart(data: SalesCategory[]) {
    if (this.chart) this.chart.destroy();
    this.hasData = data && data.length > 0;
    if (!this.hasData) {
      return;
    }

    let donutCtx: any = this.element.nativeElement.querySelector(
      '#categoryDonutChart'
    );
    const myLabels: string[] = uniq(
      data.map((item) => item.product_category_name)
    );
    let myData = groupBy(data, 'product_category_name');
    let myDataSet = [];
    for (const label of myLabels) {
      myDataSet.push(sumBy(myData[label], 'total'));
    }

    this.chart = new Chart(donutCtx, {
      type: 'doughnut',
      data: {
        labels: myLabels,
        datasets: [
          {
            data: myDataSet,
            backgroundColor: [
              getCategoryColor('Bougie'),
              getCategoryColor('Don'),
              getCategoryColor('Collecte'),
              getCategoryColor('Article'),
              getCategoryColor('Ticket'),
            ],
            borderColor: [
              getCategoryColor('Bougie'),
              getCategoryColor('Don'),
              getCategoryColor('Collecte'),
              getCategoryColor('Article'),
              getCategoryColor('Ticket'),
            ],
          },
        ],
      },
      options: {
        responsive: true,
        legend: {
          position: 'bottom',
          labels: {
            fontSize: 16,
          },
        },
      },
    });
  }
}
