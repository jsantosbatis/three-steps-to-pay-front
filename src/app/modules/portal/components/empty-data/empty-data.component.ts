import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-empty-data',
  template: `
    <div
      class="flex flex-col justify-center items-center h-full space-y-3 py-2"
    >
      <div
        class="w-16 text-cool-gray-400"
        aria-label="candle"
        [inlineSVG]="
          '/assets/images/heroic-icons/outline/information-circle.svg'
        "
      ></div>
      <p class="text-cool-gray-500 text-lg">Aucune donnée disponible</p>
    </div>
  `,
  styleUrls: ['./empty-data.component.scss'],
})
export class EmptyDataComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
