import { Component, OnInit, Input } from '@angular/core';
import { BankAccount } from '@portal/interfaces/BankAccount';

@Component({
  selector: 'app-bank-account-tile',
  template: `
    <div
      class="
      bg-white px-4 py-5 border-b border-gray-200 sm:px-6 shadow sm:rounded-lg cursor-pointer"
      [routerLink]="['/portal/bank-account', data.id]"
    >
      <div class="flex justify-between py-2 px-3">
        <div class="flex flex-col">
          <p class="text-sm text-gray-500">
            {{ data.name }}
          </p>
          <p class="text-lg text-gray-900">{{ data.number | bankAccount }}</p>
        </div>
        <i class="fas fa-money-check fa-2x text-primary-500"></i>
      </div>
    </div>
  `,
  styleUrls: ['./bank-account-tile.component.scss'],
})
export class BankAccountTileComponent implements OnInit {
  @Input() data: BankAccount;
  constructor() {}

  ngOnInit() {}
}
