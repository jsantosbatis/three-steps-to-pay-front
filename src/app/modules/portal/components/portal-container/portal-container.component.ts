import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portal-container',
  template: `
    <div class="min-h-screen bg-gray-100">
      <app-nav-bar></app-nav-bar>
      <router-outlet></router-outlet>
    </div>
  `,
  styleUrls: ['./portal-container.component.scss'],
})
export class AppPortalContainerComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
