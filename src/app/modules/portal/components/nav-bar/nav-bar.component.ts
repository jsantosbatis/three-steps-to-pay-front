import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { User } from '@portal/interfaces/User';
import { AuthService } from '@portal/services/auth.service';

@Component({
  selector: 'app-nav-bar',
  template: `
    <nav class="bg-white border-b border-gray-200">
      <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
          <div class="flex">
            <div class="flex-shrink-0 flex items-center">
              <a class="flex items-center" [routerLink]="['dashboard']">
                <img
                  class="block lg:hidden h-14 w-auto"
                  src="assets/icon-app.png"
                  alt=""
                />
                <img
                  class="hidden lg:block h-14 w-auto"
                  src="assets/icon-app.png"
                  alt=""
                />
              </a>
            </div>
            <div class="hidden sm:-my-px sm:ml-6 sm:flex">
              <a
                [routerLink]="['dashboard']"
                [routerLinkActive]="[
                  'border-primary-500',
                  'focus:outline-none',
                  'focus:border-primary-700',
                  'text-gray-700'
                ]"
                class="inline-flex items-center px-1 pt-1 border-b-2 border-transparent text-sm font-medium leading-5 text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out"
              >
                Accueil
              </a>
              <a
                [routerLink]="['point-of-sales']"
                [appActive]="[
                  'border-primary-500',
                  'focus:outline-none',
                  'focus:border-primary-700',
                  'text-gray-700'
                ]"
                [appActiveRoute]="[
                  'point-of-sales',
                  'point-of-sale',
                  'product'
                ]"
                class="ml-8 inline-flex items-center px-1 pt-1 border-b-2 border-transparent text-sm font-medium leading-5 text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out"
              >
                Point de vente
              </a>
              <a
                [routerLink]="['bank-accounts']"
                [appActive]="[
                  'border-primary-500',
                  'focus:outline-none',
                  'focus:border-primary-700',
                  'text-gray-700'
                ]"
                [appActiveRoute]="['bank-accounts', 'bank-account']"
                class="ml-8 inline-flex items-center px-1 pt-1 border-b-2 border-transparent text-sm font-medium leading-5 text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out"
              >
                Compte bancaire
              </a>
              <a
                [routerLink]="['invoices']"
                [routerLinkActive]="[
                  'border-primary-500',
                  'focus:outline-none',
                  'focus:border-primary-700',
                  'text-gray-700'
                ]"
                class="ml-8 inline-flex items-center px-1 pt-1 border-b-2 border-transparent text-sm font-medium leading-5 text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out"
              >
                Facture
              </a>
              <a
                *ngIf="isAdmin"
                [routerLink]="['users']"
                [routerLinkActive]="[
                  'border-primary-500',
                  'focus:outline-none',
                  'focus:border-primary-700',
                  'text-gray-700'
                ]"
                class="ml-8 inline-flex items-center px-1 pt-1 border-b-2 border-transparent text-sm font-medium leading-5 text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out"
              >
                Utilisateur
              </a>
            </div>
          </div>
          <div
            class="hidden sm:ml-6 sm:flex sm:items-center"
            *ngIf="currentUser$ | async"
          >
            <div
              (clickOutside)="isUserProfileOpen = false"
              [exclude]="'#mobileDropdown'"
              class="ml-3 relative"
            >
              <div>
                <button
                  (click)="isUserProfileOpen = !isUserProfileOpen"
                  class="flex items-center text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 transition duration-150 ease-in-out"
                >
                  <span
                    class="inline-block h-8 w-8 rounded-full overflow-hidden bg-gray-100"
                  >
                    <svg
                      class="h-full w-full text-gray-300"
                      fill="currentColor"
                      viewBox="0 0 24 24"
                    >
                      <path
                        d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z"
                      />
                    </svg>
                  </span>
                </button>
              </div>
              <div
                *ngIf="currentUser$ | async"
                [hidden]="!isUserProfileOpen"
                class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg z-10"
              >
                <div class="py-1 rounded-md bg-white shadow-xs">
                  <span class="flex-shrink-0 group block focus:outline-none">
                    <div class="flex items-center py-2 px-4">
                      <span
                        class="inline-block h-8 w-8 rounded-full overflow-hidden bg-gray-100"
                      >
                        <svg
                          class="h-full w-full text-gray-300"
                          fill="currentColor"
                          viewBox="0 0 24 24"
                        >
                          <path
                            d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z"
                          />
                        </svg>
                      </span>
                      <div class="ml-3 min-w-0" *ngIf="!connectedAs()">
                        <p
                          class="truncate text-sm leading-5 font-medium text-gray-700"
                        >
                          {{ (currentUser$ | async)?.company.name }}
                        </p>
                        <p
                          class="truncate text-xs leading-4 font-medium text-gray-500 transition ease-in-out duration-150"
                        >
                          {{ (currentUser$ | async)?.email }}
                        </p>
                      </div>
                      <div class="ml-3 min-w-0" *ngIf="connectedAs()">
                        <p
                          class="truncate text-sm leading-5 font-medium text-gray-700"
                        >
                          {{ connectedAs().company.name }}
                        </p>
                        <p
                          class="truncate text-xs leading-4 font-medium text-gray-500 transition ease-in-out duration-150"
                        >
                          {{ connectedAs().email }}
                        </p>
                      </div>
                    </div>
                  </span>
                  <button
                    (click)="navigateToUserPage()"
                    class="block w-full text-left px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out"
                  >
                    Votre compte
                  </button>
                  <button
                    (click)="signOut()"
                    class="block w-full text-left px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out"
                  >
                    Déconnexion
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div id="mobileDropdown" class="-mr-2 flex items-center sm:hidden">
            <button
              (click)="isUserProfileOpen = !isUserProfileOpen"
              class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out"
            >
              <svg
                class="h-6 w-6"
                stroke="currentColor"
                fill="none"
                viewBox="0 0 24 24"
              >
                <path
                  [ngClass]="{
                    hidden: isUserProfileOpen,
                    'inline-flex': !isUserProfileOpen
                  }"
                  class="inline-flex"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M4 6h16M4 12h16M4 18h16"
                />
                <path
                  [ngClass]="{
                    hidden: !isUserProfileOpen,
                    'inline-flex': isUserProfileOpen
                  }"
                  class="hidden"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </button>
          </div>
        </div>
      </div>
      <div
        [ngClass]="{
          block: isUserProfileOpen,
          hidden: !isUserProfileOpen
        }"
        class="hidden sm:hidden"
      >
        <div class="pt-2 pb-3">
          <a
            [routerLink]="['dashboard']"
            [routerLinkActive]="['border-primary-500']"
            class="block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none focus:text-gray-800 focus:bg-gray-50 focus:border-gray-300 transition duration-150 ease-in-out"
          >
            Accueil
          </a>
          <a
            [routerLink]="['point-of-sales']"
            [appActive]="['border-primary-500']"
            [appActiveRoute]="['point-of-sales', 'point-of-sale', 'product']"
            class="mt-1 block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none focus:text-gray-800 focus:bg-gray-50 focus:border-gray-300 transition duration-150 ease-in-out"
          >
            Point de vente
          </a>
          <a
            [routerLink]="['invoices']"
            [routerLinkActive]="['border-primary-500']"
            class="mt-1 block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none focus:text-gray-800 focus:bg-gray-50 focus:border-gray-300 transition duration-150 ease-in-out"
          >
            Facture
          </a>
          <a
            *ngIf="isAdmin"
            [routerLink]="['users']"
            [routerLinkActive]="['border-primary-500']"
            class="mt-1 block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300 focus:outline-none focus:text-gray-800 focus:bg-gray-50 focus:border-gray-300 transition duration-150 ease-in-out"
          >
            Utilisateur
          </a>
        </div>
        <div class="pt-4 pb-3 border-t border-gray-200">
          <div class="flex items-center px-4">
            <div class="flex-shrink-0">
              <span
                class="inline-block h-8 w-8 rounded-full overflow-hidden bg-gray-100"
              >
                <svg
                  class="h-full w-full text-gray-300"
                  fill="currentColor"
                  viewBox="0 0 24 24"
                >
                  <path
                    d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z"
                  />
                </svg>
              </span>
            </div>
            <div class="ml-3 min-w-0">
              <div
                class="truncate text-base font-medium leading-6 text-gray-800"
              >
                {{ (currentUser$ | async)?.company.name }}
              </div>
              <div class="truncate text-sm font-medium leading-5 text-gray-500">
                {{ (currentUser$ | async)?.email }}
              </div>
            </div>
          </div>
          <div class="mt-3">
            <button
              (click)="navigateToUserPage()"
              class="block text-left w-full px-4 py-2 text-base font-medium text-gray-500 hover:text-gray-800 hover:bg-gray-100 focus:outline-none focus:text-gray-800 focus:bg-gray-100 transition duration-150 ease-in-out"
            >
              Votre compte
            </button>
            <button
              (click)="signOut()"
              class="w-full text-left mt-1 block px-4 py-2 text-base font-medium text-gray-500 hover:text-gray-800 hover:bg-gray-100 focus:outline-none focus:text-gray-800 focus:bg-gray-100 transition duration-150 ease-in-out"
            >
              Déconnexion
            </button>
          </div>
        </div>
      </div>
    </nav>
  `,
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit {
  isUserProfileOpen: boolean;
  isAdmin: boolean;
  currentUser$: Observable<User>;
  user: User = null;
  constructor(private _authService: AuthService, private _router: Router) {}

  async ngOnInit() {
    this.isUserProfileOpen = false;
    this.isAdmin = await this._authService.isAdmin();
    this.currentUser$ = this._authService.currentUser$;
    this.currentUser$.subscribe((data) => (this.user = data));
  }

  toggleDropdownAccount() {
    this.isUserProfileOpen = !this.isUserProfileOpen;
  }

  navigateToUserPage() {
    this.onClickedOutside(null);
    this._router.navigate(['portal/user', this.user.id]);
  }

  connectedAs() {
    return this._authService.connectedAs;
  }

  onClickedOutside(e: Event) {
    if (this.isUserProfileOpen) {
      this.toggleDropdownAccount();
    }
  }

  signOut() {
    this._authService.connectedAs = null;
    this._authService.signOut();
  }
}
