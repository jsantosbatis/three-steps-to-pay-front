import { Component, OnInit, Input } from '@angular/core';
import { PointOfSale } from '@portal/interfaces/PointOfSale';

@Component({
  selector: 'app-point-of-sale-tile',
  template: `
    <div
      class="
      bg-white px-4 py-5 border-b border-gray-200 sm:px-6 shadow rounded-lg cursor-pointer"
      [routerLink]="['/portal/point-of-sale', data.id]"
    >
      <div class="flex justify-between py-2 px-3">
        <div class="flex flex-col">
          <p class="text-lg text-gray-900">
            {{ data.name }}
          </p>
          <p class="text-sm text-gray-500">
            {{ data.count_products }} produits
          </p>
        </div>
        <i class="fas fa-store fa-2x text-primary-500"></i>
      </div>
    </div>
  `,
  styleUrls: ['./point-of-sale-tile.component.scss'],
})
export class PointOfSaleTileComponent implements OnInit {
  @Input() data: PointOfSale;
  constructor() {}

  ngOnInit() {}
}
