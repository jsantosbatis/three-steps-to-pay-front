import { CommonModule } from '@angular/common'
import { ModuleWithProviders, NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { ClickOutsideModule } from 'ng-click-outside'
import { InlineSVGModule } from 'ng-inline-svg'
import { NgxMaskModule } from 'ngx-mask'
import { NgxSpinnerModule } from 'ngx-spinner'
import { AlertComponent } from './components/ui/alert/alert.component'
import { ContainerComponent } from './components/ui/container/container.component'
import { FormTextComponent } from './components/ui/input/form-text.component'
import { InputPercentTotalComponent } from './components/ui/input/input-percent-total.component'
import { ModalComponent } from './components/ui/modal/modal.component'
import { ActiveDirective } from './directives/active.directive'
import { BankAccountPipe } from './pipes/bank-account.pipe'
import { EmptyPipe } from './pipes/empty.pipe'
import { ReplacePipe } from './pipes/replace.pipe'
import { SortByPipe } from './pipes/sort-by.pipe'
import { AlertService } from './services/alert.service'

@NgModule({
  declarations: [
    BankAccountPipe,
    EmptyPipe,
    AlertComponent,
    ModalComponent,
    FormTextComponent,
    ActiveDirective,
    ContainerComponent,
    InputPercentTotalComponent,
    SortByPipe,
    ReplacePipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
    FontAwesomeModule,
    NgxSpinnerModule,
    RouterModule,
    ClickOutsideModule,
    InlineSVGModule.forRoot(),
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    BankAccountPipe,
    EmptyPipe,
    RouterModule,
    NgxSpinnerModule,
    AlertComponent,
    ModalComponent,
    FormTextComponent,
    ContainerComponent,
    ActiveDirective,
    ClickOutsideModule,
    InputPercentTotalComponent,
    SortByPipe,
    InlineSVGModule,
    ReplacePipe,
  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [AlertService],
    }
  }
}
