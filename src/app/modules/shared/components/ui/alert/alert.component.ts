import { Component, OnInit } from '@angular/core';
import {
  trigger,
  style,
  animate,
  transition,
  state,
} from '@angular/animations';
import { AlertService } from '../../../services/alert.service';

@Component({
  selector: 'app-alert',
  template: `
    <div
      class="fixed inset-0 flex items-end justify-center px-4 py-6 pointer-events-none sm:p-6 sm:items-start sm:justify-end"
      [@openClose]="isVisible ? 'open' : 'closed'"
      [ngStyle]="{ 'z-index': isVisible ? 10 : -10 }"
    >
      <div
        class="max-w-sm w-full bg-white shadow-lg rounded-lg pointer-events-auto"
      >
        <div class="rounded-lg shadow-xs overflow-hidden">
          <div class="p-4">
            <div class="flex items-start">
              <div class="flex-shrink-0">
                <svg
                  class="h-6 w-6 text-green-400"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                  />
                </svg>
              </div>
              <div class="ml-3 w-0 flex-1 pt-0.5">
                <p class="text-sm leading-5 font-medium text-gray-900">
                  {{ title }}
                </p>
                <p
                  class="mt-1 text-sm leading-5 text-gray-500"
                  *ngIf="message === ''"
                >
                  {{ message }}
                </p>
              </div>
              <div class="ml-4 flex-shrink-0 flex">
                <button
                  class="inline-flex text-gray-400 focus:outline-none focus:text-gray-500 transition ease-in-out duration-150"
                >
                  <svg class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path
                      fill-rule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clip-rule="evenodd"
                    />
                  </svg>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./alert.component.scss'],
  animations: [
    trigger('openClose', [
      state(
        'open',
        style({
          opacity: 1,
        })
      ),
      state(
        'closed',
        style({
          opacity: 0,
          transform: 'translateY(0.5rem)',
        })
      ),
      transition('open => closed', [animate('300ms ease-out')]),
      transition('closed => open', [animate('100ms ease-in')]),
    ]),
  ],
})
export class AlertComponent implements OnInit {
  isVisible: boolean = false;
  title: string;
  message: string;
  constructor(private _alert: AlertService) {}

  ngOnInit() {
    this._alert.register(this);
  }

  success(title: string, message: string) {
    this.title = title;
    this.message = message;
    this._toggle();
  }

  hide() {
    this.isVisible = false;
  }
  private _toggle() {
    this.isVisible = !this.isVisible;
  }
}
