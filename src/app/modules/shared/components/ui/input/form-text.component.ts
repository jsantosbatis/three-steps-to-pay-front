import { Component, Injector, Input } from '@angular/core'
import { NG_VALUE_ACCESSOR } from '@angular/forms'
import { ControlValueAccessorConnector } from './control-value-accessor-connector.ts'
@Component({
  selector: 'app-form-text',
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: FormTextComponent, multi: true },
  ],
  styles: [
    `
      /* 
        TailwindCsss style could be applied based on props of the component container
        Disable it otherwise there are weird behaviours.
      */
      :host {
        all: unset !important;
      }
    `,
  ],
  template: `
    <!-- HORIZONTAL -->
    <div
      *ngIf="direction == 'horizontal'"
      class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
      [ngClass]="{
        'mt-6 sm:mt-5': marginTop
      }"
    >
      <label
        [for]="id"
        class="block text-sm font-medium leading-5 text-gray-700 sm:mt-px sm:pt-2"
      >
        {{ label }} {{ required ? '*' : '' }}
      </label>
      <div class="mt-1 sm:mt-0 sm:col-span-2">
        <div class="max-w-xs rounded-md shadow-sm">
          <input
            [id]="id"
            [type]="type"
            [placeholder]="placeholder"
            [formControl]="control"
            [mask]="customMask"
            [readonly]="readonly"
            class="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5"
            [ngClass]="{ 'bg-gray-100 cursor-not-allowed': readonly }"
          />
        </div>
      </div>
    </div>
    <!-- VERTICAL -->
    <div
      *ngIf="direction == 'vertical'"
      [ngClass]="{
        'grid grid-cols-1 row-gap-6 col-gap-4 sm:grid-cols-6': !fullWidth,
        'mt-6': marginTop
      }"
    >
      <div
        [ngClass]="{
          'sm:col-span-4': !fullWidth
        }"
      >
        <label
          [for]="id"
          class="block text-sm font-medium leading-5 text-gray-700"
        >
          {{ label }} {{ required ? '*' : '' }}
        </label>
        <div class="mt-1 rounded-md shadow-sm">
          <input
            [id]="id"
            [type]="type"
            [placeholder]="placeholder"
            [formControl]="control"
            [mask]="customMask"
            [readonly]="readonly"
            class="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 "
            [ngClass]="{ 'bg-gray-100 cursor-not-allowed': readonly }"
          />
        </div>
      </div>
    </div>
  `,
})
export class FormTextComponent extends ControlValueAccessorConnector {
  @Input() required: Boolean = false
  @Input() type: String = 'text'
  @Input() placeholder: String = ''
  @Input() label: String = 'My label'
  @Input() direction: 'vertical' | 'horizontal' = 'horizontal'
  @Input() fullWidth: Boolean = false
  @Input() marginTop: Boolean = true
  @Input() readonly: Boolean = false
  @Input() customMask?: String
  @Input() id: String =
    Math.random().toString(36).substring(2, 15) +
    Math.random().toString(36).substring(2, 15)
  @Input()
  formControlName: string

  constructor(injector: Injector) {
    super(injector)
  }
}
