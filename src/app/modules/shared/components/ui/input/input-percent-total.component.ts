import { Component, Injector, Input } from '@angular/core'
import { NG_VALUE_ACCESSOR } from '@angular/forms'
import { ControlValueAccessorConnector } from './control-value-accessor-connector.ts'
@Component({
  selector: 'app-input-percent-total',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputPercentTotalComponent,
      multi: true,
    },
  ],
  template: `
    <div class="max-w-xs rounded-md shadow-sm relative">
      <div
        class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none"
      >
        <span class="text-gray-500 sm:text-sm sm:leading-5"> % </span>
      </div>
      <div
        class="absolute inset-y-0 right-0 pr-10 flex items-center pointer-events-none"
      >
        <span class="text-gray-500 sm:text-sm sm:leading-5">
          {{
            (control.value / 100) * realValue
              | currency: 'EUR'
              | replace: ',':'.'
          }}
        </span>
      </div>
      <input
        #myInput
        class="pl-7 form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5"
        type="number"
        step="10"
        min="0"
        max="100"
        (focus)="myInput.select()"
        [formControl]="control"
        [readonly]="readonly"
        [ngClass]="{ 'bg-gray-100 cursor-not-allowed': readonly }"
      />
    </div>
  `,
})
export class InputPercentTotalComponent extends ControlValueAccessorConnector {
  @Input() realValue: number
  @Input() id: String =
    Math.random().toString(36).substring(2, 15) +
    Math.random().toString(36).substring(2, 15)

  @Input()
  formControlName: string
  @Input() readonly: Boolean = false

  constructor(injector: Injector) {
    super(injector)
  }
}
