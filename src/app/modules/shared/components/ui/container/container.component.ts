import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-container',
  template: `
    <div class="{{ extraClass }}">
      <div class="mx-4 sm:mx-8">
        <ng-content></ng-content>
      </div>
    </div>
  `
})
export class ContainerComponent implements OnInit {
  @Input() extraClass = 'py-5';
  constructor() {}

  ngOnInit() {}
}
