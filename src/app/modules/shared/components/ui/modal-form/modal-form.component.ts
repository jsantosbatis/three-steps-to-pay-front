import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-form',
  template: `
    <div
      *ngIf="isOpen"
      class="fixed bottom-0 inset-x-0 px-4 pb-4 sm:inset-0 sm:flex sm:items-center sm:justify-center"
    >
      <div *ngIf="isOpen" class="fixed inset-0 transition-opacity">
        <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
      </div>

      <div
        [hidden]="!isOpen"
        class="bg-white rounded-lg overflow-hidden shadow-xl transform transition-all sm:max-w-lg sm:w-full"
      >
        <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
          <div class="hidden sm:block absolute top-0 right-0 pt-4 pr-4">
            <button
              (click)="isOpen = false"
              type="button"
              class="text-gray-400 hover:text-gray-500 focus:outline-none focus:text-gray-500 transition ease-in-out duration-150"
            >
              <svg
                class="h-6 w-6"
                stroke="currentColor"
                fill="none"
                viewBox="0 0 24 24"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </button>
          </div>
          <div class="sm:flex sm:items-start">
            <div
              class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-primary-100 sm:mx-0 sm:h-10 sm:w-10"
            >
              <svg
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                class="h-6 w-6 text-primary-600"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"
                />
              </svg>
            </div>
            <div
              class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left w-full sm:mr-4"
            >
              <h3 class="text-lg leading-6 font-medium text-gray-900">
                <ng-content select="[modalHeader]"></ng-content>
              </h3>
              <div class="mt-2">
                <ng-content select="[modalBody]"></ng-content>
              </div>
            </div>
          </div>
        </div>
        <ng-content select="[modalFooter]"></ng-content>
      </div>
    </div>
  `,
  styleUrls: ['./modal-form.component.scss'],
})
export class ModalFormComponent implements OnInit {
  isOpen: Boolean = false;
  constructor() {}
  ngOnInit() {}

  toggle() {
    this.isOpen = !this.isOpen;
  }

  open() {
    this.isOpen = true;
  }

  close() {
    this.isOpen = false;
  }
}
