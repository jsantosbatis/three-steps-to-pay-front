import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-modal',
  template: `
    <div
      *ngIf="isOpen"
      class="fixed bottom-0 inset-x-0 px-4 pb-4 sm:inset-0 sm:flex sm:items-center sm:justify-center z-50"
    >
      <div *ngIf="isOpen" class="fixed inset-0 transition-opacity">
        <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
      </div>

      <div
        [hidden]="!isOpen"
        class="relative bg-white rounded-lg px-4 pt-5 pb-4 overflow-hidden shadow-xl transform transition-all sm:max-w-lg sm:w-full sm:p-6"
      >
        <div class="hidden sm:block absolute top-0 right-0 pt-4 pr-4">
          <button
            (click)="isOpen = false"
            type="button"
            class="text-gray-400 hover:text-gray-500 focus:outline-none focus:text-gray-500 transition ease-in-out duration-150"
          >
            <svg
              class="h-6 w-6"
              stroke="currentColor"
              fill="none"
              viewBox="0 0 24 24"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          </button>
        </div>
        <div class="sm:flex sm:items-start">
          <ng-content select="[modalIconHeader]"></ng-content>
          <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
            <h3 class="text-lg leading-6 font-medium text-gray-900">
              <ng-content select="[modalHeader]"></ng-content>
            </h3>
            <div class="mt-2">
              <p class="text-sm leading-5 text-gray-500">
                <ng-content select="[modalBody]"></ng-content>
              </p>
            </div>
          </div>
        </div>
        <ng-content select="[modalFooter]"></ng-content>
      </div>
    </div>
  `,
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  isOpen: Boolean = false;
  constructor() {}
  ngOnInit() {}

  toggle() {
    this.isOpen = !this.isOpen;
  }

  open() {
    this.isOpen = true;
  }

  close() {
    this.isOpen = false;
  }

  save() {}
}
