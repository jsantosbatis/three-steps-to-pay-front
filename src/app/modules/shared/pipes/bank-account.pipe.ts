import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'bankAccount' })
export class BankAccountPipe implements PipeTransform {
  transform(value: string): string {
    if (value) {
      return `${value.substr(0, 4)} ${value.substr(4, 4)} ${value.substr(
        8,
        4
      )} ${value.substr(12, 4)}`;
    } else return '';
  }
}
