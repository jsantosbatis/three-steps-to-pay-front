import { Pipe, PipeTransform } from '@angular/core';
import { sortBy } from 'lodash-es';
@Pipe({
  name: 'sortBy',
})
export class SortByPipe implements PipeTransform {
  transform(value: any[], columns: string[]): unknown {
    return sortBy(value, columns);
  }
}
