import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Directive({
  selector: '[appActive]',
})
export class ActiveDirective implements OnInit {
  @Input('appActive') classes: Array<string>;
  @Input('appActiveRoute') activeRoutes: Array<string>;
  constructor(
    private _el: ElementRef,
    private _router: Router,
    private _renderer: Renderer2
  ) {}

  ngOnInit(): void {
    this._router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        if (this.isRightUrl(event.url)) {
          this.addClass();
        } else {
          this.removeClass();
        }
      }
    });

    if (this.isRightUrl(window.location.toString())) {
      this.addClass();
    } else {
      this.removeClass();
    }
  }

  isRightUrl(url): boolean {
    return this.activeRoutes.some((value) => url.includes(value));
  }

  addClass() {
    for (let classe of this.classes) {
      this._renderer.addClass(this._el.nativeElement, classe);
    }
  }

  removeClass() {
    for (let classe of this.classes) {
      this._renderer.removeClass(this._el.nativeElement, classe);
    }
  }
}
