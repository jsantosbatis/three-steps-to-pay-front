import { Injectable } from '@angular/core';
import { AlertComponent } from '../components/ui/alert/alert.component';
import { timer } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  _alerts: AlertComponent[] = [];
  constructor() {}

  register(alert: AlertComponent) {
    this._alerts.push(alert);
  }

  success(title: string, message: string = '') {
    this._alerts[0].success(title, message);
    timer(100, 2000)
      .pipe(take(2))
      .subscribe(null, null, () => {
        this._alerts[0].hide();
      });
  }
}
