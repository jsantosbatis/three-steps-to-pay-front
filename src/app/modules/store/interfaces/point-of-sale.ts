import { Product } from './product';

export interface PointOfSale {
  id: number;
  token: string;
  name: string;
  postal_code: string;
  city: string;
  street: string;
  products?: Product[];
}
