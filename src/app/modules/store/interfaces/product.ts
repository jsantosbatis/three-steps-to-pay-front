import { PointOfSale } from './point-of-sale';

export interface Product {
  id: number;
  name: string;
  price: number;
  token?: string;
  point_of_sale_id: number;
  point_of_sale_name: string;
  product_category_id: number;
  point_of_sale?: PointOfSale;
}
