export interface ProductCart {
  id: number;
  name: string;
  price: number;
  point_of_sale_id: number;
  point_of_sale_name: string;
  product_category_id: number;
  quantity?: number;
}
