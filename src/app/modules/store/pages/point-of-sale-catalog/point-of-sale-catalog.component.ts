import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { PointOfSale } from '../../interfaces/point-of-sale';
import { ProductCategory } from '@enums/product-category';
import { Store, Select } from '@ngxs/store';
import { SetCurrentPointOfSale } from '../../actions/point-of-sales.action';
import { PointOfSalesState } from '../../states/point-of-sales.state';
import { Observable, Subscription } from 'rxjs';
import { Product } from '../../interfaces/product';
import { map } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { ResetPaymentDirectState } from '../../actions/payment-direct.action';
import { PaymentDirectState } from '../../states/payment-direct.state';
import { ClearShopppingCart } from '../../actions/shopping-cart.action';

@Component({
  selector: 'app-point-of-sale-catalog',
  template: `
    <div class="w-full lg:max-w-md mx-auto">
      <div
        class="bg-white -mx-5 -mt-4 lg:mt-0 rounded-b-lg lg:rounded-t-lg lg:shadow"
      >
        <div
          class="flex items-center space-x-5 text-cool-gray-600 py-2 lg:py-4 bg-church px-5"
        >
          <i class="fas fa-church fa-2x"></i>
          <div>
            <span class="text-base font-semibold">{{
              (current$ | async)?.name
            }}</span
            ><br />
            <span class="text-base">
              {{ (current$ | async)?.city }}
              {{ (current$ | async)?.postal_code }}
            </span>
          </div>
        </div>
        <div
          class="flex w-full justify-between text-lg px-5 bg-white sm:shadow md:shadow-none rounded-b-lg"
        >
          <div
            class="p-2 flex flex-col items-center text-cool-gray-300-svg space-y-1 lg:cursor-pointer"
            [ngClass]="{
              'border-b-2 border-primary-700 text-primary-700-svg':
                crtCategory === productCategory.CANDLE
            }"
            (click)="onCategoryChange(productCategory.CANDLE)"
          >
            <div
              class="w-6"
              aria-label="candle"
              [inlineSVG]="'/assets/images/icons/candle.svg'"
            ></div>
            <p class="text-xs tracking-wide capitalize">
              {{ 'store.category.candle' | translate }}
            </p>
          </div>
          <div
            class="p-2 text-cool-gray-300 flex flex-col items-center space-y-1 lg:cursor-pointer"
            [ngClass]="{
              'border-b-2 border-primary-700 text-primary-700':
                crtCategory === productCategory.GIFT
            }"
            (click)="onCategoryChange(productCategory.GIFT)"
          >
            <div
              class="w-6"
              aria-label="gift"
              [inlineSVG]="'/assets/images/heroic-icons/solid/heart.svg'"
            ></div>
            <p class="text-xs tracking-wide capitalize">
              {{ 'store.category.gift' | translate }}
            </p>
          </div>
          <div
            class="p-2 text-cool-gray-300 flex flex-col items-center space-y-1 lg:cursor-pointer"
            [ngClass]="{
              'border-b-2 border-primary-700 text-primary-700':
                crtCategory === productCategory.COLLECTION
            }"
            (click)="onCategoryChange(productCategory.COLLECTION)"
          >
            <div
              class="w-6"
              aria-label="collecte"
              [inlineSVG]="
                '/assets/images/heroic-icons/solid/currency-euro.svg'
              "
            ></div>
            <p class="text-xs tracking-wide capitalize">
              {{ 'store.category.collection' | translate }}
            </p>
          </div>
          <div
            class="p-2 text-cool-gray-300 flex flex-col items-center space-y-1 lg:cursor-pointer"
            [ngClass]="{
              'border-b-2 border-primary-700 text-primary-700':
                crtCategory === productCategory.ITEM
            }"
            (click)="onCategoryChange(productCategory.ITEM)"
          >
            <div
              class="w-6"
              aria-label="collecte"
              [inlineSVG]="'/assets/images/heroic-icons/solid/tag.svg'"
            ></div>
            <p class="text-xs tracking-wide capitalize">
              {{ 'store.category.item' | translate }}
            </p>
          </div>
          <div
            class="p-2 text-cool-gray-300 flex flex-col items-center space-y-1 lg:cursor-pointer"
            [ngClass]="{
              'border-b-2 border-primary-700 text-primary-700':
                crtCategory === productCategory.TICKET
            }"
            (click)="onCategoryChange(productCategory.TICKET)"
          >
            <div
              class="w-6"
              aria-label="collecte"
              [inlineSVG]="'/assets/images/heroic-icons/solid/ticket.svg'"
            ></div>
            <p class="text-xs tracking-wide capitalize">
              {{ 'store.category.ticket' | translate }}
            </p>
          </div>
        </div>
      </div>
      <div class="my-4">
        <div class="grid grid-cols-2 lg:grid-cols-3 gap-4">
          <app-product-tile
            [data]="product"
            *ngFor="let product of products$ | async"
          >
          </app-product-tile>
        </div>
        <div
          *ngIf="(products$ | async)?.length <= 0"
          class="flex flex-col justify-center items-center h-full space-y-3 py-2"
        >
          <div
            class="w-16 text-cool-gray-400"
            aria-label="empty"
            [inlineSVG]="
              '/assets/images/heroic-icons/outline/information-circle.svg'
            "
          ></div>
          <div class="text-center text-cool-gray-500">
            {{ 'store.catalog.no-items' | translate }}
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./point-of-sale-catalog.component.scss'],
})
export class PointOfSaleCatalogComponent implements OnInit, OnDestroy {
  constructor(
    private _route: ActivatedRoute,
    private _store: Store,
    private _spinner: NgxSpinnerService
  ) {}

  productCategory: typeof ProductCategory = ProductCategory;
  crtCategory: ProductCategory = ProductCategory.CANDLE;
  products$: Observable<Product[]>;
  loadingSub: Subscription;
  onChangeSub: Subscription;

  @Select(PointOfSalesState.current) current$: Observable<PointOfSale>;
  @Select(PointOfSalesState.isLoading) isLoading$: Observable<boolean>;

  ngOnInit(): void {
    this._route.paramMap.subscribe((params: ParamMap) => {
      const token = params.get('id');
      if (this.haveToReset()) {
        this._store.dispatch(new ResetPaymentDirectState());
        this._store.dispatch(new ClearShopppingCart());
      }

      this.onChangeSub = this._store
        .dispatch(new SetCurrentPointOfSale(token))
        .subscribe(() => {
          this.onCategoryChange(ProductCategory.CANDLE);
        });
    });
    this.products$ = this._store
      .select(PointOfSalesState.products)
      .pipe(map((filterFn) => filterFn(this.crtCategory)));
  }

  ngAfterViewInit(): void {
    this.loadingSub = this.isLoading$.subscribe((isLoading) => {
      isLoading ? this._spinner.show() : this._spinner.hide();
    });
  }

  ngOnDestroy(): void {
    this.loadingSub ? this.loadingSub.unsubscribe() : null;
    this.onChangeSub ? this.onChangeSub.unsubscribe() : null;
  }

  onCategoryChange(category): void {
    this.crtCategory = category;
    this.products$ = this._store
      .select(PointOfSalesState.products)
      .pipe(map((filterFn) => filterFn(this.crtCategory)));
  }

  haveToReset(): boolean {
    const crtProduct = this._store.selectSnapshot(
      PaymentDirectState.currentProduct
    );
    return crtProduct != null;
  }
}
