import { Component, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import { Store } from '@ngxs/store'
import { Subscription } from 'rxjs'
import { ClearShopppingCart } from '../../actions/shopping-cart.action'
import { HelperService } from '../../services/helper.service'
@Component({
  selector: 'app-payment-status',
  template: `
    <store-container>
      <div children class="flex flex-col items-center">
        <div class="flex flex-col items-center py-10 text-center">
          <img class="h-36 w-auto" src="assets/images/icon-app.png" alt="" />
          <div
            *ngIf="status === 'succeeded'"
            class="flex flex-col items-center space-y-6"
          >
            <svg
              fill="none"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              viewBox="0 0 24 24"
              stroke="currentColor"
              class="h-16 w-auto text-teal-400"
            >
              <path d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
            </svg>
            <p class="text-xl font-semibold text-cool-gray-700">
              {{ 'store.payment-success.message' | translate }}
            </p>
            <button
              type="button"
              class="button is-primary is-large shadow"
              (click)="goToHome()"
            >
              {{ 'store.payment-success.back-home' | translate }}
            </button>
            <p class="text-base text-cool-gray-500">ChurchPay</p>
          </div>
          <div
            *ngIf="status === 'failed'"
            class="flex flex-col items-center space-y-6"
          >
            <svg
              fill="none"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              viewBox="0 0 24 24"
              stroke="currentColor"
              class="h-16 w-auto text-red-400"
            >
              <path
                d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"
              ></path>
            </svg>
            <p class="text-xl font-semibold text-cool-gray-700 text-center">
              {{ 'store.payment-fail.error' | translate }} <br />
              {{ 'store.payment-fail.retry' | translate }}
            </p>
            <button
              type="button"
              class="button is-primary is-large shadow"
              (click)="goToHome()"
            >
              {{ 'store.payment-fail.back-home' | translate }}
            </button>
            <p class="text-base text-cool-gray-500">ChurchPay</p>
          </div>
          <div *ngIf="status === 'pending'" class="space-y-6">
            <div class="flex flex-col items-center">
              <svg
                fill="none"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                viewBox="0 0 24 24"
                stroke="currentColor"
                class="h-16 w-auto text-cool-gray-500"
              >
                <path
                  d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                ></path>
              </svg>
              <p class="text-xl font-semibold text-cool-gray-700 text-center">
                {{ 'store.payment-waiting.message' | translate }}
              </p>
              <p class="text-base text-cool-gray-500">ChurchPay</p>
            </div>
          </div>
        </div>
      </div>
    </store-container>
  `,
  styleUrls: ['./payment-status.component.scss'],
})
export class PaymentStatusComponent implements OnInit, OnDestroy {
  status: string
  paramsSub: Subscription

  constructor(
    private _route: ActivatedRoute,
    private _store: Store,
    private _helper: HelperService
  ) {}

  ngOnInit(): void {
    this.status = 'pending'
    this.paramsSub = this._route.queryParams.subscribe((params: Params) => {
      const status = params['status']
      if (status) {
        this.status = status
      }
    })
  }

  goToHome(): void {
    if (this.status === 'succeeded') {
      this._store.dispatch(new ClearShopppingCart())
    }
    this._helper.goToHome()
  }

  ngOnDestroy(): void {
    this.paramsSub.unsubscribe()
  }
}
