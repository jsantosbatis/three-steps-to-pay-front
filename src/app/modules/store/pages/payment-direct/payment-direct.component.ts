import { Component, OnDestroy, OnInit } from '@angular/core'
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms'
import { ActivatedRoute, ParamMap } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { Select, Store } from '@ngxs/store'
import { NgxSpinnerService } from 'ngx-spinner'
import { Observable, Subscription } from 'rxjs'
import { take } from 'rxjs/operators'
import { PaymentDirectState } from 'src/app/modules/store/states/payment-direct.state'
import { environment } from 'src/environments/environment'
import { SetCurrentProduct } from '../../actions/payment-direct.action'
import { ResetPointOfSaleState } from '../../actions/point-of-sales.action'
import { Product } from '../../interfaces/product'
import { PaymentService } from '../../services/payment.service'
import { ShoppingCartState } from '../../states/shopping-cart.state'

@Component({
  selector: 'app-payment-direct',
  template: `
    <div class="w-full lg:max-w-md mx-auto">
      <div class="space-y-3 -mt-4 lg:mt-0">
        <div
          class="flex items-center space-x-5 text-cool-gray-600 py-5 bg-church px-5 -mx-5
          rounded-b-lg lg:rounded-t-lg lg:shadow"
        >
          <i class="fas fa-church fa-2x"></i>
          <div>
            <span class="text-base font-semibold">{{
              (current$ | async)?.point_of_sale.name
            }}</span
            ><br />
            <span class="text-base">
              {{ (current$ | async)?.point_of_sale.city }}
              {{ (current$ | async)?.point_of_sale.postal_code }}
            </span>
          </div>
        </div>
      </div>
      <div class="mt-5 space-y-4" *ngIf="!!product">
        <div class="w-full flex items-center space-x-2">
          <app-product-category-icon
            [category]="(current$ | async)?.product_category_id"
          >
          </app-product-category-icon>
          <span class="text-xl font-medium text-cool-gray-700"
            >{{ (current$ | async)?.name }}
          </span>
        </div>
        <form [formGroup]="priceForm" autocomplete="off">
          <div class="mt-1 relative rounded-md shadow-sm">
            <div
              class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none"
            >
              <span class="text-gray-500 text-lg"> € </span>
            </div>
            <input
              type="text"
              id="price"
              class="focus:ring-indigo-500 focus:border-indigo-500 block w-full text-center pl-7 pr-12 py-5 text-3xl border-gray-300 rounded-md"
              placeholder="0.00"
              aria-describedby="price-currency"
              inputmode="numeric"
              formControlName="price"
              autocapitalize="off"
              autocomplete="off"
              spellcheck="false"
              autocorrect="off"
            />
            <div
              class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none"
            >
              <span class="text-gray-500 text-lg" id="price-currency">
                EUR
              </span>
            </div>
          </div>
        </form>
        <button
          class="button is-primary is-large shadow w-full"
          [disabled]="
            !priceForm.get('price').valid ||
            !hasEnoughAmount() ||
            hasLowerPrice() ||
            hasMaximumAmount()
          "
          (click)="proceedToCheckout()"
        >
          <span>{{ 'store.cart.button' | translate }}</span>
          <div
            class="mx-3 h-6 w-6"
            aria-label="cart"
            [inlineSVG]="'/assets/images/heroic-icons/outline/credit-card.svg'"
          ></div>
        </button>
        <div
          class="rounded-md bg-yellow-50 p-4 shadow"
          [hidden]="hasEnoughAmount()"
        >
          <div class="flex">
            <div class="flex-shrink-0">
              <svg
                class="h-5 w-5 text-yellow-400"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fill-rule="evenodd"
                  d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                  clip-rule="evenodd"
                />
              </svg>
            </div>
            <div class="ml-3">
              <h3 class="text-sm leading-5 font-medium text-yellow-800">
                {{
                  'store.cart.minimum-amount-required'
                    | translate
                      : {
                          amount:
                            minAmountValue | currency: 'EUR' | replace: ',':'.'
                        }
                }}
              </h3>
              <div class="mt-2 text-sm leading-5 text-yellow-700">
                <p>
                  {{
                    'store.cart.minimum-amount-description'
                      | translate
                        : {
                            amount:
                              minAmountValue
                              | currency: 'EUR'
                              | replace: ',':'.'
                          }
                  }}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div
          class="rounded-md bg-yellow-50 p-4 shadow"
          [hidden]="!hasLowerPrice()"
        >
          <div class="flex">
            <div class="flex-shrink-0">
              <svg
                class="h-5 w-5 text-yellow-400"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fill-rule="evenodd"
                  d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                  clip-rule="evenodd"
                />
              </svg>
            </div>
            <div class="ml-3" *ngIf="product && product.price">
              <h3 class="text-sm leading-5 font-medium text-yellow-800">
                {{
                  'store.cart.minimum-amount-required'
                    | translate
                      : {
                          amount:
                            product.price | currency: 'EUR' | replace: ',':'.'
                        }
                }}
              </h3>
              <div class="mt-2 text-sm leading-5 text-yellow-700">
                <p>
                  {{
                    'store.cart.minimum-amount-description'
                      | translate
                        : {
                            amount:
                              product.price | currency: 'EUR' | replace: ',':'.'
                          }
                  }}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div
          class="rounded-md bg-yellow-50 p-4 shadow"
          [hidden]="!hasMaximumAmount()"
        >
          <div class="flex">
            <div class="flex-shrink-0">
              <svg
                class="h-5 w-5 text-yellow-400"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fill-rule="evenodd"
                  d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                  clip-rule="evenodd"
                />
              </svg>
            </div>
            <div class="ml-3">
              <h3 class="text-sm leading-5 font-medium text-yellow-800">
                {{
                  'store.cart.maximum-amount-reached'
                    | translate
                      : {
                          amount:
                            maxAmountValue | currency: 'EUR' | replace: ',':'.'
                        }
                }}
              </h3>
              <div class="mt-2 text-sm leading-5 text-yellow-700">
                <p>
                  {{
                    'store.cart.maximum-amount-description'
                      | translate
                        : {
                            amount:
                              maxAmountValue
                              | currency: 'EUR'
                              | replace: ',':'.'
                          }
                  }}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./payment-direct.component.scss'],
})
export class PaymentDirectComponent implements OnInit, OnDestroy {
  constructor(
    private _route: ActivatedRoute,
    private _store: Store,
    private _spinner: NgxSpinnerService,
    private _paymentService: PaymentService,
    private _translate: TranslateService
  ) {}

  subLoading: Subscription
  subCurrent: Subscription
  priceForm: FormGroup
  product: Product
  minAmountValue = environment.constants.MIN_AMOUNT_VALUE
  maxAmountValue = environment.constants.MAX_AMOUNT_VALUE

  @Select(PaymentDirectState.currentProduct) current$: Observable<Product>
  @Select(PaymentDirectState.isLoading) isLoading$: Observable<boolean>

  ngOnInit(): void {
    this.priceForm = new FormGroup({
      price: new FormControl('', [Validators.required, validDigitNumber()]),
    })

    this.subCurrent = this.current$.subscribe((product) => {
      if (product) {
        this.product = product
        this.priceForm.get('price').setValue(product.price)
      }
    })

    this._route.paramMap.subscribe((params: ParamMap) => {
      if (this.haveToReset(params.get('id'))) {
        this._store.dispatch(new ResetPointOfSaleState())
        this._store.dispatch(new SetCurrentProduct(params.get('id')))
      }
    })
  }

  haveToReset(token: string): boolean {
    const currentProduct = this._store.selectSnapshot(
      PaymentDirectState.currentProduct
    )
    const products = this._store.selectSnapshot(ShoppingCartState.products)
    return (
      !currentProduct ||
      (currentProduct && currentProduct.token != token) ||
      products.length == 0
    )
  }

  ngAfterViewInit(): void {
    this.subLoading = this.isLoading$.subscribe((isLoading) => {
      isLoading ? this._spinner.show() : this._spinner.hide()
    })
  }

  ngOnDestroy(): void {
    if (this.subLoading) this.subLoading.unsubscribe()
    if (this.subCurrent) this.subCurrent.unsubscribe()
  }

  proceedToCheckout() {
    this.current$.pipe(take(1)).subscribe((product) => {
      this._paymentService.checkout(this._translate.currentLang, [
        { ...product, price: this.priceForm.get('price').value },
      ])
    })
  }

  hasLowerPrice(): boolean {
    if (this.product?.price < this.minAmountValue) return false
    return this.priceForm.get('price').value < this.product.price
  }

  hasEnoughAmount(): boolean {
    if (this.product.price >= this.minAmountValue) return true
    return this.priceForm.get('price').value >= this.minAmountValue
  }

  hasMaximumAmount(): boolean {
    return this.priceForm.get('price').value > this.maxAmountValue
  }
}

export function validDigitNumber(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const reg = new RegExp('^[0-9]+(\\.[0-9]{1,2})?$')
    const isValid = reg.test(control.value)
    return isValid ? null : { validDigitNumber: { value: control.value } }
  }
}
