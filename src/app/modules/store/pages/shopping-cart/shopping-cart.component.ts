import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { PointOfSalesState } from '../../states/point-of-sales.state';
import { PointOfSale } from '../../interfaces/point-of-sale';

@Component({
  selector: 'app-shopping-cart',
  template: `
    <div class="pb-32">
      <div class="w-full lg:max-w-md mx-auto flex flex-col pb-5">
        <div class="flex justify-between items-center -mb-2">
          <h1 class="text-2xl font-semibold text-cool-gray-700">
            {{ 'store.cart.title' | translate }}
          </h1>
          <a
            class="text-cool-gray-500 underline flex items-center"
            [routerLink]="['/store', (crtPointOfSale$ | async)?.token]"
          >
            <div
              class="mr-2 h-5 w-5"
              aria-label="arrow-circle-left"
              [inlineSVG]="
                '/assets/images/heroic-icons/outline/arrow-circle-left.svg'
              "
            ></div>
            {{ 'store.cart.back-store' | translate }}
          </a>
        </div>
        <store-shopping-cart-list></store-shopping-cart-list>
      </div>
    </div>
  `,
  styleUrls: ['./shopping-cart.component.scss'],
})
export class ShoppingCartComponent implements OnInit {
  @Select(PointOfSalesState.current) crtPointOfSale$: Observable<PointOfSale>;
  ngOnInit(): void {}
}
