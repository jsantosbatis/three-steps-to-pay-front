import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { Store, Select } from '@ngxs/store';
import {
  PointOfSalesState,
  PointOfSalesStateModel,
} from '../../states/point-of-sales.state';
import { Observable, fromEvent } from 'rxjs';
import { map, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { FindGeo, Search } from '../../actions/point-of-sales.action';

@Component({
  selector: 'app-point-of-sales',
  template: `
    <div class="w-full lg:max-w-md mx-auto">
      <h1
        class="text-2xl font-semibold pb-4 text-cool-gray-700"
        [translate]="'store.home.title'"
      ></h1>
      <div class="mb-2 w-full">
        <div class="relative rounded-md shadow-sm sm:max-w-md md:max-w-lg">
          <input
            id="point_of_sale_name"
            class="form-input block w-full sm:text-sm sm:leading-5"
            [attr.placeholder]="'store.home.placeholder-search' | translate"
            [(ngModel)]="inputValue"
            #inputSearch
          />
          <div
            class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none"
          >
            <svg
              *ngIf="isLoading$ | async"
              class="animate-spin h-5 w-5 text-cool-gray-400"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
            >
              <circle
                class="opacity-25"
                cx="12"
                cy="12"
                r="10"
                stroke="currentColor"
                stroke-width="4"
              ></circle>
              <path
                class="opacity-75"
                fill="currentColor"
                d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
              ></path>
            </svg>
            <svg
              *ngIf="!(isLoading$ | async)"
              class="h-5 w-5 text-cool-gray-400"
              fill="none"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
            </svg>
          </div>
        </div>
      </div>
      <div class="space-y-3">
        <div *ngIf="(pointOfSales$ | async).findGeo.length > 0">
          <h1 class="text-lg font-semibold py-2 text-cool-gray-700">
            {{ 'store.home.location' | translate }}
          </h1>
          <div class="grid grid-cols-1 gap-4">
            <app-point-of-sale-tile
              *ngFor="let data of (pointOfSales$ | async).findGeo"
              [data]="data"
            >
            </app-point-of-sale-tile>
          </div>
        </div>
        <h1 class="text-lg font-semibold py-2 text-cool-gray-700">
          {{ 'store.home.search-result' | translate }}
        </h1>
        <div
          *ngIf="(pointOfSales$ | async).find.length > 0"
          class="grid grid-cols-1 gap-4"
        >
          <app-point-of-sale-tile
            *ngFor="let data of (pointOfSales$ | async).find"
            [data]="data"
          >
          </app-point-of-sale-tile>
        </div>
        <div
          *ngIf="(pointOfSales$ | async).find.length === 0"
          class="text-center text-cool-gray-500 pt-5"
        >
          {{ 'store.home.no-results' | translate }}
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./point-of-sales.component.scss'],
})
export class PointOfSalesComponent implements OnInit, AfterViewInit {
  inputValue: string = '';
  @ViewChild('inputSearch', { static: false, read: ElementRef })
  inputSearch: ElementRef;

  @Select(PointOfSalesState)
  pointOfSales$: Observable<PointOfSalesStateModel>;

  @Select(PointOfSalesState.isLoading) isLoading$: Observable<boolean>;

  constructor(private store: Store) {}

  ngOnInit(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const longitude = position.coords.longitude;
        const latitude = position.coords.latitude;
        this.store.dispatch(new FindGeo({ lat: latitude, lng: longitude }));
      });
    }
  }

  ngAfterViewInit(): void {
    fromEvent(this.inputSearch.nativeElement, 'keyup')
      .pipe(
        map(() => this.inputValue),
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe((data) => this.store.dispatch(new Search(data)));
  }
}
