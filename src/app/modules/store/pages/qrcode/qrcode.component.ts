import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qrcode',
  template: `
    <store-container>
      <div children class="flex flex-col items-center">
        <img class="h-36 w-auto" src="assets/images/icon-app.png" alt="" />
        <div class="flex flex-col items-center space-y-6">
          <div
            class="h-36 w-36 text-cool-gray-600"
            aria-label="qrcode"
            [inlineSVG]="'/assets/images/heroic-icons/outline/qrcode.svg'"
          ></div>
          <p class="text-xl font-semibold text-cool-gray-700">
            {{ 'store.qrcode.message' | translate }}
          </p>
        </div>
      </div>
    </store-container>
  `,
  styleUrls: ['./qrcode.component.scss'],
})
export class QrCodeComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
