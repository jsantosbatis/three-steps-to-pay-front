export class SetCurrentProduct {
  static readonly type = '[PaymentDirect] Set current product';
  constructor(public token: string) {}
}

export class ResetPaymentDirectState {
  static readonly type = '[PaymentDirect] Reset state';
  constructor() {}
}
