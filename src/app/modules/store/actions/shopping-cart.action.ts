import { Product } from '../interfaces/product';

export class AddProduct {
  static readonly type = '[ShoppingCart] Add product';
  constructor(public product: Product) {}
}

export class AddProductFromToken {
  static readonly type = '[ShoppingCart] Add product by token';
  constructor(public token: string) {}
}

export class RemoveProduct {
  static readonly type = '[ShoppingCart] Remove product';
  constructor(public id: number) {}
}

export class ClearShopppingCart {
  static readonly type = '[ShoppingCart] Clear shopping cart';
  constructor() {}
}
