import { Location } from '../interfaces/location';

export class FindGeo {
  static readonly type =
    '[Point of sales API] Find point of sales based on location';
  constructor(public location: Location) {}
}

export class Search {
  static readonly type = '[Point of sales API] Search point of sales with name';
  constructor(public name: string) {}
}

export class SetCurrentPointOfSale {
  static readonly type = '[Catalog] Set current point of sale';
  constructor(public token: string) {}
}

export class ResetPointOfSaleState {
  static readonly type = '[Catalog] Reset state';
  constructor() {}
}
