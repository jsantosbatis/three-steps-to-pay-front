import { Component, Input, OnInit } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { Select, Store } from '@ngxs/store'
import { Observable } from 'rxjs'
import { take } from 'rxjs/operators'
import { environment } from 'src/environments/environment'
import { Product } from '../../interfaces/product'
import { ProductCart } from '../../interfaces/product-cart'
import { PaymentService } from '../../services/payment.service'
import { ShoppingCartState } from '../../states/shopping-cart.state'

@Component({
  selector: 'store-shopping-cart-list',
  template: `
    <div class="space-y-4">
      <div
        *ngIf="items.length === 0"
        class="flex flex-col justify-center items-center h-full space-y-3 py-4"
      >
        <div
          class="w-16 text-cool-gray-400"
          aria-label="empty"
          [inlineSVG]="
            '/assets/images/heroic-icons/outline/information-circle.svg'
          "
        ></div>
        <div class="text-center text-cool-gray-500">
          {{ 'store.cart.empty' | translate }}
        </div>
      </div>
      <app-product-cart
        *ngFor="let data of items | sortBy: ['product_category_id', 'name']"
        [data]="data"
        [holdLast]="holdLast"
      ></app-product-cart>
      <button
        class="button is-primary is-large shadow w-full"
        [disabled]="
          items.length === 0 || !hasEnoughAmount() || hasMaximumAmount()
        "
        (click)="proceedToCheckout()"
      >
        <span>{{ 'store.cart.button' | translate }}</span>
        <div
          class="mx-3 h-6 w-6"
          aria-label="cart"
          [inlineSVG]="'/assets/images/heroic-icons/outline/credit-card.svg'"
        ></div>

        <span>{{
          totalAmount$ | async | currency: 'EUR' | replace: ',':'.'
        }}</span>
      </button>
      <div
        class="rounded-md bg-yellow-50 p-4 shadow"
        [hidden]="hasEnoughAmount()"
      >
        <div class="flex">
          <div class="flex-shrink-0">
            <svg
              class="h-5 w-5 text-yellow-400"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                clip-rule="evenodd"
              />
            </svg>
          </div>
          <div class="ml-3">
            <h3 class="text-sm leading-5 font-medium text-yellow-800">
              {{
                'store.cart.minimum-amount-required'
                  | translate
                    : {
                        amount:
                          minAmountValue | currency: 'EUR' | replace: ',':'.'
                      }
              }}
            </h3>
            <div class="mt-2 text-sm leading-5 text-yellow-700">
              <p>
                {{
                  'store.cart.minimum-amount-description'
                    | translate
                      : {
                          amount:
                            minAmountValue | currency: 'EUR' | replace: ',':'.'
                        }
                }}
              </p>
            </div>
          </div>
        </div>
      </div>
      <div
        class="rounded-md bg-yellow-50 p-4 shadow"
        [hidden]="!hasMaximumAmount()"
      >
        <div class="flex">
          <div class="flex-shrink-0">
            <svg
              class="h-5 w-5 text-yellow-400"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z"
                clip-rule="evenodd"
              />
            </svg>
          </div>
          <div class="ml-3">
            <h3 class="text-sm leading-5 font-medium text-yellow-800">
              {{
                'store.cart.maximum-amount-reached'
                  | translate
                    : {
                        amount:
                          maxAmountValue | currency: 'EUR' | replace: ',':'.'
                      }
              }}
            </h3>
            <div class="mt-2 text-sm leading-5 text-yellow-700">
              <p>
                {{
                  'store.cart.maximum-amount-description'
                    | translate
                      : {
                          amount:
                            maxAmountValue | currency: 'EUR' | replace: ',':'.'
                        }
                }}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./shopping-cart-list.component.scss'],
})
export class ShoppingCartListComponent implements OnInit {
  @Input() holdLast: boolean = false
  constructor(
    private _store: Store,
    private _paymentService: PaymentService,
    private _translate: TranslateService
  ) {}
  items: ProductCart[]
  minAmountValue = environment.constants.MIN_AMOUNT_VALUE
  maxAmountValue = environment.constants.MAX_AMOUNT_VALUE

  @Select(ShoppingCartState.products) shoppingCart$: Observable<Product[]>
  @Select(ShoppingCartState.totalAmount) totalAmount$: Observable<number>

  ngOnInit(): void {
    this.shoppingCart$.subscribe((data) => {
      this.items = data.reduce((output, product) => {
        const index = output.findIndex((item) => item.id === product.id)
        if (index < 0) {
          const copyProduct = Object.assign({}, product) as ProductCart
          copyProduct.quantity = 1
          output.push(copyProduct)
        } else {
          output[index].quantity++
        }
        return output
      }, [])
    })
  }

  hasEnoughAmount(): boolean {
    return (
      this._store.selectSnapshot(ShoppingCartState.totalAmount) >=
      this.minAmountValue
    )
  }

  hasMaximumAmount(): boolean {
    return (
      this._store.selectSnapshot(ShoppingCartState.totalAmount) >=
      this.maxAmountValue
    )
  }

  proceedToCheckout() {
    this.shoppingCart$.pipe(take(1)).subscribe((products) => {
      this._paymentService.checkout(this._translate.currentLang, products)
    })
  }
}
