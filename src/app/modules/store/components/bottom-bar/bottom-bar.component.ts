import { Component, OnInit } from '@angular/core'
import { Select } from '@ngxs/store'
import { Observable } from 'rxjs'
import { ShoppingCartState } from '../../states/shopping-cart.state'

@Component({
  selector: 'app-bottom-bar',
  template: `
    <div
      class="fixed bottom-0 left-0 z-50 w-full h-32 bg-white flex flex-col justify-center items-center space-y-3 px-10"
      [ngStyle]="{
        'border-top-left-radius': '2rem',
        'border-top-right-radius': '2rem',
        'box-shadow': '0 -2px 2px 0 rgba(0, 0, 0, 0.1)'
      }"
    >
      <span class="font-semibold text-lg text-cool-gray-500"
        >Total :
        {{ shoppingCartAmount$ | async | currency: 'EUR' | replace: ',':'.' }}
      </span>
      <ng-content select="[buttonAction]"></ng-content>
    </div>
  `,
  styleUrls: ['./bottom-bar.component.scss'],
})
export class BottomBarComponent implements OnInit {
  constructor() {}
  @Select(ShoppingCartState.totalAmount) shoppingCartAmount$: Observable<number>
  ngOnInit(): void {}
}
