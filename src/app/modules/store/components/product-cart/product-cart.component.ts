import { Component, Input, OnInit } from '@angular/core'
import { ProductCategory } from '@enums/product-category'
import { Store } from '@ngxs/store'
import { AddProduct, RemoveProduct } from '../../actions/shopping-cart.action'
import { ProductCart } from '../../interfaces/product-cart'

@Component({
  selector: 'app-product-cart',
  template: `
    <div
      class="bg-white rounded-lg shadow py-4 px-5 flex items-center space-x-4 select-none my-4"
    >
      <div>
        <svg
          enable-background="new 0 0 511.213 511.213"
          viewBox="0 0 511.213 511.213"
          xmlns="http://www.w3.org/2000/svg"
          class="w-10 text-cool-gray-700-svg"
          *ngIf="data.product_category_id == productCategory.CANDLE"
        >
          <g>
            <path
              d="m70.06 218.765h78.343v30h-78.343z"
              transform="matrix(.707 -.707 .707 .707 -133.304 145.707)"
            />
            <path
              d="m94.232-.51h30v78.343h-30z"
              transform="matrix(.707 -.707 .707 .707 4.655 88.563)"
            />
            <path d="m25.283 121.213h80v30h-80z" />
            <path
              d="m386.982 194.593h30v78.343h-30z"
              transform="matrix(.707 -.707 .707 .707 -47.559 352.714)"
            />
            <path
              d="m362.81 23.662h78.343v30h-78.343z"
              transform="matrix(.707 -.707 .707 .707 90.4 295.568)"
            />
            <path d="m405.93 121.213h80v30h-80z" />
            <path d="m195.283 488.001 120-120v-64.575l-120 120z" />
            <path
              d="m240.283 136.213c0 8.271 6.729 15 15 15s15-6.729 15-15c0-12.44-7.646-26.34-15.001-36.589-7.351 10.245-14.999 24.147-14.999 36.589z"
            />
            <path d="m315.283 511.213v-100.786l-100.787 100.786z" />
            <path d="m195.283 381 111.787-111.787h-111.787z" />
            <path
              d="m255.283 0-10.606 10.607c-2.562 2.561-62.727 63.46-62.727 127.273 0 35.296 25.068 64.843 58.333 71.786v29.548h30v-29.548c33.265-6.943 58.333-36.489 58.333-71.786 0-63.813-60.165-124.712-62.727-127.273zm0 181.213c-24.813 0-45-20.187-45-45 0-35.197 30.873-67.086 34.394-70.606l10.606-10.607 10.606 10.607c3.521 3.52 34.394 35.409 34.394 70.606 0 24.813-20.187 45-45 45z"
            />
          </g>
        </svg>
        <svg
          fill="currentColor"
          viewBox="0 0 20 20"
          class="w-10 text-cool-gray-700"
          *ngIf="data.product_category_id == productCategory.GIFT"
        >
          <path
            fill-rule="evenodd"
            d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
            clip-rule="evenodd"
          ></path>
        </svg>
        <svg
          fill="currentColor"
          viewBox="0 0 20 20"
          class="w-10 text-cool-gray-700"
          *ngIf="data.product_category_id == productCategory.COLLECTION"
        >
          <path
            fill-rule="evenodd"
            d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.736 6.979C9.208 6.193 9.696 6 10 6c.304 0 .792.193 1.264.979a1 1 0 001.715-1.029C12.279 4.784 11.232 4 10 4s-2.279.784-2.979 1.95c-.285.475-.507 1-.67 1.55H6a1 1 0 000 2h.013a9.358 9.358 0 000 1H6a1 1 0 100 2h.351c.163.55.385 1.075.67 1.55C7.721 15.216 8.768 16 10 16s2.279-.784 2.979-1.95a1 1 0 10-1.715-1.029c-.472.786-.96.979-1.264.979-.304 0-.792-.193-1.264-.979a4.265 4.265 0 01-.264-.521H10a1 1 0 100-2H8.017a7.36 7.36 0 010-1H10a1 1 0 100-2H8.472c.08-.185.167-.36.264-.521z"
            clip-rule="evenodd"
          ></path>
        </svg>
        <svg
          fill="currentColor"
          viewBox="0 0 20 20"
          class="w-10 text-cool-gray-700"
          *ngIf="data.product_category_id == productCategory.ITEM"
        >
          <path
            fill-rule="evenodd"
            d="M17.707 9.293a1 1 0 010 1.414l-7 7a1 1 0 01-1.414 0l-7-7A.997.997 0 012 10V5a3 3 0 013-3h5c.256 0 .512.098.707.293l7 7zM5 6a1 1 0 100-2 1 1 0 000 2z"
            clip-rule="evenodd"
          ></path>
        </svg>
        <svg
          fill="currentColor"
          viewBox="0 0 20 20"
          class="w-10 text-cool-gray-700"
          *ngIf="data.product_category_id == productCategory.TICKET"
        >
          <path
            d="M2 6a2 2 0 012-2h12a2 2 0 012 2v2a2 2 0 100 4v2a2 2 0 01-2 2H4a2 2 0 01-2-2v-2a2 2 0 100-4V6z"
          ></path>
        </svg>
      </div>
      <div class="flex flex-col w-full">
        <div class="font-semibold text-cool-gray-600">
          {{ data.name }}
        </div>
        <div class="text-cool-gray-500">
          {{ data.price | currency: 'EUR' | replace: ',':'.' }}
        </div>
      </div>
      <div class="flex items-center space-x-3">
        <span
          (click)="removeProduct()"
          [hidden]="isQuantityEqualToOne()"
          class="md:cursor-pointer text-cool-gray-400"
          ><svg
            fill="none"
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width="2"
            viewBox="0 0 24 24"
            stroke="currentColor"
            class="w-5"
          >
            <path
              d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"
            ></path></svg></span
        ><span class="text-cool-gray-500 font-medium">{{ data.quantity }}</span
        ><span
          (click)="addProduct()"
          class="md:cursor-pointer text-white bg-primary-700 p-1 rounded-lg shadow-sm"
          ><svg
            fill="none"
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width="2"
            viewBox="0 0 24 24"
            stroke="currentColor"
            class="w-5"
          >
            <path d="M12 4v16m8-8H4"></path></svg
        ></span>
      </div>
    </div>
  `,
  styleUrls: ['./product-cart.component.scss'],
})
export class ProductCartComponent implements OnInit {
  @Input() data: ProductCart
  @Input() holdLast: boolean = false

  productCategory: typeof ProductCategory = ProductCategory

  constructor(private store: Store) {}

  ngOnInit(): void {}

  addProduct(): void {
    this.store.dispatch(new AddProduct(this.data))
  }

  removeProduct(): void {
    this.store.dispatch(new RemoveProduct(this.data.id))
  }

  isQuantityEqualToOne() {
    return this.holdLast && this.data.quantity === 1
  }
}
