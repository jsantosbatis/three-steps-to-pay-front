import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'store-container',
  template: `
    <div class="min-h-screen bg-gray-100 pt-20 pb-5 px-5 w-full">
      <ng-content select="[children]"></ng-content>
    </div>
  `,
  styleUrls: ['./container.component.scss'],
})
export class ContainerComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
