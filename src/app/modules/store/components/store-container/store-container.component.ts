import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-store-container',
  template: `
    <app-nav-bar></app-nav-bar>
    <store-container>
      <router-outlet children></router-outlet>
    </store-container>
  `,
  styleUrls: ['./store-container.component.scss'],
})
export class StoreContainerComponent implements OnInit {
  constructor(private route: Router) {}

  ngOnInit(): void {
    if (this.route.url === '/store') {
      this.route.navigate(['store', 'qrcode']);
    }
  }
}
