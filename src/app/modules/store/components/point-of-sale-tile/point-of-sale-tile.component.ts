import { Component, OnInit, Input } from '@angular/core';
import { PointOfSale } from '../../interfaces/point-of-sale';

@Component({
  selector: 'app-point-of-sale-tile',
  template: `
    <div
      class="bg-white shadow rounded-lg cursor-pointer"
      [routerLink]="[data.token]"
    >
      <div class="w-full flex items-center justify-between p-6 space-x-6">
        <div class="flex-1 truncate space-y-3">
          <div class="flex items-center">
            <h3
              class="text-cool-gray-500 leading-5 font-medium truncate text-md sm:text-lg"
            >
              {{ data.name }}
            </h3>
          </div>
          <p class="mt-1 text-sm text-cool-gray-500 truncate">
            {{ data.street }}<br />
            {{ data.postal_code }} {{ data.city }}
          </p>
        </div>
        <i class="fas fa-church fa-3x text-primary-500"></i>
      </div>
      <div class="border-t border-gray-200">
        <div class="-mt-px flex">
          <div class="w-0 flex-1 flex">
            <a
              class="relative -mr-px w-0 flex-1 inline-flex items-center justify-center py-4 text-sm leading-5 text-primary-700 font-medium border border-transparent rounded-bl-lg hover:text-gray-500 focus:outline-none focus:shadow-outline-blue focus:border-blue-300 focus:z-10 transition ease-in-out duration-150 cursor-pointer"
              [routerLink]="[data.token]"
            >
              <span class="mr-3">{{
                'store.product-tile.go-to' | translate
              }}</span>
              <svg
                fill="none"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                viewBox="0 0 24 24"
                stroke="currentColor"
                class="w-5 h-5"
              >
                <path
                  d="M13 9l3 3m0 0l-3 3m3-3H8m13 0a9 9 0 11-18 0 9 9 0 0118 0z"
                ></path>
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./point-of-sale-tile.component.scss'],
})
export class PointOfSaleTileComponent implements OnInit {
  @Input() data: PointOfSale;
  constructor() {}

  ngOnInit(): void {}
}
