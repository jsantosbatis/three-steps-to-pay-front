import { Component, Input, OnInit } from '@angular/core'
import { Store } from '@ngxs/store'
import { ToastrService } from 'ngx-toastr'
import { AddProduct } from '../../actions/shopping-cart.action'
import { Product } from '../../interfaces/product'

@Component({
  selector: 'app-product-tile',
  template: `
    <div class="bg-white shadow rounded-lg relative select-none">
      <div class="w-full px-4 py-4">
        <div class="flex flex-col space-y-2">
          <h3 class="text-sm text-cool-gray-500 leading-5 font-medium truncate">
            {{ data.name }}
          </h3>
          <p class="text-cool-gray-700 text-2xl leading-5">
            {{ data.price | currency: 'EUR' | replace: ',':'.' }}
          </p>
        </div>
      </div>
      <div class="pb-2 px-2">
        <button
          class="button is-primary w-full shadow relative"
          (click)="addToShoppingCart()"
        >
          <div class="invisible">empty</div>
          <div class="inline-flex absolute transition-all ease-in">
            <span class="mr-3">{{ 'store.catalog.add' | translate }}</span>
            <div
              class="w-5 h-5"
              aria-label="cart"
              [inlineSVG]="
                '/assets/images/heroic-icons/outline/shopping-cart.svg'
              "
            ></div>
          </div>
        </button>
      </div>
    </div>
  `,
  styleUrls: ['./product-tile.component.scss'],
})
export class ProductTileComponent implements OnInit {
  @Input('data') data: Product
  constructor(private store: Store, private _toastr: ToastrService) {}
  ngOnInit(): void {}

  addToShoppingCart(): void {
    this.store.dispatch(new AddProduct(this.data))
    this._toastr.show('store.toast-cart.message', 'store.toast-cart.title')
  }
}
