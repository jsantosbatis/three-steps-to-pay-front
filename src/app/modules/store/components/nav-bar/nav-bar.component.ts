import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { Product } from '../../interfaces/product';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ShoppingCartState } from '../../states/shopping-cart.state';
import { PointOfSale } from '../../interfaces/point-of-sale';
import { PointOfSalesState } from '../../states/point-of-sales.state';
import { HelperService } from '../../services/helper.service';

@Component({
  selector: 'app-nav-bar',
  template: `
    <nav class="bg-white border-b border-gray-200 fixed w-full z-40">
      <div class="pl-5 pr-6">
        <div
          class="flex justify-between items-center h-16 lg:max-w-md lg:mx-auto"
        >
          <div class="flex">
            <div class="flex-shrink-0 flex items-center">
              <a class="flex items-center cursor-pointer" (click)="goToHome()">
                <img
                  class="block lg:hidden h-12 w-auto"
                  src="assets/images/icon-app.png"
                  alt=""
                />
                <img
                  class="hidden lg:block h-14 w-auto"
                  src="assets/images/icon-app.png"
                  alt=""
                />
              </a>
            </div>
          </div>
          <div class="flex justify-around items-center">
            <span
              (click)="useLanguage('fr')"
              class="px-2 md:cursor-pointer"
              [class.text-indigo-600]="isLanguage('fr')"
            >
              FR
            </span>
            <span
              (click)="useLanguage('en')"
              class="px-2 md:cursor-pointer"
              [class.text-indigo-600]="isLanguage('en')"
            >
              EN
            </span>
            <a
              [routerLink]="['cart']"
              class="relative ml-4 cursor-default md:cursor-pointer"
              *ngIf="!isDirectMode"
            >
              <span class="inline-block relative">
                <svg
                  fill="none"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  class="w-7 h-7"
                >
                  <path
                    d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
                  ></path>
                </svg>
                <span
                  class="absolute left-5 bottom-0 h-5 w-5 rounded-full text-white 
                  shadow-solid bg-primary-700 text-xs flex justify-center items-center"
                  >{{ shoppingCartCount }}
                </span>
              </span>
            </a>
          </div>
        </div>
      </div>
    </nav>
  `,
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit {
  constructor(
    private translate: TranslateService,
    private _helper: HelperService
  ) {}
  shoppingCartCount: number = 0;
  isDirectMode = false;
  @Select(ShoppingCartState.products) shoppingCart$: Observable<Product[]>;
  @Select(PointOfSalesState.current) crtPointOfSale$: Observable<PointOfSale>;
  ngOnInit(): void {
    this.crtPointOfSale$.subscribe((data) => {
      this.isDirectMode = data ? false : true;
    });

    this.shoppingCart$.subscribe((data) => {
      this.shoppingCartCount = data.length;
    });
  }

  goToHome() {
    this._helper.goToHome();
  }

  useLanguage(language: string) {
    this.translate.use(language);
    localStorage.setItem('lang', language);
  }

  isLanguage(language: string) {
    return this.translate.currentLang === language;
  }
}
