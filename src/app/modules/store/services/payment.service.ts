import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Select } from '@ngxs/store'
import { NgxSpinnerService } from 'ngx-spinner'
import { Observable } from 'rxjs'
import { Product } from '../interfaces/product'
import { ShoppingCartState } from '../states/shopping-cart.state'
@Injectable({
  providedIn: 'root',
})
export class PaymentService {
  constructor(private _http: HttpClient, private _spinner: NgxSpinnerService) {}
  @Select(ShoppingCartState.products) shoppingCart$: Observable<Product[]>

  checkout(lang: string, products: Product[]) {
    this._spinner.show()
    this._http
      .post('api/app/transactions/checkout', {
        items: products,
        lang: lang,
      })
      .subscribe((data: any) => {
        this._spinner.hide()
        window.location.href = data.checkout_url
      })
  }
}
