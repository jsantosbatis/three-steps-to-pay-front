import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Location } from '../interfaces/location';
import { PointOfSale } from '../interfaces/point-of-sale';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class PointOfSaleService {
  constructor(private _http: HttpClient) {}

  findGeo(location: Location): Observable<PointOfSale[]> {
    return this._http.get<PointOfSale[]>('api/app/point_of_sales/find_geo', {
      params: {
        lat: location.lat.toString(),
        lng: location.lng.toString(),
      },
    });
  }

  find(): Observable<PointOfSale[]> {
    return this._http.get<PointOfSale[]>('api/app/point_of_sales/find');
  }

  search(name: string): Observable<PointOfSale[]> {
    return this._http.get<PointOfSale[]>('api/app/point_of_sales/search', {
      params: {
        name: name,
      },
    });
  }

  getByToken(token: string): Observable<PointOfSale> {
    return this._http.get<PointOfSale>(`api/app/point_of_sales/${token}`);
  }
}
