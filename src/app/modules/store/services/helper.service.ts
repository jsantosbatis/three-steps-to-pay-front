import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { PaymentDirectState } from '../states/payment-direct.state';
import { PointOfSalesState } from '../states/point-of-sales.state';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(private _store : Store, private _router : Router) { }


  goToHome() {
    const crtPointOfSale = this._store.selectSnapshot(PointOfSalesState.current);
    const crtProduct = this._store.selectSnapshot(PaymentDirectState.currentProduct);
    if(crtPointOfSale) {
      this._router.navigate(['/store', crtPointOfSale.token]);
    } else {
      this._router.navigate(['/store/direct', crtProduct.token]);
    } 
  }

  isDirectMode() {
    const crtProduct = this._store.selectSnapshot(PaymentDirectState.currentProduct);
    return crtProduct != null;
  }
}
