import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreContainerComponent } from './components/store-container/store-container.component';
import { HasNotCurrentPointOfSaleGuard } from './guards/has-not-current-point-of-sale.guard';
import { PaymentDirectComponent } from './pages/payment-direct/payment-direct.component';
import { PaymentStatusComponent } from './pages/payment-status/payment-status.component';
import { PointOfSaleCatalogComponent } from './pages/point-of-sale-catalog/point-of-sale-catalog.component';
import { QrCodeComponent } from './pages/qrcode/qrcode.component';
import { ShoppingCartComponent } from './pages/shopping-cart/shopping-cart.component';

const routes: Routes = [
  {
    path: 'qrcode',
    component: QrCodeComponent,
    canActivate: [HasNotCurrentPointOfSaleGuard],
  },
  {
    path: '',
    component: StoreContainerComponent,
    children: [
      { component: ShoppingCartComponent, path: 'cart' },
      {
        path: ':id',
        component: PointOfSaleCatalogComponent,
      },
      {
        path: 'direct/:id',
        component: PaymentDirectComponent,
      },
    ],
  },
  { path: 'payment/status', component: PaymentStatusComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
