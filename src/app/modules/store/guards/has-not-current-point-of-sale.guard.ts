import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { PointOfSalesState } from '../states/point-of-sales.state';
import { PaymentDirectState } from '../states/payment-direct.state';

@Injectable({
  providedIn: 'root',
})
export class HasNotCurrentPointOfSaleGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const pointOfSale = this.store.selectSnapshot(PointOfSalesState.current);
    const product = this.store.selectSnapshot(PaymentDirectState.currentProduct);
    if (pointOfSale) {
      return this.router.parseUrl(`store/${pointOfSale.token}`);
    } else if(product) {
      return this.router.parseUrl(`store/direct/${product.token}`);
    } else {
      return true;
    }
  }
}
