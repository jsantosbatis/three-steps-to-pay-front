import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { ShoppingCartState } from '../states/shopping-cart.state';
import { environment } from 'src/environments/environment';
import { HelperService } from '../services/helper.service';
import { PaymentDirectState } from '../states/payment-direct.state';

@Injectable({
  providedIn: 'root',
})
export class HasMinimumAmountGuard implements CanActivate {
  constructor(private _store: Store, private _router: Router, private _helper : HelperService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
      const crtProduct = this._store.selectSnapshot(PaymentDirectState.currentProduct);
    if (
      this._store.selectSnapshot(ShoppingCartState.totalAmount) >=
      environment.constants.MIN_AMOUNT_VALUE
    ) {
      return true;
    } else if(this._helper.isDirectMode()) {
      return this._router.parseUrl(`/store/direct/${crtProduct.token}`);
    } else {
      return this._router.parseUrl('/store/cart');
    }
  }
}
