import { CommonModule } from '@angular/common'
import { HttpClient } from '@angular/common/http'
import { NgModule } from '@angular/core'
import {
  TranslateLoader,
  TranslateModule,
  TranslateService,
} from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin'
import { NgxsModule } from '@ngxs/store'
import { CurrencyMaskInputMode, NgxCurrencyModule } from 'ngx-currency'
import { ToastrModule, ToastrService } from 'ngx-toastr'
import { environment } from 'src/environments/environment'
import { SharedModule } from '../shared/shared.module'
import { AppRoutingModule } from './app-routing.module'
import { BottomBarComponent } from './components/bottom-bar/bottom-bar.component'
import { ContainerComponent } from './components/container/container.component'
import { NavBarComponent } from './components/nav-bar/nav-bar.component'
import { PointOfSaleTileComponent } from './components/point-of-sale-tile/point-of-sale-tile.component'
import { ProductCartComponent } from './components/product-cart/product-cart.component'
import { ProductCategoryIconComponent } from './components/product-category-icon/product-category-icon.component'
import { ProductTileComponent } from './components/product-tile/product-tile.component'
import { ShoppingCartListComponent } from './components/shopping-cart-list/shopping-cart-list.component'
import { StoreContainerComponent } from './components/store-container/store-container.component'
import { ToastComponent } from './components/toast/toast.component'
import { HasMinimumAmountGuard } from './guards/has-minimum-amount.guard'
import { HasNotCurrentPointOfSaleGuard } from './guards/has-not-current-point-of-sale.guard'
import { PaymentDirectComponent } from './pages/payment-direct/payment-direct.component'
import { PaymentStatusComponent } from './pages/payment-status/payment-status.component'
import { PointOfSaleCatalogComponent } from './pages/point-of-sale-catalog/point-of-sale-catalog.component'
import { PointOfSalesComponent } from './pages/point-of-sales/point-of-sales.component'
import { QrCodeComponent } from './pages/qrcode/qrcode.component'
import { ShoppingCartComponent } from './pages/shopping-cart/shopping-cart.component'
import { HelperService } from './services/helper.service'
import { PaymentDirectState } from './states/payment-direct.state'
import { PointOfSalesState } from './states/point-of-sales.state'
import { ShoppingCartState } from './states/shopping-cart.state'

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http)
}

export const customCurrencyMaskConfig = {
  align: 'left',
  allowNegative: false,
  allowZero: true,
  decimal: ',',
  precision: 2,
  prefix: '',
  suffix: ' €',
  thousands: '',
  nullable: true,
  min: null,
  max: null,
  inputMode: CurrencyMaskInputMode.FINANCIAL,
}

@NgModule({
  declarations: [
    StoreContainerComponent,
    ShoppingCartComponent,
    ProductCartComponent,
    PointOfSalesComponent,
    PointOfSaleTileComponent,
    PointOfSaleCatalogComponent,
    ProductTileComponent,
    NavBarComponent,
    PaymentStatusComponent,
    BottomBarComponent,
    QrCodeComponent,
    PaymentDirectComponent,
    ContainerComponent,
    ShoppingCartListComponent,
    ToastComponent,
    ProductCategoryIconComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    NgxsModule.forRoot(
      [ShoppingCartState, PointOfSalesState, PaymentDirectState],
      {
        developmentMode: !environment.production,
      }
    ),
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
    NgxsStoragePluginModule.forRoot({
      key: ['store_shoppingCart', 'store_pointOfSales', 'store_paymentDirect'],
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    ToastrModule.forRoot({
      toastComponent: ToastComponent,
      timeOut: 4000,
      easeTime: 250,
      resetTimeoutOnDuplicate: true,
      preventDuplicates: true,
      autoDismiss: true,
      maxOpened: 1,
    }), // ToastrModule added
  ],
  bootstrap: [PaymentDirectComponent],
  providers: [
    HasMinimumAmountGuard,
    HasNotCurrentPointOfSaleGuard,
    HelperService,
    ToastrService,
  ],
})
export class StoreModule {
  constructor(private translate: TranslateService) {
    this.translate.setDefaultLang('fr')
    if (localStorage.getItem('lang')) {
      this.translate.use(localStorage.getItem('lang'))
    } else {
      this.translate.use('fr')
    }
  }
}
