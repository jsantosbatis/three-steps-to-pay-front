import { Injectable } from '@angular/core'
import { Action, Selector, State, StateContext, Store } from '@ngxs/store'
import { of } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'
import {
  ResetPaymentDirectState,
  SetCurrentProduct,
} from '../actions/payment-direct.action'
import { Product } from '../interfaces/product'
import { ProductService } from '../services/product.service'

export interface PaymentDirectStateModel {
  currentProduct: Product
  isLoading: boolean
}
@State<PaymentDirectStateModel>({
  name: 'store_paymentDirect',
  defaults: {
    currentProduct: null,
    isLoading: false,
  },
})
@Injectable()
export class PaymentDirectState {
  constructor(private productService: ProductService, private store: Store) {}

  @Selector()
  static currentProduct(state: PaymentDirectStateModel) {
    return state.currentProduct
  }

  @Selector()
  static isLoading(state: PaymentDirectStateModel) {
    return state.isLoading
  }

  @Action(SetCurrentProduct)
  SetCurrentProduct(
    ctx: StateContext<PaymentDirectStateModel>,
    action: SetCurrentProduct
  ) {
    ctx.patchState({ isLoading: true })
    return this.productService.getFromToken(action.token).pipe(
      tap((data) => {
        ctx.patchState({ currentProduct: data, isLoading: false })
      }),
      catchError(() => {
        return of<Product>(null)
      })
    )
  }

  @Action(ResetPaymentDirectState)
  ResetPaymentDirectState(
    ctx: StateContext<PaymentDirectStateModel>,
    action: ResetPaymentDirectState
  ) {
    ctx.patchState({ currentProduct: null, isLoading: false })
  }
}
