import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Product } from '../interfaces/product';
import {
  AddProduct,
  RemoveProduct,
  ClearShopppingCart,
} from '../actions/shopping-cart.action';
import { sumBy } from 'lodash-es';
export interface ShoppingCartStateModel {
  products: Product[];
  total: number;
}
@State<ShoppingCartStateModel>({
  name: 'store_shoppingCart',
  defaults: {
    products: [],
    total: 0,
  },
})
@Injectable()
export class ShoppingCartState {
  @Selector()
  static products(state: ShoppingCartStateModel) {
    return state.products;
  }

  @Selector()
  static totalAmount(state: ShoppingCartStateModel): number {
    return state.total;
  }

  @Action(AddProduct)
  AddProduct(ctx: StateContext<ShoppingCartStateModel>, action: AddProduct) {
    const state = ctx.getState();
    const newProducts = [...state.products, action.product];
    ctx.patchState({
      products: newProducts,
      total: sumBy(newProducts, 'price'),
    });
  }

  @Action(RemoveProduct)
  RemoveProduct(
    ctx: StateContext<ShoppingCartStateModel>,
    action: RemoveProduct
  ) {
    const products = ctx.getState().products;
    const newProducts = [...products];
    const index = newProducts.findIndex((item) => item.id === action.id);
    newProducts.splice(index, 1);
    ctx.patchState({
      products: [...newProducts],
      total: sumBy(newProducts, 'price'),
    });
  }

  @Action(ClearShopppingCart)
  ClearShopppingCart(
    ctx: StateContext<ShoppingCartStateModel>,
    action: ClearShopppingCart
  ) {
    ctx.patchState({ total: 0, products: [] });
  }
}
