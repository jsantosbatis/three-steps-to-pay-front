import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { PointOfSale } from '../interfaces/point-of-sale';
import {
  FindGeo,
  ResetPointOfSaleState,
  Search,
} from '../actions/point-of-sales.action';
import { PointOfSaleService } from '../services/point-of-sale.service';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { SetCurrentPointOfSale } from '../actions/point-of-sales.action';
import { ClearShopppingCart } from '../actions/shopping-cart.action';
import { unionBy } from 'lodash-es';
import * as moment from 'moment';

export interface PointOfSalesStateModel {
  findGeo: PointOfSale[];
  find: PointOfSale[];
  items: PointOfSale[];
  current: PointOfSale;
  isLoading: boolean;
  lastUpdate: moment.Moment;
}
@State<PointOfSalesStateModel>({
  name: 'store_pointOfSales',
  defaults: {
    findGeo: [],
    find: [],
    items: [],
    current: null,
    isLoading: false,
    lastUpdate: null,
  },
})
@Injectable()
export class PointOfSalesState {
  constructor(private pointOfSaleService: PointOfSaleService) {}

  @Selector()
  static current(state: PointOfSalesStateModel) {
    return state.current;
  }

  @Selector()
  static products(state: PointOfSalesStateModel) {
    return (category: number) => {
      if (state.current) {
        return state.current.products?.filter(
          (item) => item.product_category_id == category
        );
      } else {
        return [];
      }
    };
  }

  @Selector()
  static isLoading(state: PointOfSalesStateModel) {
    return state.isLoading;
  }

  @Action(FindGeo)
  findGeo(ctx: StateContext<PointOfSalesStateModel>, action: FindGeo) {
    return this.pointOfSaleService.findGeo(action.location).pipe(
      tap((data) => {
        ctx.patchState({ findGeo: data });
      })
    );
  }

  @Action(Search)
  search(ctx: StateContext<PointOfSalesStateModel>, action: Search) {
    ctx.patchState({ isLoading: true });
    return this.pointOfSaleService.search(action.name).pipe(
      tap((data) => {
        ctx.patchState({ find: data, isLoading: false });
      }),
      catchError(() => {
        ctx.patchState({ isLoading: false });
        return of<PointOfSale[]>([]);
      })
    );
  }

  @Action(SetCurrentPointOfSale)
  SetCurrentPointOfSale(
    ctx: StateContext<PointOfSalesStateModel>,
    action: SetCurrentPointOfSale
  ) {
    ctx.patchState({ isLoading: true });
    if (
      !ctx.getState().lastUpdate ||
      this.haveToRefresh(ctx.getState().lastUpdate)
    ) {
      ctx.patchState({
        items: [],
        lastUpdate: moment(),
      });
    }
    const state = ctx.getState();
    if (state.current && state.current.token != action.token) {
      ctx.dispatch(new ClearShopppingCart());
    }

    const find = state.items.filter((item) => item.token === action.token);
    if (find.length === 1) {
      ctx.patchState({
        current: find[0],
        isLoading: false,
      });
    } else {
      return this.pointOfSaleService.getByToken(action.token).pipe(
        tap((data) => {
          ctx.patchState({
            current: data,
            items: unionBy([...state.items, data], 'id'),
            isLoading: false,
          });
        }),
        catchError(() => {
          ctx.patchState({ isLoading: false });
          return of<PointOfSale[]>([]);
        })
      );
    }
  }

  @Action(ResetPointOfSaleState)
  ResetPointOfSaleState(
    ctx: StateContext<PointOfSalesStateModel>,
    action: SetCurrentPointOfSale
  ) {
    ctx.patchState({
      findGeo: [],
      find: [],
      items: [],
      current: null,
      isLoading: false,
      lastUpdate: null,
    });
  }

  haveToRefresh(lastUpdate) {
    return moment.duration(moment().diff(lastUpdate)).asMinutes() > 15;
  }
}
