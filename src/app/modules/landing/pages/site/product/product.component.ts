import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product',
  template: `
    <div class="relative bg-indigo-600 z-10">
      <div class="relative py-8 px-6 sm:py-16 max-w-7xl mx-auto">
        <div
          id="product"
          class="flex flex-col sm:flex-row sm:items-center sm:justify-between "
        >
          <div class="text-3xl sm:text-5xl text-indigo-200">
            ChurchPay est un service de paiement pour l’Eglise Catholique
          </div>
          <img
            src="assets/images/church.png"
            class="sm:h-64
      h-auto"
          />
        </div>
        <div
          class="w-full grid grid-flow-col grid-cols-3 grid-rows-2 gap-4 mt-10"
          [ngStyle]="{
            'justify-items': 'center'
          }"
        >
          <div
            class="bg-indigo-100 p-3 lg:p-5 flex items-center h-15 w-15 lg:h-32 lg:w-32 rounded-full"
          >
            <div
              class="h-auto w-15 flex-1"
              aria-label="Payconiq"
              [inlineSVG]="'/assets/images/payment-brand/payconiq.svg'"
            ></div>
          </div>
          <div
            class="bg-indigo-100 p-3 lg:p-5 flex items-center h-15 w-15 lg:h-32 lg:w-32 rounded-full"
          >
            <div
              class="h-auto w-15 flex-1"
              aria-label="Bancontact"
              [inlineSVG]="'/assets/images/payment-brand/bancontact.svg'"
            ></div>
          </div>
          <div
            class="bg-indigo-100 p-3 lg:p-5 flex items-center h-15 w-15 lg:h-32 lg:w-32 rounded-full"
          >
            <div
              class="h-auto w-15 flex-1"
              aria-label="Mastercard"
              [inlineSVG]="'/assets/images/payment-brand/mastercard.svg'"
            ></div>
          </div>
          <div
            class="bg-indigo-100 p-3 lg:p-5 flex items-center h-15 w-15 lg:h-32 lg:w-32 rounded-full"
          >
            <div
              class="h-auto w-15 flex-1"
              aria-label="Visa"
              [inlineSVG]="'/assets/images/payment-brand/visa.svg'"
            ></div>
          </div>
          <div
            class="bg-indigo-100 p-3 lg:p-5 flex items-center h-15 w-15 lg:h-32 lg:w-32 rounded-full"
          >
            <div
              class="h-auto w-15 flex-1"
              aria-label="Paypal"
              [inlineSVG]="'/assets/images/payment-brand/paypal.svg'"
            ></div>
          </div>
          <div
            class="bg-indigo-100 p-3 lg:p-5 flex items-center h-15 w-15 lg:h-32 lg:w-32 rounded-full"
          >
            <div
              class="h-auto w-15 flex-1"
              aria-label="Sofort"
              [inlineSVG]="'/assets/images/payment-brand/sofort.svg'"
            ></div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
