import { HttpBackend, HttpClient } from '@angular/common/http'
import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'app-contact',
  template: `
    <div id="contact" class="relative bg-white">
      <div class="absolute inset-0">
        <div class="absolute inset-y-0 left-0 w-1/2 bg-indigo-100"></div>
      </div>
      <div class="relative max-w-7xl mx-auto lg:grid lg:grid-cols-5">
        <div
          class="bg-indigo-100 py-16 px-4 sm:px-6 lg:col-span-2 lg:px-8 lg:py-24 xl:pr-12"
        >
          <div class="max-w-lg mx-auto">
            <h2
              class="text-2xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-3xl sm:leading-9"
            >
              Contactez-nous
            </h2>
            <p class="mt-3 text-lg leading-6 text-gray-500">
              Curieux/se? Intéressé(e)? <br />
              Souhaitez vous proposer ce service au sein de votre église ?
              <br />
            </p>
            <dl class="mt-8 text-base leading-6 text-gray-500">
              <div class="mt-6">
                <dt class="sr-only">Phone number</dt>
                <dd class="flex">
                  <svg
                    class="flex-shrink-0 h-6 w-6 text-gray-400"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z"
                    />
                  </svg>
                  <span class="ml-3">+32 477 59 13 05</span>
                </dd>
              </div>
              <div class="mt-3">
                <dt class="sr-only">Email</dt>
                <dd class="flex">
                  <svg
                    class="flex-shrink-0 h-6 w-6 text-gray-400"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"
                    />
                  </svg>
                  <span class="ml-3">support@churchpay.be</span>
                </dd>
              </div>
            </dl>
          </div>
        </div>
        <div
          class="bg-white py-16 px-4 sm:px-6 lg:col-span-3 lg:py-24 lg:px-8 xl:pl-12"
        >
          <div class="max-w-lg mx-auto lg:max-w-none">
            <form
              class="grid grid-cols-1 gap-y-6"
              name="contact-form"
              [formGroup]="contactForm"
              (ngSubmit)="onSubmit()"
            >
              <div>
                <label htmlFor="full_name" class="sr-only"> Full name </label>
                <input
                  id="full_name"
                  name="name"
                  type="text"
                  class="block w-full shadow-sm py-3 px-4 placeholder-gray-500 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md"
                  placeholder="Nom"
                  formControlName="name"
                />
              </div>
              <div>
                <label htmlFor="email" class="sr-only"> Email </label>
                <input
                  id="email"
                  type="email"
                  name="email"
                  type="email"
                  class="block w-full shadow-sm py-3 px-4 placeholder-gray-500 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md"
                  placeholder="Email"
                  formControlName="email"
                />
              </div>
              <div>
                <label htmlFor="phone" class="sr-only"> Phone </label>
                <input
                  id="phone"
                  name="phone"
                  type="text"
                  class="block w-full shadow-sm py-3 px-4 placeholder-gray-500 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md"
                  placeholder="Téléphone/Gsm"
                  formControlName="phone"
                />
              </div>
              <div>
                <label htmlFor="message" class="sr-only"> Message </label>
                <textarea
                  id="message"
                  rows="4"
                  name="message"
                  class="block w-full shadow-sm py-3 px-4 placeholder-gray-500 focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md"
                  placeholder="Message"
                  formControlName="message"
                ></textarea>
              </div>
              <div class="flex sm:flex-row flex-col sm:items-center">
                <div>
                  <span class="inline-flex rounded-md shadow-sm">
                    <button
                      type="submit"
                      class="inline-flex justify-center py-3 px-6 border border-transparent text-base leading-6 font-medium rounded-md text-white focus:outline-none transition duration-150 ease-in-out"
                      [ngClass]="{
                        'bg-indigo-600 hover:bg-indigo-500 focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700':
                          contactForm.valid,
                        'bg-indigo-300 cursor-not-allowed': !contactForm.valid
                      }"
                    >
                      <span [hidden]="isRequestPending">Envoyer</span>
                      <span [hidden]="!isRequestPending">Envoi en cours</span>
                    </button>
                  </span>
                </div>
                <div
                  class="rounded-md bg-red-50 p-4 sm:ml-5 sm:mt-0 mt-5"
                  [hidden]="initForm || succesfullSubmited || isRequestPending"
                >
                  <div class="flex">
                    <div class="flex-shrink-0">
                      <svg
                        class="h-5 w-5 text-red-400"
                        view-box="0 0 20 20"
                        fill="currentColor"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z"
                          clip-rule="evenodd"
                        />
                      </svg>
                    </div>
                    <div class="ml-3">
                      <h3 class="text-sm leading-5 font-medium text-red-800">
                        Veuillez compléter correctement le formulaire
                      </h3>
                    </div>
                  </div>
                </div>
                <div
                  class="rounded-md bg-green-50 p-4 sm:ml-5 sm:mt-0 mt-5"
                  [hidden]="initForm || !succesfullSubmited"
                >
                  <div class="flex">
                    <div class="flex-shrink-0">
                      <svg
                        class="h-5 w-5 text-green-400"
                        view-box="0 0 20 20"
                        fill="currentColor"
                      >
                        <path
                          fill-rule="evenodd"
                          d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                          clip-rule="evenodd"
                        />
                      </svg>
                    </div>
                    <div class="ml-3">
                      <h3 class="text-sm leading-5 font-medium text-green-800">
                        {{ statusForm }}
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  constructor(private handler: HttpBackend) {
    this.http = new HttpClient(handler)
  }
  http: HttpClient
  initForm: boolean = true
  isRequestPending = false
  succesfullSubmited: boolean = false
  contactForm: FormGroup
  statusForm: string = ''
  ngOnInit(): void {
    this.contactForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      message: new FormControl('', [Validators.required]),
    })
  }

  onSubmit() {
    this.initForm = false
    if (this.contactForm.valid) {
      this.isRequestPending = true
      this.http.post('/api/contact_form', this.contactForm.value).subscribe(
        (data) => {
          this.isRequestPending = false
          this.succesfullSubmited = true
          this.statusForm = 'Votre demande de contact a bien été transmise'
          this.contactForm.reset({})
        },
        (error) => {
          this.statusForm = `Votre demande de contact n'a pas pu être transmise. Veuillez réessayer plus tard`
        }
      )
    } else {
      this.succesfullSubmited = false
      this.statusForm = 'Veuillez compléter correctement le formulaire'
    }
  }
}
