import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-step-info',
  template: `
    <div id="howto" class="bg-indigo-600">
      <div id="howto" class="px-8 py-12 sm:py-16 sm:px-0 max-w-7xl mx-auto">
        <p class="text-center text-3xl sm:text-5xl text-indigo-200">
          Comment ça fonctionne ?
        </p>
        <div class="my-12">
          <div
            class="flex flex-col sm:flex-row justify-around space-y-10 sm:space-y-0"
          >
            <div class="flex flex-col items-center md:w-72">
              <img src="assets/images/step_access_to_store.png" />
              <p
                class="text-center text-4xl font-bold text-yellow-300 uppercase tracking-wide"
              >
                Accèdez à la boutique
              </p>
            </div>
            <div class="flex flex-col items-center md:w-72">
              <img src="assets/images/step_buy_candle.png" />
              <p
                class="text-center text-4xl font-bold text-yellow-300 uppercase tracking-wide"
              >
                Achetez une bougie
              </p>
            </div>
          </div>
          <div
            class="flex flex-col sm:flex-row justify-around mt-12 space-y-10 sm:space-y-0"
          >
            <div class="flex flex-col items-center md:w-72">
              <img src="assets/images/step_candle.png" />
              <p
                class="text-center text-4xl font-bold text-yellow-300 uppercase tracking-wide"
              >
                Allumez la bougie
              </p>
            </div>
            <div class="flex flex-col items-center md:w-72">
              <img src="assets/images/step_mary.png" />
              <p
                class="text-center text-3xl sm:text-4xl font-bold text-yellow-300 uppercase tracking-wide"
              >
                Faites vos prières
              </p>
            </div>
          </div>
        </div>
        <p class="text-center text-3xl sm:text-5xl text-indigo-200">
          C’est simple, non ?
        </p>
      </div>
    </div>
  `,
  styleUrls: ['./step-info.component.scss'],
})
export class StepInfoComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
