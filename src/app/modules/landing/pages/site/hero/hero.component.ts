import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero',
  template: `
    <div class="bg-indigo-100" id="hero">
      <div
        class="w-full flex flex-col sm:flex-row max-w-7xl mx-auto sm:pt-12 pt-20 sm:items-center -mb-8 sm:-mb-16"
      >
        <div class="text-4xl sm:text-6xl font-light text-gray-600 text-center">
          Je paye ma bougie avec
          <span class="font-semibold">ChurchPay</span>!
          <br />
          Et toi ?
        </div>
        <img src="assets/images/simple_front.png" class="block sm:hidden" />
        <img
          src="assets/images/simple_front.png"
          class="hidden sm:block"
          [ngStyle]="{ width: '40rem' }"
        />
      </div>
    </div>
  `,
  styleUrls: ['./hero.component.scss'],
})
export class HeroComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
