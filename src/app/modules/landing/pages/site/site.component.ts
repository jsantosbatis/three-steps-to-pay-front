import {
  Component,
  OnInit,
  AfterViewInit,
  ElementRef,
  ViewChild,
  HostListener,
} from '@angular/core';

@Component({
  selector: 'app-site',
  template: `
    <div class="landing font-quicksand">
      <app-navbar [activeItem]="activeItem"></app-navbar>
      <app-hero #hero></app-hero>
      <app-product #product></app-product>
      <app-service #service></app-service>
      <app-step-info #stepInfo></app-step-info>
      <app-contact #contact></app-contact>
    </div>
  `,
  styleUrls: ['./site.component.scss'],
})
export class SiteComponent implements OnInit, AfterViewInit {
  @ViewChild('hero', { static: false, read: ElementRef })
  heroSection: ElementRef;
  @ViewChild('product', { static: false, read: ElementRef })
  productSection: ElementRef;
  @ViewChild('service', { static: false, read: ElementRef })
  serviceSection: ElementRef;
  @ViewChild('stepInfo', { static: false, read: ElementRef })
  stepInfoSection: ElementRef;
  @ViewChild('contact', { static: false, read: ElementRef })
  contactSection: ElementRef;

  heroOffset: number;
  productOffset: number;
  serviceOffset: number;
  stepInfoOffset: number;
  contactOffset: number;

  activeItem: string = 'hero';
  constructor() {}
  @HostListener('window:scroll', ['$event'])
  checkOffsetTop() {
    if (
      window.pageYOffset >= this.heroOffset &&
      window.pageYOffset < this.productOffset
    ) {
      this.activeItem = 'hero';
    } else if (
      window.pageYOffset >= this.productOffset &&
      window.pageYOffset < this.serviceOffset
    ) {
      this.activeItem = 'product';
    } else if (
      window.pageYOffset >= this.serviceOffset &&
      window.pageYOffset < this.stepInfoOffset
    ) {
      this.activeItem = 'service';
    } else if (
      window.pageYOffset >= this.stepInfoOffset &&
      window.pageYOffset < this.contactOffset
    ) {
      this.activeItem = 'stepInfo';
    } else if (window.pageYOffset >= this.contactOffset) {
      this.activeItem = 'contact';
    } else {
      this.activeItem = 'hero';
    }
  }
  ngOnInit(): void {}

  ngAfterViewInit() {
    this.heroOffset = this.heroSection.nativeElement.offsetTop;
    this.productOffset = this.productSection.nativeElement.offsetTop;
    this.serviceOffset = this.serviceSection.nativeElement.offsetTop;
    this.stepInfoOffset = this.stepInfoSection.nativeElement.offsetTop;
    this.contactOffset = this.contactSection.nativeElement.offsetTop;
  }
}
