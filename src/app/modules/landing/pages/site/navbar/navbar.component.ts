import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-navbar',
  template: `
    <div class="mx-auto px-4 sm:px-6 lg:px-8 fixed w-full z-50 bg-white">
      <div class="flex justify-between h-16">
        <div class="flex justify-between w-full">
          <div class="flex-shrink-0 flex items-center">
            <div class="uppercase tracking-widest font-semibold">
              <span class="text-indigo-400">church</span>
              <span class="text-indigo-700">pay</span>
            </div>
          </div>
          <div class="hidden sm:ml-6 sm:flex">
            <a
              class="cursor-pointer inline-flex items-center px-1 pt-1 text-lg font-medium leading-5 text-gray-400 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out"
              [ngxScrollTo]="'#hero'"
              [ngClass]="{ 'text-gray-700': activeItem === 'hero' }"
              >Accueil</a
            >
            <a
              class="cursor-pointer ml-8 inline-flex items-center px-1 pt-1 text-lg font-medium leading-5 text-gray-400 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out"
              [ngxScrollTo]="'#product'"
              [ngClass]="{ 'text-gray-700': activeItem === 'product' }"
              >Produit</a
            >
            <a
              class="cursor-pointer ml-8 inline-flex items-center px-1 pt-1 text-lg font-medium leading-5 text-gray-400 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out"
              [ngxScrollTo]="'#services'"
              [ngClass]="{ 'text-gray-700': activeItem === 'service' }"
              >Services</a
            >
            <a
              class="cursor-pointer ml-8 inline-flex items-center px-1 pt-1 text-lg font-medium leading-5 text-gray-400 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out"
              [ngxScrollTo]="'#howto'"
              [ngClass]="{ 'text-gray-700': activeItem === 'stepInfo' }"
              >Mode d'emploi</a
            >
            <a
              class="cursor-pointer ml-8 inline-flex items-center px-1 pt-1 text-lg font-medium leading-5 text-gray-400 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out"
              [ngxScrollTo]="'#contact'"
              [ngClass]="{ 'text-gray-700': activeItem === 'contact' }"
              >Contact</a
            >
          </div>
        </div>
        <div class="-mr-2 flex items-center sm:hidden">
          <button
            class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-700 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out"
            aria-label="Main menu"
            aria-expanded="false"
            (click)="toggleMenu()"
          >
            <svg
              class="block h-6 w-6"
              fill="none"
              view-box="0 0 24 24"
              stroke="currentColor"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M4 6h16M4 12h16M4 18h16"
              />
            </svg>

            <svg
              class="hidden h-6 w-6"
              fill="none"
              view-box="0 0 24 24"
              stroke="currentColor"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M6 18L18 6M6 6l12 12"
              />
            </svg>
          </button>
        </div>
      </div>
      <div [ngClass]="{ hidden: !isMenuVisible }">
        <div class="pt-2 pb-3">
          <a
            class="block pl-3 pr-4 py-2 text-base font-medium text-gray-400"
            [ngxScrollTo]="'#hero'"
            [ngClass]="{ 'text-gray-700': activeItem === 'hero' }"
          >
            Accueil
          </a>
          <a
            class="mt-1 block pl-3 pr-4 py-2 text-base font-medium text-gray-400"
            [ngxScrollTo]="'#product'"
            [ngClass]="{ 'text-gray-700': activeItem === 'product' }"
          >
            Produit
          </a>
          <a
            class="mt-1 block pl-3 pr-4 py-2 text-base font-medium text-gray-400"
            [ngxScrollTo]="'#services'"
            [ngClass]="{ 'text-gray-700': activeItem === 'service' }"
          >
            Services
          </a>
          <a
            class="mt-1 block pl-3 pr-4 py-2 text-base font-medium text-gray-400"
            [ngxScrollTo]="'#howto'"
            [ngClass]="{ 'text-gray-700': activeItem === 'stepInfo' }"
          >
            Mode d'emploi
          </a>
          <a
            class="mt-1 block pl-3 pr-4 py-2 text-base font-medium text-gray-400"
            [ngxScrollTo]="'#contact'"
            [ngClass]="{ 'text-gray-700': activeItem === 'contact' }"
          >
            Contact
          </a>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  isMenuVisible: boolean = false;

  @Input('activeItem') activeItem: string;
  constructor() {}

  ngOnInit(): void {}

  toggleMenu() {
    this.isMenuVisible = !this.isMenuVisible;
  }
}
