import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-service',
  template: `
    <div id="services" class="bg-indigo-100">
      <div class="px-14 py-12 sm:py-16 max-w-7xl mx-auto">
        <p class="text-center text-3xl sm:text-5xl text-gray-600">
          Payer avec son smartphone
        </p>
        <div class="flex flex-col my-10">
          <div>
            <div
              class="flex sm:flex-row flex-col justify-start sm:items-center sm:my-0 my-6"
            >
              <img
                src="assets/images/pray_with_candle.png"
                class="sm:h-64 h-auto"
              />
              <div class="sm:ml-12 sm:text-4xl text-xl">
                <p class="text-gray-700 font-bold">Bougie</p>
                <p>Rendez vos moments de prières uniques</p>
              </div>
            </div>
          </div>
          <div>
            <div
              class="flex sm:flex-row-reverse flex-col justify-start sm:items-center sm:my-0 my-6"
            >
              <img src="assets/images/donation.png" class="sm:h-64 h-auto" />
              <div class="sm:mr-12 sm:text-4xl text-xl">
                <p class="text-gray-700 font-bold">Collecte/Don</p>
                <p>Soutenez votre église</p>
              </div>
            </div>
          </div>
        </div>
        <div
          class="flex sm:flex-row flex-col justify-start sm:items-center sm:my-0 my-6"
        >
          <img
            src="assets/images/band.png"
            class="sm:h-72
        h-auto"
          />
          <div class="sm:ml-12 sm:text-4xl text-xl sm:pt-0 pt-2 ">
            <p class="text-gray-700 font-bold">Ticket</p>
            <p>Concert ou évènement</p>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./service.component.scss'],
})
export class ServiceComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
