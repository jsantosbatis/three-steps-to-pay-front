import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SiteComponent } from './pages/site/site.component';
import { NavbarComponent } from './pages/site/navbar/navbar.component';
import { HeroComponent } from './pages/site/hero/hero.component';
import { ProductComponent } from './pages/site/product/product.component';
import { ServiceComponent } from './pages/site/service/service.component';
import { StepInfoComponent } from './pages/site/step-info/step-info.component';
import { ContactComponent } from './pages/site/contact/contact.component';
import { AppRoutingModule } from './app-routing.module';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    SiteComponent,
    NavbarComponent,
    HeroComponent,
    ProductComponent,
    ServiceComponent,
    StepInfoComponent,
    ContactComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    ScrollToModule.forRoot(),
  ],
  exports: [],
})
export class LandingModule {}
