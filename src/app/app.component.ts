import { Component, OnDestroy, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  template: `
    <div>
      <ngx-spinner bdColor="rgba(51,51,51,0.5)"></ngx-spinner>
      <router-outlet></router-outlet>
      <app-alert></app-alert>
    </div>
  `,
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  constructor() {}

  themeEventListener(e: MediaQueryListEvent) {
    if (e.matches) {
      document.querySelector('body').classList.add('scheme-dark');
    } else {
      document.querySelector('body').classList.remove('scheme-dark');
    }
  }

  ngOnInit(): void {
    window
      .matchMedia('(prefers-color-scheme: dark)')
      .addEventListener('change', this.themeEventListener);
    const appVersion = localStorage.getItem('appVersion');

    if (environment.appVersion != appVersion) {
      localStorage.clear();
      localStorage.setItem('appVersion', environment.appVersion);
    }
  }

  ngOnDestroy(): void {
    window.removeEventListener('change', this.themeEventListener);
  }
}
