export enum PaymentType {
  PAYCONIQ = 1,
  VISA = 2,
  MASTERCARD = 3,
  BANCONTACT = 4,
  PAYPAL = 5,
  SEPA = 6,
  SOFORT = 7,
}
