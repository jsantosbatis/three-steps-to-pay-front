export enum ProductCategory {
  CANDLE = 1,
  GIFT = 2,
  COLLECTION = 3,
  ITEM = 4,
  TICKET = 5,
}
