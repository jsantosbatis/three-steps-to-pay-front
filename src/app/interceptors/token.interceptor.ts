import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthService } from '@portal/services/auth.service';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private _auth: AuthService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return from(this._auth.getToken()).pipe(
      switchMap((token) => {
        request = request.clone({
          setHeaders: {
            Authorization: this._auth.connectedAs
              ? `Bearer ${token} connectedAs ${this._auth.connectedAs.id}`
              : `Bearer ${token}`,
          },
          body: { ...request.body },
        });
        return next.handle(request);
      })
    );
  }
}
