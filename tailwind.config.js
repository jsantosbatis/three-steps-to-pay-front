const defaultTheme = require('tailwindcss/defaultTheme')
const colors = require('@tailwindcss/ui/colors')

module.exports = {
  prefix: '',
  purge: {
    content: ['./src/**/*.{html,ts}'],
  },
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        primary: colors.indigo,
      },
      zIndex: {
        '-10': '-10',
      },
    },
  },
  variants: {
    borderStyle: ['responsive', 'hover', 'focus'],
    borderWidth: ['responsive', 'hover', 'focus'],
    borderColor: ['responsive', 'hover', 'focus', 'dark'],
    backgroundColor: [
      'responsive',
      'hover',
      'focus',
      'active',
      'group-hover',
      'disabled',
      'dark',
    ],
    cursor: ['responsive', 'hover', 'focus', 'disabled'],
    textColor: ['dark'],
    opacity: ['disabled'],
    extend: {},
  },
  plugins: [require('@tailwindcss/ui'), require('@tailwindcss/forms')],
}
